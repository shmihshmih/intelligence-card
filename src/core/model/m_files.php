<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("SELECT $db.f_files8mod(
                                                    :id,
                                                    :pid,
                                                    :caption,
                                                    :author,
                                                    :link,
                                                    :bibliography,
                                                    :type,
                                                    :key_word,
                                                    :format)");
$stmt->execute(array('id'           => $decodedData['files']['id'],
                     'pid'          => $decodedData['files']['pid']?:null,
                     'caption'      => $decodedData['files']['caption'],
                     'author'       => $decodedData['files']['author'],
                     'link'         => $decodedData['files']['link'],
                     'bibliography' => $decodedData['files']['bibliography'],
                     'type'         => $decodedData['files']['type'],
                     'key_word'     => $decodedData['files']['key_word'],
                     'format'       => $decodedData['files']['format']));
$err = $stmt->fetch();
if(isset($err[2]) == false) {
    echo $err[2];
} else {
    echo $err[2];
}

//удаление файла
if(isset($decodedData['del'])) {
    $stmt = $connect->prepare("SELECT $db.f_files8del(?)");
    $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
    $stmt->execute();
    $arr = $stmt->errorInfo();
    echo json_encode($arr);
}
?>