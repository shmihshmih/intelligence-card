<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("SELECT $db.f_person8mod(
                                                   :id,
                                                   :name,
                                                   :surname,
                                                   :last_name,
                                                   :birth_date,
                                                   :death_date,
                                                   :country,
                                                   :parent,
                                                   :school,
                                                   :key_word,
                                                   :annotation,
                                                   :link,
                                                   :portrait,
                                                   :bibliography)");
$stmt->execute(array('id'           => $decodedData['person']['id'],
                     'name'         => $decodedData['person']['name'],
                     'surname'      => $decodedData['person']['surname'],
                     'last_name'    => $decodedData['person']['last_name'],
                     'birth_date'   => $decodedData['person']['birth_date'],
                     'death_date'   => $decodedData['person']['death_date'],
                     'country'      => $decodedData['person']['country']?:null,
                     'parent'       => $decodedData['person']['parent']?:null,
                     'school'       => $decodedData['person']['school']?:null,
                     'key_word'     => $decodedData['person']['key_word'],
                     'annotation'   => $decodedData['person']['annotation'],
                     'link'         => $decodedData['person']['link'],
                     'portrait'     => $decodedData['person']['portrait'],
                     'bibliography' => $decodedData['person']['bibliography']));
$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление персоны
if($decodedData['del']) {
    $stmt = $connect->prepare("SELECT $db.f_person8del(?)");
    $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
    $stmt->execute();
    $arr = $stmt->errorInfo();
    echo json_encode($arr);
};
