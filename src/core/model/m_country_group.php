<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("SELECT $db.f_country_group8mod(
                                                          :id,
                                                          :caption)");

$stmt->execute(array('id'      => $decodedData['countryGroup']['id'],
                     'caption' => $decodedData['countryGroup']['caption']));

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление группы стран
if(isset($decodedData['del'])) {
    $stmt = $connect->prepare("SELECT $db.f_country_group8del(?)");
    $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
    $stmt->execute();
    $arr = $stmt->errorInfo();
    echo json_encode($arr);
}
?>