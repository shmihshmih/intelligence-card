<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("SELECT $db.f_dictionary8mod(
                                                       :id,
                                                       :pid,
                                                       :parent,
                                                       :caption,
                                                       :description,
                                                       :bibliography,
                                                       :foreign_lang)");

$stmt->execute(array('id'           => $decodedData['dictionary']['id'],
                     'pid'          => $decodedData['dictionary']['pid']?:null,
                     'parent'       => $decodedData['dictionary']['parent']?:null,
                     'caption'      => $decodedData['dictionary']['caption'],
                     'description'  => $decodedData['dictionary']['description'],
                     'bibliography' => $decodedData['dictionary']['bibliography'],
                     'foreign_lang' => $decodedData['dictionary']['foreign_lang']));

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление dict
if(isset($decodedData['del'])) {
    $stmt = $connect->prepare("SELECT $db.f_dictionary8del(?)");
    $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
    $stmt->execute();
    $arr = $stmt->errorInfo();
    echo json_encode($arr);
}
?>