<?php
header('access-control-allow-origin: *');
header('access-control-allow-headers: content-type, origin');
header('content-type: application/json;charset=utf-8');
require_once($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("SELECT $db.f_opus8mod(:id, 
                                                 :pid, 
                                                 :caption, 
                                                 :annotation, 
                                                 :cover, 
                                                 :published, 
                                                 :bibliography, 
                                                 :key_word, 
                                                 :section)");
$stmt->execute(array('id'           => $decodedData['opus']['id'],
                     'pid'          => $decodedData['opus']['pid']?:null,
                     'caption'      => $decodedData['opus']['caption'],
                     'annotation'   => $decodedData['opus']['annotation'],
                     'cover'        => $decodedData['opus']['cover'],
                     'published'    => $decodedData['opus']['published'],
                     'bibliography' => $decodedData['opus']['bibliography'],
                     'key_word'     => $decodedData['opus']['key_word'],
                     'section'      => $decodedData['opus']['section']?:null));

$err = $stmt->fetch();

if(isset($err[2]) == false) {
    echo $err[2];
} else {
    echo $err[2];
}

//удаление произведения
if(isset($decodedData['del'])) {
    $stmt = $connect->prepare("SELECT $db.f_opus8del(?)");
    $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
    $stmt->execute();
    $arr = $stmt->errorInfo();
    echo json_encode($arr);
}
?>