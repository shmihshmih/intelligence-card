<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("SELECT $db.f_school8mod(
                                                   :id,
                                                   :school,
                                                   :annotation,
                                                   :parent,
                                                   :bibliography)");

$stmt->execute(array('id'           => $decodedData['school']['id'],
                     'school'       => $decodedData['school']['school'],
                     'annotation'   => $decodedData['school']['annotation'],
                     'parent'       => $decodedData['school']['parent']?:null,
                     'bibliography' => $decodedData['school']['bibliography']));

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление школы
if(isset($decodedData['del'])) {
    $stmt = $connect->prepare("SELECT $db.f_school8del(?)");
    $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
    $stmt->execute();
    $arr = $stmt->errorInfo();
    echo json_encode($arr);
}
?>
