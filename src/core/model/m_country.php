<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("SELECT $db.f_country8mod(
                                                   :id,
                                                   :pid,
                                                   :country,
                                                   :language)");

$stmt->execute(array(
    'id'       => $decodedData['country']['id'],
    'pid'      => $decodedData['country']['pid']?:null,
    'country'  => $decodedData['country']['country'],
    'language' => $decodedData['country']['language']
  )
);

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_country8del(?)");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();
  $arr = $stmt->errorInfo();
  echo $arr;
}
?>