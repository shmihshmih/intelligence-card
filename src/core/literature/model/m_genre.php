<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("
SELECT $db.f_genre8mod(:pn_id, :pv_caption, :pv_description);
                                                   ");

$stmt->execute(array(
    'pn_id'          => $decodedData['genre']['id'],
    'pv_caption'     => $decodedData['genre']['caption'],
    'pv_description' => $decodedData['genre']['description']
  )
);

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_cycle8del(?);");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();
  $arr = $stmt->errorInfo();
  echo json_encode($arr);
}
?>