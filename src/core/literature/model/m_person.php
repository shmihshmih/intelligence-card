<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("
SELECT $db.f_person8mod(
  :pn_id,
  :pv_name,
  :pv_surname,
  :pv_last_name,
  :pd_birth_date,
  :pd_death_date,
  :pn_country,
  :pv_key_word,
  :pv_annotation,
  :pv_link,
  :pv_portrait,
  :pv_bibliography,
  :pn_group_person
);
                                                   ");

$stmt->execute(array(
    'pn_id'           => $decodedData['person']['id'],
    'pv_name'         => $decodedData['person']['name'],
    'pv_surname'      => $decodedData['person']['surname'],
    'pv_last_name'    => $decodedData['person']['last_name'],
    'pd_birth_date'   => $decodedData['person']['birth_date'],
    'pd_death_date'   => $decodedData['person']['death_date'],
    'pn_country'      => $decodedData['person']['country'],
    'pv_key_word'     => $decodedData['person']['key_word'],
    'pv_annotation'   => $decodedData['person']['annotation'],
    'pv_link'         => $decodedData['person']['link'],
    'pv_portrait'     => $decodedData['person']['portrait'],
    'pv_bibliography' => $decodedData['person']['bibliography'],
    'pn_group_person' => $decodedData['person']['group_person']
  )
);

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_person8del(?);");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();
  $arr = $stmt->errorInfo();
  echo json_encode($arr);
}
?>