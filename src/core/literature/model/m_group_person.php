<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("
SELECT $db.f_group_person8mod(:pn_id, :pv_caption, :pv_description, :pv_link, :pn_previous);
                                                   ");

$stmt->execute(array(
    'pn_id'           => $decodedData['group_person']['id'],
    'pv_caption'      => $decodedData['group_person']['caption'],
    'pv_description'  => $decodedData['group_person']['description'],
    'pv_link'         => $decodedData['group_person']['link'],
    'pn_previous'     => $decodedData['group_person']['previous']
  )
);

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_group_person8del(?);");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();
  $arr = $stmt->errorInfo();
  echo json_encode($arr);
}
?>