<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("
SELECT $db.f_phrase8mod(:pn_id, :pn_pid, :pv_caption, :pv_original);
                                                   ");

$stmt->execute(array(
    'pn_id'       => $decodedData['phrase']['id'],
    'pn_pid'      => $decodedData['phrase']['pid'],
    'pv_caption'  => $decodedData['phrase']['caption'],
    'pv_original' => $decodedData['phrase']['original']
  )
);

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_phrase8del(?);");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();
  $arr = $stmt->errorInfo();
  echo json_encode($arr);
}
?>