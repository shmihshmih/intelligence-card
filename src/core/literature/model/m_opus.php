<?php
header("content-type: application/json; charset=utf-8");
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');
//редактирование или добавление
$data = file_get_contents('php://input');
$decodedData = json_decode($data, true);
$stmt = $connect->prepare("
SELECT $db.f_opus8mod(
  :id,
  :pid,
  :caption,
  :annotation,
  :cover,
  :published,
  :bibliography,
  :key_word,
  :original_lang,
  :cycle,
  :cycle_order,
  :genre,
  :type
);
                                                   ");

$stmt->execute(array(
    'id'           => $decodedData['opus']['id'],
    'pid'         => $decodedData['opus']['pid'],
    'caption'      => $decodedData['opus']['caption'],
    'annotation'    => $decodedData['opus']['annotation'],
    'cover'   => $decodedData['opus']['cover'],
    'published'   => $decodedData['opus']['published'],
    'bibliography'      => $decodedData['opus']['bibliography'],
    'key_word'     => $decodedData['opus']['key_word'],
    'original_lang'   => $decodedData['opus']['original_lang'],
    'cycle'         => $decodedData['opus']['cycle'],
    'cycle_order'     => $decodedData['opus']['cycle_order'],
    'genre' => $decodedData['opus']['genre'],
    'type' => $decodedData['opus']['type']
  )
);

$err = $stmt->fetch();

if(isset($err[2]) == false) {
  echo $err[2];
} else {
  echo $err[2];
}

//удаление стран
if(isset($decodedData['del'])) {
  $stmt = $connect->prepare("SELECT $db.f_opus8del(?);");
  $stmt->bindValue(1, $decodedData['del'], PDO::PARAM_INT);
  $stmt->execute();
  $arr = $stmt->errorInfo();
  echo json_encode($arr);
}
?>