<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');

$stmt = $connect->prepare("SELECT * from $db.v_person");

$stmt->execute();

while($row = $stmt->fetch()) {
  $data[] = [
    'id' => $row['id'],
    'content' => '
      <div>
        <div>'.$row['name'].' '.$row['surname'].'</div>
         <a href="'.$row['link'].'" target="blank">
          <img style="max-width: 100px;" src="'.$row['portrait'].'" alt="'.$row['annotation'].'">
         </a>
         <div>'.$row['country_caption'].'</div>
         <div>'.$row['group_person_caption'].'</div>
      </div>',

    'start' => $row['birth_date'],
    'type' => 'box'
  ];
};

echo json_encode($data);

