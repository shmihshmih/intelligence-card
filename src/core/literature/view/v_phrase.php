<?php
header('access-control-allow-headers: origin. content-type');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');

require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');

$stmt = $connect->prepare("
SELECT 
  id,
  pid,
  caption,
  original
FROM 
  literature.v_phrase ;
");
$stmt->execute();


while($row = $stmt->fetch()) {
  $data[] = [
    'id'   => $row['id'],
    'pid' => $row['pid'],
    'caption' => $row['caption'],
    'original' => $row['original']
  ];
}

echo json_encode($data);
