<?php
header('access-control-allow-headers: origin. content-type');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');

require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');

$stmt = $connect->prepare("
SELECT 
  id,
  pid,
  caption,
  annotation,
  cover,
  published,
  bibliography,
  key_word,
  original_lang,
  cycle,
  cycle_order,
  genre,
  type
FROM 
  literature.v_opus ;
");
$stmt->execute();


while($row = $stmt->fetch()) {
  $data[] = [
    'id'   => $row['id'],
    'pid' => $row['pid'],
    'caption' => $row['caption'],
    'annotation' => $row['annotation'],
    'cover' => $row['cover'],
    'published' => $row['published'],
    'bibliography' => $row['bibliography'],
    'key_word' => $row['key_word'],
    'original_lang' => $row['original_lang'],
    'cycle' => $row['cycle'],
    'cycle_order' => $row['cycle_order'],
    'genre' => $row['genre'],
    'type' => $row['type']
  ];
}

echo json_encode($data);
