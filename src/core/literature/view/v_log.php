<?php
header('access-control-allow-headers: origin. content-type');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');

require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');

$stmt = $connect->prepare("
SELECT 
  timed,
  message
FROM 
  literature.v_log 
");
$stmt->execute();


while($row = $stmt->fetch()) {
  $data[] = [
    'timed'   => $row['timed'],
    'message' => $row['message']
  ];
}

echo json_encode($data);
