<?php
header('access-control-allow-headers: origin. content-type');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');

require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');

$stmt = $connect->prepare("
SELECT 
  id,
  name,
  surname,
  last_name,
  birth_date,
  death_date,
  country,
  key_word,
  annotation,
  link,
  portrait,
  bibliography,
  group_person
FROM 
  literature.v_person ;
");
$stmt->execute();


while($row = $stmt->fetch()) {
  $data[] = [
    'id'   => $row['id'],
    'name' => $row['name'],
    'surname' => $row['surname'],
    'last_name' => $row['last_name'],
    'birth_date' => $row['birth_date'],
    'death_date' => $row['death_date'],
    'country' => $row['country'],
    'key_word' => $row['key_word'],
    'annotation' => $row['annotation'],
    'link' => $row['link'],
    'portrait' => $row['portrait'],
    'bibliography' => $row['bibliography'],
    'group_person' => $row['group_person']
  ];
}

echo json_encode($data);
