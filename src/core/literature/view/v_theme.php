<?php
header('access-control-allow-headers: origin. content-type');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');

require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/literature/db.php');

$stmt = $connect->prepare("
SELECT 
  id,
  caption,
  description,
  link
FROM 
  literature.v_theme ;
");
$stmt->execute();


while($row = $stmt->fetch()) {
  $data[] = [
    'id'   => $row['id'],
    'caption' => $row['caption'],
    'description' => $row['description'],
    'link' => $row['link']
  ];
}

echo json_encode($data);
