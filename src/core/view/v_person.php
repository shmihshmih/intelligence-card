<?php
header("content-type: application/json; charset=utf-8");
header("access-control-allow-origin: *");
header("Access-Control-Allow-Headers: content-type, origin");

require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  select v.id,
         v.name, 
         v.surname,
         v.last_name,
         v.birth_date,
         v.death_date,
         v.country_id,
         v.papa_person_id,
         v.school_id,
         v.person_key_word,
         v.person_annotation,
         v.link,
         v.portrait,
         v.person_bibliography,
         v.country_caption,
         v.school_caption
    from $db.v_person v 
    order by v.name");

$stmt->execute();

while($row = $stmt->fetch()) {
    $data[] = [
        'id'              => $row['id'],
        'name'            => $row['name'],
        'surname'         => $row['surname'],
        'last_name'       => $row['last_name'],
        'birth_date'      => $row['birth_date'],
        'death_date'      => $row['death_date'],
        'country'         => $row['country_id'],
        'parent'          => $row['papa_person_id'],
        'school'          => $row['school_id'],
        'key_word'        => $row['person_key_word'],
        'annotation'      => $row['person_annotation'],
        'link'            => $row['link'],
        'portrait'        => $row['portrait'],
        'bibliography'    => $row['person_bibliography'],
        'country_caption' => $row['country_caption'],
        'school_caption'  => $row['school_caption']
    ];
}
echo json_encode($data);