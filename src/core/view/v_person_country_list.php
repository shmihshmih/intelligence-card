<?php
header("access-control-allow-header: content-type, origin");
header("access-control-allow-origin: *");
header("content-type: application/json;charset=utf-8");
require_once ($_SERVER['DOCUMENT_ROOT']."/my-app/src/core/db.php");
    $stmt = $connect->prepare("
    SELECT 
        id, 
        country 
    from $db.v_country");
    $stmt->execute();
    while($row = $stmt->fetch()) {
        $data[] = [
            'id' => $row['id'],
            'country' => $row['country']
        ];
    };
    echo json_encode($data);
?>
