<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  SELECT v.id, 
         v.pid,
         v.idiom,
         v.original,
         v.name
  from $db.v_idiom v 
  order by v.idiom");
$stmt->execute();
while($row = $stmt->fetch()) {
    $data[] = [
        'id' => $row['id'],
        'pid' => $row['pid'],
        'idiom' => $row['idiom'],
        'original' => $row['original'],
        'name' => $row['name']
    ];
}
echo json_encode($data);