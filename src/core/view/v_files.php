<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  SELECT v.id, 
         v.pid,
         v.caption,
         v.author,
         v.link,
         v.add_date,
         v.visible,
         v.bibliography,
         v.type,
         v.key_word,
         v.format
  from $db.v_files v 
  order by v.caption");
$stmt->execute();
while($row = $stmt->fetch()) {
    $data[] = [
        'id' => $row['id'],
        'pid' => $row['pid'],
        'caption' => $row['caption'],
        'author' => $row['author'],
        'link' => $row['link'],
        'add_date' => $row['add_date'],
        'visible' => $row['visible'],
        'bibliography' => $row['bibliography'],
        'type' => $row['type'],
        'key_word' => $row['key_word'],
        'format' => $row['format']
    ];
}
echo json_encode($data);