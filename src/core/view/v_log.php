<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');

$stmt = $connect->prepare("
SELECT v.timed,
       v.message
from $db.core_log v 
order by -v.timed");

$stmt->execute();

while($row = $stmt->fetch()) {
    $data[] = [
      'timed'   => $row['timed'],
      'message' => $row['message']
    ];
}
echo json_encode($data);