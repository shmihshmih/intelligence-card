<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type,origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  SELECT v.id, 
         v.pid,
         v.parent,
         v.caption,
         v.description,
         v.bibliography,
         v.foreign_lang,
         v.parent_caption,
         v.parent_description,
         v.name,
         v.surname
  from $db.v_dictionary v 
  order by v.caption");

$stmt->execute();

while($row = $stmt->fetch()) {
    $data[] = [
        'id' => $row['id'],
        'pid' => $row['pid'],
        'parent' => $row['parent'],
        'caption' => $row['caption'],
        'description' => $row['description'],
        'bibliography' => $row['bibliography'],
        'foreign_lang' => $row['foreign_lang'],
        'parent_caption' => $row['parent_caption'],
        'parent_description' => $row['parent_description'],
        'name' => $row['name'],
        'surname' => $row['surname']
    ];
}

echo json_encode($data);