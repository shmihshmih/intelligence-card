<?php
header("content-type: application/json;charset=utf8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  select 
    v.id,
    v.school,
    v.annotation,
    v.parent,
    v.bibliography,
    v.parent_school
  from $db.v_school v 
  order by v.school");
$stmt->execute();
while($row = $stmt->fetch()) {
    $data[] = [
        'id' => $row['id'],
        'school' => $row['school'],
        'annotation' => $row['annotation'],
        'parent' => $row['parent'],
        'bibliography' => $row['bibliography'],
        'parent_school' => $row['parent_school']
    ];
}
echo json_encode($data);
