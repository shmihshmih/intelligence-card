<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json; charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
    $stmt = $connect->prepare("
SELECT 
id, 
caption 
from $db.v_dictionary");
    $stmt->execute();
    while($row = $stmt->fetch()) {
        $data[] = [
          'id' => $row['id'],
            'caption' => $row['caption']
        ];
    };
echo json_encode($data);
