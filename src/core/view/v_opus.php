<?php
header("content-type: application/json; charset=utf-8");
header("access-control-allow-origin: *");
header("Access-Control-Allow-Headers: content-type, origin");

require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  select v.id, 
         v.pid,
         v.caption,
         v.annotation,
         v.cover,
         v.published,
         v.bibliography,
         v.key_word,
         v.section,
         v.name,
         v.surname,
         v.section_caption
  from $db.v_opus v 
  order by v.caption");

$stmt->execute();

while($row = $stmt->fetch()) {
    $data[] = [
      'id' => $row['id'],
      'pid' => $row['pid'],
      'caption' => $row['caption'],
      'annotation' => $row['annotation'],
      'cover' => $row['cover'],
      'published' => $row['published'],
      'bibliography' => $row['bibliography'],
      'key_word' => $row['key_word'],
      'section' => $row['section'],
        'name' => $row['name'],
        'surname' => $row['surname'],
        'section_caption' => $row['section_caption']
    ];
}
echo json_encode($data);