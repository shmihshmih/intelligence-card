<?php
header('access-control-allow-headers: content-type, origin');
header('access-control-allow-origin: *');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');

$stmt = $connect->prepare("SELECT * from $db.v_person");

$stmt->execute();

while($row = $stmt->fetch()) {

    //Что возможно вытащить
    //$data[] = [
    //  'id'                  => $row['id'],
    //  'name'                => $row['name'],
    //  'surname'             => $row['surname'],
    //  'last_name'           => $row['last_name'],
    //  'birth_date'          => $row['birth_date'],
    //  'death_date'          => $row['death_date'],
    //  'country_id'          => $row['country_id'],
    //  'papa_person_id'      => $row['papa_person_id'],
    //  'school_id'           => $row['school_id'],
    //  'person_annotation'   => $row['person_annotation'],
    //  'link'                => $row['link'],
    //  'portrait'            => $row['portrait'],
    //  'person_bibliography' => $row['person_bibliography'],
    //  'person_key_word'     => $row['person_key_word'],
    //  'country_caption'     => $row['country_caption'],
    //  'school_caption'      => $row['school_caption']
    //];

  $data[] = [
    'id' => $row['id'],
    'content' => '
      <div>
        <div>'.$row['name'].' '.$row['surname'].'</div>
         <a href="'.$row['link'].'" target="blank">
          <img style="max-width: 100px;" src="'.$row['portrait'].'" alt="'.$row['person_annotation'].'">
         </a>
         <div>'.$row['country_caption'].'</div>
         <div>'.$row['school_caption'].'</div>
      </div>',

    'start' => $row['birth_date'],
    'type' => 'box'
  ];
};

echo json_encode($data);

