<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  SELECT v.id, 
         v.pid,
         v.country,
         v.language,
         v.pid_country,
         v.pid_language
  from $db.v_country v 
  order by v.country");
$stmt->execute();
while($row = $stmt->fetch()) {
    $data[] = [
      'id' => $row['id'],
        'pid' => $row['pid'],
        'country' => $row['country'],
        'language' => $row['language'],
        'pid_country' => $row['pid_country'],
        'pid_language' => $row['pid_language']
    ];
}
echo json_encode($data);
?>