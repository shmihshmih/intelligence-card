<?php
header("content-type: application/json;charset=utf8;");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type, origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  SELECT 
    v.id, 
    v.pid,
    v.caption,
    v.annotation,
    v.pid_caption,
    v.pid_annotation
  from $db.v_section v 
  order by v.caption");
$stmt->execute();
while($row = $stmt->fetch()) {
    $data[] = [
      'id' => $row['id'],
        'pid' => $row['pid'],
        'caption' => $row['caption'],
        'annotation' => $row['annotation'],
        'pid_caption' => $row['pid_caption'],
        'pid_annotation' => $row['pis_annotation']
    ];
}
echo json_encode($data);