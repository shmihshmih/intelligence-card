<?php
header('content-type: application/json; charset=utf-8;');
header('access-control-allow-origin: *');
header('access-control-allow-headers: content-type, origin');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
    $stmt = $connect->prepare("
SELECT 
id, 
name, 
surname 
from $db.v_person");
    $stmt->execute();
    while($row = $stmt->fetch()) {
        $data[] = [
            'id' => $row['id'],
            'name' => $row['name'],
            'surname' => $row['surname']
        ];
    };
    echo json_encode($data);
?>
