<?php
header('access-control-allow-origin: *');
header('access-control-allow-header: content-type, origin');
header('content-type: application/json;charset=utf-8');
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
    $stmt = $connect->prepare("
SELECT 
id, 
school 
from $db.v_school");
    $stmt->execute();
    while($row = $stmt->fetch()) {
        $data[] = [
          'id' => $row['id'],
            'school' => $row['school']
        ];
    };
    echo json_encode($data);
?>
