<?php
header("content-type: application/json;charset=utf-8");
header("access-control-allow-origin: *");
header("access-control-allow-headers: content-type,origin");
require_once ($_SERVER['DOCUMENT_ROOT'].'/my-app/src/core/db.php');
$stmt = $connect->prepare("
  SELECT v.id, 
       v.caption 
  from $db.v_country_group v 
  order by v.caption");
$stmt->execute();
while($row = $stmt->fetch()) {
    $data[] = [
        'id' => $row['id'],
        'caption' => $row['caption']
    ];
}
echo json_encode($data);