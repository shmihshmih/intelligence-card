import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

import { LiteratureService } from './literature-card/service/literature.service';
import { LiteratureAdmin } from './literature-card/admin/admin.component';

import { PhilosophyService } from './philosophy-card/service/philosophy.service';
import { PhilosophyAdmin } from './philosophy-card/admin/admin.component';
import { PhilosophyLog } from './philosophy-card/log/log.component';
import { Main } from './main/main.component';
import { PhilosophyVisual } from './philosophy-card/visual/visual.component';
import { VisModule } from './common/vis-timeline/ng2-vis';
import { LiteratureLog } from './literature-card/log/log.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LiteratureVisual } from './literature-card/visual/visual.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    VisModule,
    routing
  ],
  declarations: [
    AppComponent,
    LiteratureAdmin,
    LiteratureLog,
    LiteratureVisual,
    PhilosophyAdmin,
    PhilosophyLog,
    PhilosophyVisual,
    Main
  ],
  providers: [
    LiteratureService,
    PhilosophyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
