"use strict";
var core_1 = require("@angular/core");
require("../style/app.scss");
var AppComponent = (function () {
    function AppComponent() {
        this.url = 'https://bitbucket.org/shmihshmih/intelligence-card';
        this.title = 'Hello from AppComponent';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.scss'],
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map