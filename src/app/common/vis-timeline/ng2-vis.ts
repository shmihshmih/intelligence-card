import { NgModule } from '@angular/core';

import { VisTimelineDirective, VisTimelineService } from './index';

export * from './index';

@NgModule({
  declarations: [ VisTimelineDirective],
  exports: [ VisTimelineDirective],
  providers: [ VisTimelineService],
})
export class VisModule { }
