import { RouterModule, Routes } from '@angular/router';

import { LiteratureAdmin } from './literature-card/admin/admin.component';
import { PhilosophyAdmin } from './philosophy-card/admin/admin.component';
import { PhilosophyLog } from './philosophy-card/log/log.component';
import { Main } from './main/main.component';
import { PhilosophyVisual } from './philosophy-card/visual/visual.component';
import { LiteratureLog } from './literature-card/log/log.component';
import { LiteratureVisual } from './literature-card/visual/visual.component';

const routes: Routes = [
  { path: '', component: Main },
  { path: 'literatureAdmin', component:  LiteratureAdmin},
  { path: 'philosophyAdmin', component: PhilosophyAdmin },
  { path: 'main', component: Main },
  { path: 'philosophyLog', component: PhilosophyLog },
  { path: 'philosophyVisual', component: PhilosophyVisual },
  { path: 'literatureLog', component: LiteratureLog},
  { path: 'literatureVisual', component: LiteratureVisual}

];

export const routing = RouterModule.forRoot(routes);
