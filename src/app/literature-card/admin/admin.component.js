"use strict";
var core_1 = require("@angular/core");
var literature_service_1 = require("../service/literature.service");
var LiteratureAdmin = (function () {
    function LiteratureAdmin(literatureService) {
        this.literatureService = literatureService;
    }
    LiteratureAdmin.prototype.ngOnInit = function () {
        this.getInfoAbout();
    };
    LiteratureAdmin.prototype.getInfoAbout = function () {
        var _this = this;
        this.literatureService.getAuthors()
            .then(function (authors) { return _this.authors = authors; });
    };
    return LiteratureAdmin;
}());
LiteratureAdmin = __decorate([
    core_1.Component({
        selector: 'literature-admin-panel',
        templateUrl: './admin.component.html',
        styleUrls: ['./admin.component.scss'],
        providers: [literature_service_1.LiteratureService]
    }),
    __metadata("design:paramtypes", [literature_service_1.LiteratureService])
], LiteratureAdmin);
exports.LiteratureAdmin = LiteratureAdmin;
//# sourceMappingURL=admin.component.js.map