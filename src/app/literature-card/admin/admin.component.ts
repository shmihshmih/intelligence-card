import { Component, OnInit } from '@angular/core';
//validators
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {
  trigger, state, animate, transition, style
} from '@angular/animations';

import { LiteratureService } from '../service/literature.service';

import { Person } from '../service/model/person.model';
import { Country } from '../service/model/country.model';
import { Cycle } from '../service/model/cycle.model';
import { Genre } from '../service/model/genre.model';
import { GroupPerson } from '../service/model/group_person.model';
import { Opus } from '../service/model/opus.model';
import { Phrase } from '../service/model/phrase.model';
import { Theme } from '../service/model/theme.model';
import { TypeOpus } from '../service/model/type_opus.model';

@Component ({
    selector: 'literature-admin-panel',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss'],
    providers: [LiteratureService],
    animations: [
      trigger('personChangedE', [
        state('active', style({opacity: 0.7, zIndex: 2})),
        state('inactive', style({opacity: 0, zIndex: 1})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('personChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('opusChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('opusChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('group_personChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('group_personChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('type_opusChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('type_opusChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('phraseChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('phraseChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('genreChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('genreChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('cycleChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('cycleChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('themeChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('themeChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('countryChangedE', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ]),
      trigger('countryChangedL', [
        state('active', style({opacity: 1})),
        state('inactive', style({opacity: 0.3})),
        transition('inactive => active', animate('300ms ease-in')),
        transition('active => inactive', animate('300ms ease-out'))
      ])
  ]
})

export class LiteratureAdmin implements OnInit {
  group_person_mod:boolean = false;
  type_opus_mod   :boolean = false;
  genre_mod       :boolean = false;
  theme_mod       :boolean = false;

  persons: Person[] = [];
  countries: Country[] = [];
  cycles: Cycle[] = [];
  genres: Genre[] = [];
  group_persons: GroupPerson[] = [];
  opuses: Opus[] = [];
  phrases: Phrase[] = [];
  themes: Theme[] = [];
  type_opuses: TypeOpus[] = [];

  activeCountry: Country = new Country();
  activeTheme: Theme = new Theme();
  activeCycle: Cycle = new Cycle();
  activeGenre: Genre = new Genre();
  activePhrase: Phrase = new Phrase();
  activeGroup_person: GroupPerson = new GroupPerson();
  activePerson: Person = new Person();
  activeOpus: Opus = new Opus();
  activeType_opus = new TypeOpus();

  countryEditForm: FormGroup;
  themeEditForm: FormGroup;
  cycleEditForm: FormGroup;
  genreEditForm: FormGroup;
  phraseEditForm: FormGroup;
  group_personEditForm: FormGroup;
  personEditForm: FormGroup;
  opusEditForm: FormGroup;
  type_opusEditForm: FormGroup;

  constructor(private literatureService: LiteratureService,
              public formBuilder: FormBuilder) {
    this.createForms();
  }

  createForms():void {
    this.countryEditForm = this.formBuilder.group({
      'id':       new FormControl(this.activeCountry.id),
      'caption':  new FormControl(this.activeCountry.caption),
      'language': new FormControl(this.activeCountry.language, Validators.required)
    });
    this.themeEditForm = this.formBuilder.group({
      'id':          new FormControl(this.activeTheme.id),
      'caption':     new FormControl(this.activeTheme.caption),
      'description': new FormControl(this.activeTheme.description),
      'link':        new FormControl(this.activeTheme.link)
    });
    this.cycleEditForm = this.formBuilder.group({
      'id':          new FormControl(this.activeCycle.id),
      'caption':     new FormControl(this.activeCycle.caption),
      'description': new FormControl(this.activeCycle.description),
      'link':        new FormControl(this.activeCycle.link)
    });
    this.genreEditForm = this.formBuilder.group({
      'id':          new FormControl(this.activeGenre.id),
      'caption':     new FormControl(this.activeGenre.caption),
      'description': new FormControl(this.activeGenre.description)
    });
    this.phraseEditForm = this.formBuilder.group({
      'id':          new FormControl(this.activePhrase.id),
      'pid':         new FormControl(this.activePhrase.pid),
      'caption':     new FormControl(this.activePhrase.caption),
      'original':    new FormControl(this.activePhrase.original)
    });
    this.group_personEditForm = this.formBuilder.group({
      'id':           new FormControl(this.activeGroup_person.id),
      'caption':      new FormControl(this.activeGroup_person.caption),
      'description':  new FormControl(this.activeGroup_person.description),
      'link':         new FormControl(this.activeGroup_person.link),
      'previous':     new FormControl(this.activeGroup_person.previous)
    });
    this.personEditForm = this.formBuilder.group({
      'id':           new FormControl(this.activePerson.id),
      'name':         new FormControl(this.activePerson.name),
      'surname':      new FormControl(this.activePerson.surname),
      'last_name':    new FormControl(this.activePerson.last_name),
      'birth_date':   new FormControl(this.activePerson.birth_date),
      'death_date':   new FormControl(this.activePerson.death_date),
      'country':      new FormControl(this.activePerson.country),
      'key_word':     new FormControl(this.activePerson.key_word),
      'annotation':   new FormControl(this.activePerson.annotation),
      'link':         new FormControl(this.activePerson.link),
      'portrait':     new FormControl(this.activePerson.portrait),
      'bibliography': new FormControl(this.activePerson.bibliography),
      'group_person': new FormControl(this.activePerson.group_person)
    });
    this.opusEditForm = this.formBuilder.group({
      'id':           new FormControl(this.activeOpus.id),
      'pid':          new FormControl(this.activeOpus.pid),
      'caption':      new FormControl(this.activeOpus.caption),
      'annotation':   new FormControl(this.activeOpus.annotation),
      'cover':        new FormControl(this.activeOpus.cover),
      'published':    new FormControl(this.activeOpus.published),
      'bibliography': new FormControl(this.activeOpus.bibliography),
      'key_word':     new FormControl(this.activeOpus.key_word),
      'original_lang':new FormControl(this.activeOpus.original_lang),
      'cycle':        new FormControl(this.activeOpus.cycle),
      'cycle_order':  new FormControl(this.activeOpus.cycle_order),
      'genre':        new FormControl(this.activeOpus.genre),
      'type':         new FormControl(this.activeOpus.type)
    });
    this.type_opusEditForm = this.formBuilder.group({
      'id':          new FormControl(this.activeType_opus.id),
      'caption':     new FormControl(this.activeType_opus.caption),
      'description': new FormControl(this.activeType_opus.description)
    });
  }

  ngOnInit():void {
      this.getPersons();
      this.getCountries();
      this.getCycles();
      this.getGenres();
      this.getGroupPersons();
      this.getOpuses();
      this.getPhrases();
      this.getThemes();
      this.getTypeOpus();
  }

  visibility = {
    personL: 'active',
    personE: 'inactive',
    opusL: 'active',
    opusE: 'inactive',
    group_personL: 'active',
    group_personE: 'inactive',
    type_opusL: 'active',
    type_opusE: 'inactive',
    phraseL: 'active',
    phraseE: 'inacrive',
    genreL: 'active',
    genreE: 'inacrive',
    cycleL: 'active',
    cycleE: 'inactive',
    themeL: 'active',
    themeE: 'inactive',
    countryL: 'active',
    countryE: 'inactive'
  };

  toggleForm(formName: string): void {
    if (this.visibility[formName+'L'] == 'active') { this.visibility[formName+'L'] = 'inactive';} else { this.visibility[formName+'L'] = 'active' }
    if (this.visibility[formName+'E'] == 'active') { this.visibility[formName+'E'] = 'inactive';} else { this.visibility[formName+'E'] = 'active' }
  }

  //get list
  getPersons() {
      this.literatureService.getPersons('v_person.php')
          .subscribe( persons => this.persons = persons);
  }
  getCountries(): void {
    this.literatureService.getCountries('v_country.php')
      .subscribe(country => this.countries = country);
  }
  getCycles(): void {
    this.literatureService.getCycles('v_cycle.php')
      .subscribe(cycles => this.cycles = cycles);
  }
  getGenres(): void {
    this.literatureService.getGenres('v_genre.php')
      .subscribe(genres => this.genres = genres);
  }
  getGroupPersons(): void {
    this.literatureService.getGroupPersons('v_group_person.php')
      .subscribe(group_persons => this.group_persons = group_persons);
  }
  getOpuses(): void {
    this.literatureService.getOpuses('v_opus.php')
      .subscribe(opuses => this.opuses = opuses);
  }
  getPhrases(): void {
    this.literatureService.getPhrases('v_phrase.php')
      .subscribe(phrases => this.phrases = phrases);
  }
  getThemes(): void {
    this.literatureService.getThemes('v_theme.php')
      .subscribe(themes => this.themes = themes);
  }
  getTypeOpus(): void {
    this.literatureService.getTypeOpus('v_type_opus.php')
      .subscribe(type_opuses => this.type_opuses = type_opuses);
  }

  //get single
  getCountry(identificator: number): void {
     this.literatureService.getCountries('v_country.php')
      .subscribe((countries) => {
       this.activeCountry = countries.find(country => country.id === identificator);
        this.countryEditForm.patchValue({
          'id':      this.activeCountry.id,
          'caption': this.activeCountry.caption,
          'language':this.activeCountry.language
        });
        this.toggleForm('country');
     });
  }
  getTheme(identificator: number): void {
    this.literatureService.getThemes('v_theme.php')
      .subscribe((themes) => {
        this.activeTheme = themes.find(theme => theme.id === identificator);
        this.themeEditForm.patchValue({
          'id':         this.activeTheme.id,
          'caption':    this.activeTheme.caption,
          'description':this.activeTheme.description,
          'link':       this.activeTheme.link
        });
        this.toggleForm('theme');
      });
  }
  getCycle(identificator: number):void {
    this.literatureService.getCycles('v_cycle.php')
      .subscribe((cycles) => {
        this.activeCycle = cycles.find(cycle => cycle.id === identificator);
        this.cycleEditForm.patchValue({
          'id':         this.activeCycle.id,
          'caption':    this.activeCycle.caption,
          'description':this.activeCycle.description,
          'link':       this.activeCycle.link
        });
        this.toggleForm('cycle');
      });
  }
  getGenre(identificator: number): void {
    this.literatureService.getGenres('v_genre.php')
      .subscribe((genres) => {
        this.activeGenre = genres.find(genre => genre.id === identificator);
          this.genreEditForm.patchValue({
            'id':         this.activeGenre.id,
            'caption':    this.activeGenre.caption,
            'description':this.activeGenre.description
          });
        this.toggleForm('genre');
      });
  }
  getPhrase(identificator: number): void {
    this.literatureService.getPhrases('v_phrase.php')
      .subscribe((phrases) => {
        this.activePhrase = phrases.find(phrase => phrase.id === identificator);
        this.phraseEditForm.patchValue({
          'id':      this.activePhrase.id,
          'pid':     this.activePhrase.pid,
          'caption': this.activePhrase.caption,
          'original':this.activePhrase.original
        });
        this.toggleForm('phrase');
      });
  }
  getGroup_person(identificator: number): void {
    this.literatureService.getGroupPersons('v_group_person.php')
      .subscribe((group_persons) => {
        this.activeGroup_person = group_persons.find(group_person => group_person.id === identificator);
        this.group_personEditForm.patchValue({
          'id':         this.activeGroup_person.id,
          'caption':    this.activeGroup_person.caption,
          'description':this.activeGroup_person.description,
          'link':       this.activeGroup_person.link,
          'previous':   this.activeGroup_person.previous
        });
        this.toggleForm('group_person');
      });
  }
  getPerson(identificator: number): void {
    this.literatureService.getPersons('v_person.php')
      .subscribe((persons) => {
        this.activePerson = persons.find(person => person.id === identificator);
        this.personEditForm.patchValue({
          'id':          this.activePerson.id,
          'name':        this.activePerson.name,
          'surname':     this.activePerson.surname,
          'last_name':   this.activePerson.last_name,
          'birth_date':  this.activePerson.birth_date,
          'death_date':  this.activePerson.death_date,
          'country':     this.activePerson.country,
          'key_word':    this.activePerson.key_word,
          'annotation':  this.activePerson.annotation,
          'link':        this.activePerson.link,
          'portrait':    this.activePerson.portrait,
          'bibliography':this.activePerson.bibliography,
          'group_person':this.activePerson.group_person,
        });
        this.toggleForm('person');
      });
  }
  getOpus(identificator: number): void {
    this.literatureService.getOpuses('v_opus.php')
      .subscribe((opuses) => {
        this.activeOpus = opuses.find(opus => opus.id === identificator);
        this.opusEditForm.patchValue({
          'id':           this.activeOpus.id,
          'pid':          this.activeOpus.pid,
          'caption':      this.activeOpus.caption,
          'annotation':   this.activeOpus.annotation,
          'cover':        this.activeOpus.cover,
          'published':    this.activeOpus.published,
          'bibliography': this.activeOpus.bibliography,
          'key_word':     this.activeOpus.key_word,
          'original_lang':this.activeOpus.original_lang,
          'cycle':        this.activeOpus.cycle,
          'cycle_order':  this.activeOpus.cycle_order,
          'genre':        this.activeOpus.genre,
          'type':         this.activeOpus.type,
        });
        this.toggleForm('opus');
      });
  }
  getType_opus(identificator: number): void {
    this.literatureService.getTypeOpus('v_type_opus.php')
      .subscribe((type_opuses) => {
        this.activeType_opus = type_opuses.find(type_opus => type_opus.id === identificator);
        this.type_opusEditForm.patchValue({
          'id':          this.activeType_opus.id,
          'caption':     this.activeType_opus.caption,
          'description': this.activeType_opus.description
        });
        this.toggleForm('type_opus');
      });
  }

  //submit
  countrySubmit(): void {
     this.literatureService.modCountry(this.countryEditForm.value)
       .catch(err => {return err})
       .subscribe(() => {this.getCountries()});
    this.visibility.countryE = 'inactive';
    this.visibility.countryL = 'active';
  }
  themeSubmit(): void {
    this.literatureService.modTheme(this.themeEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getThemes()});
    this.visibility.themeE = 'inactive';
    this.visibility.themeL = 'active';
  }
  cycleSubmit(): void {
    this.literatureService.modCycle(this.cycleEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getCycles()});
    this.visibility.cycleE = 'inactive';
    this.visibility.cycleL = 'active';
  }
  genreSubmit(): void {
    this.literatureService.modGenre(this.genreEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getGenres()});
    this.visibility.genreE = 'inactive';
    this.visibility.genreL = 'active';
  }
  phraseSubmit(): void {
    this.literatureService.modPhrase(this.phraseEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getPhrases()});
    this.visibility.phraseE = 'inactive';
    this.visibility.phraseL = 'active';
  }
  group_personSubmit(): void {
    this.literatureService.modGroup_person(this.group_personEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getGroupPersons()});
    this.visibility.group_personE = 'inactive';
    this.visibility.group_personL = 'active';
  }
  personSubmit(): void {
    this.literatureService.modPerson(this.personEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getPersons()});
    this.visibility.personE = 'inactive';
    this.visibility.personL = 'active';
  }
  opusSubmit(): void {
    this.literatureService.modOpus(this.opusEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getOpuses()});
    this.visibility.opusE = 'inactive';
    this.visibility.opusL = 'active';
  }
  type_opusSubmit(): void {
    this.literatureService.modType_opus(this.type_opusEditForm.value)
      .catch(err => {return err})
      .subscribe(() => {this.getTypeOpus()});
    this.visibility.type_opusE = 'inactive';
    this.visibility.type_opusL = 'active';
  }

  //deletes
  delPerson(id: number): void {
    this.literatureService.delPerson(id)
      .catch(err => {return err})
      .subscribe(() => {this.getPersons();})
  }
  delGroup_person(id: number): void {
    this.literatureService.delGroup_person(id)
      .catch(err => {return err})
      .subscribe(() => {this.getGroupPersons();})
  }
  delOpus(id: number): void {
    this.literatureService.delOpus(id)
      .catch(err => {return err})
      .subscribe(() => {this.getOpuses();})
  }
  delType_opus(id: number): void {
    this.literatureService.delType_opus(id)
      .catch(err => {return err})
      .subscribe(() => {this.getTypeOpus();})
  }
  delPhrase(id: number): void {
    this.literatureService.delPhrase(id)
      .catch(err => {return err})
      .subscribe(() => {this.getPhrases();})
  }
  delGenre(id: number): void {
    this.literatureService.delGenre(id)
      .catch(err => {return err})
      .subscribe(() => {this.getGenres();})
  }
  delCycle(id: number): void {
    this.literatureService.delCycle(id)
      .catch(err => {return err})
      .subscribe(() => {this.getCycles();})
  }
  delTheme(id: number): void {
    this.literatureService.delTheme(id)
      .catch(err => {return err})
      .subscribe(() => {this.getThemes();})
  }
  delCountry(id: number): void {
    this.literatureService.delCountry(id)
      .catch(err => {return err})
      .subscribe(() => {this.getCountries();})
  }

}