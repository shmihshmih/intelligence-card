import { VisTimelineService, VisTimelineItems } from '../../common/vis-timeline';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { LiteratureService } from '../service/literature.service';

@Component ({
  selector: 'literature-visual',
  templateUrl: './visual.component.html',
  styleUrls: ['./visual.component.scss'],
  providers: [ LiteratureService ]
})

export class LiteratureVisual implements OnInit, OnDestroy{

  public visTimeline: string = 'timelineId1';
  public visTimelineItems: VisTimelineItems;

  constructor( private literatureService: LiteratureService,
               private visTimelineService: VisTimelineService) {}

  ngOnInit():void {
    this.getAllPersons();
  }

  public timelineInitialized(): void {
    this.visTimelineService.on(this.visTimeline, 'click');
    this.visTimelineService.click
      .subscribe((eventData: any[]) => {
        if (eventData[0] === this.visTimeline) {
          console.log(eventData[1]);
        }
      });
  }

  getAllPersons(): void {
    this.literatureService.getAllPersons()
      .subscribe((persons) => {
        console.log(persons);
        this.visTimelineItems = new VisTimelineItems(persons);
      });
  }
  ngOnDestroy() {
    this.visTimelineService.off(this.visTimeline, 'click');
  }
}