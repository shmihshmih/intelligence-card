export class GroupPerson {
    id: number;
    caption: string;
    description: string;
    link: string;
    previous: number;
}