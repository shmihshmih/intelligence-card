export class TypeOpus {
  id: number;
  caption: string;
  description: string;
  link: string;
}