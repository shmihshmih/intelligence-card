export class Cycle {
    id: number;
    caption: string;
    description: string;
    link: string;
}