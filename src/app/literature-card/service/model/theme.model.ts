export class Theme {
    id: number;
    caption: string;
    description: string;
    link: string;
}