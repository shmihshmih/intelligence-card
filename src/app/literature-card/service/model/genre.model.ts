export class Genre {
    id: number;
    caption: string;
    description: string;
}