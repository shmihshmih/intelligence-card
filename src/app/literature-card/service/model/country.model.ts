export class Country {
    id: number;
    caption: string;
    language: string;
}