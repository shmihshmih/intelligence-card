export class Opus {
    id: number;
    pid: number;
    caption: string;
    annotation: string;
    cover: string;
    published: string;
    bibliography: string;
    key_word: string;
    original_lang: string;
    cycle: number;
    cycle_order: number;
    genre: number;
    type: number;
}