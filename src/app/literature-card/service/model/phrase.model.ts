export class Phrase {
    id: number;
    pid: number;
    caption: string;
    original: string;
}