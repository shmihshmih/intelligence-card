import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';

import { Person } from './model/person.model';
import { Country } from './model/country.model';
import { Cycle } from './model/cycle.model';
import { Genre } from './model/genre.model';
import { GroupPerson } from './model/group_person.model';
import { Opus } from './model/opus.model';
import { Phrase } from './model/phrase.model';
import { Theme } from './model/theme.model';
import { TypeOpus } from './model/type_opus.model';

@Injectable()
export class LiteratureService {
  viewPath: string = 'http://localhost/my-app/src/core/literature/view/';
  modelPath: string = 'http://localhost/my-app/src/core/literature/model/';

  constructor(private http: Http) {};

  getCountries(viewName: string): Observable<Country[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getCycles(viewName: string): Observable<Cycle[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getGenres(viewName: string): Observable<Genre[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getGroupPersons(viewName: string): Observable<GroupPerson[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getOpuses(viewName: string): Observable<Opus[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getPersons(viewName: string): Observable<Person[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getPhrases(viewName: string): Observable<Phrase[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getThemes(viewName: string): Observable<Theme[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getTypeOpus(viewName: string): Observable<TypeOpus[]> {
    return this.http
      .get(this.viewPath+viewName)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getLog(): Observable<any[]> {
    return this.http
      .get(this.viewPath+'v_log.php')
      .map(this.extractData)
      .catch(this.handleError);
  }
  //mods
  modCountry(country: Country): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_country.php', { country }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modTheme(theme: Theme): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_theme.php', { theme }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modCycle(cycle: Cycle): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_cycle.php', { cycle }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modGenre(genre: Genre): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_genre.php', { genre }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modPhrase(phrase: Phrase): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_phrase.php', { phrase }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modGroup_person(group_person: GroupPerson): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_group_person.php', { group_person }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modPerson(person: Person): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_person.php', { person }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modOpus(opus: Opus): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_opus.php', { opus }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modType_opus(type_opus: TypeOpus): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_type_opus.php', { type_opus }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  //deletes
  delPerson(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_person.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delGroup_person(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_group_person.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delOpus(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_opus.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delType_opus(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_type_opus.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delPhrase(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_phrase.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delGenre(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_genre.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delCycle(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_cycle.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delTheme(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_theme.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delCountry(del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/literature/model/m_country.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  //visual
  getAllPersons(): Observable<any> {
    return this.http
      .get('http://localhost/my-app/src/core/literature/view/v_visual.php')
      .map(this.extractData)
      .catch(this.handleError);
  }

  //common
  private extractData(res: Response) {
      let body = res.json();
      return body || { };
  }
  private  handleError(error: Response | any) {
      let errMsg: string;
      if (error instanceof Response) {
          const body = error.json() || '';
          const err = body.error || JSON.stringify(body);
          errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
          errMsg = error.message ? error.message : error.toString();
      }
      return Observable.throw(errMsg);
  }
}
