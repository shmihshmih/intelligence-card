"use strict";
var core_1 = require("@angular/core");
var author_mock_1 = require("./mock/author.mock");
var LiteratureService = (function () {
    function LiteratureService() {
    }
    LiteratureService.prototype.getAuthors = function () {
        //return this.authors;
        return Promise.resolve(author_mock_1.AUTHORS);
    };
    return LiteratureService;
}());
LiteratureService = __decorate([
    core_1.Injectable()
], LiteratureService);
exports.LiteratureService = LiteratureService;
//# sourceMappingURL=literature.service.js.map