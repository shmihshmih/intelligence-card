import { Component, OnInit } from '@angular/core';
import { LiteratureService } from '../service/literature.service';

@Component ({
  selector: 'literature-log',
  templateUrl: './log.component.html',
  providers: [LiteratureService]
})
export class LiteratureLog implements OnInit{
  logs: any[];

  constructor(private literatureService: LiteratureService) {};

  ngOnInit() {
    this.getLog();
  };

  getLog(): void {
    this.literatureService.getLog()
      .subscribe(logs => this.logs = logs);
  }
}