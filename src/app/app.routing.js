"use strict";
var router_1 = require("@angular/router");
var admin_component_1 = require("./literature-card/admin/admin.component");
var routes = [
    { path: '', component: admin_component_1.LiteratureAdmin },
    { path: 'literatureAdmin', component: admin_component_1.LiteratureAdmin }
];
exports.routing = router_1.RouterModule.forRoot(routes);
//# sourceMappingURL=app.routing.js.map