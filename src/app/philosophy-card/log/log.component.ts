import { Component, OnInit } from '@angular/core';
import { PhilosophyService } from '../service/philosophy.service';

@Component ({
  templateUrl: './log.component.html',
  providers: [ PhilosophyService ],
  selector: 'philosophy-log'
})
export class PhilosophyLog implements OnInit {
  logs: any[];

  constructor(private philosophyService: PhilosophyService) {}

  ngOnInit() {
    this.getLog();
  }

  getLog(): void {
    this.philosophyService.getLog()
      .subscribe(logs => this.logs = logs);
  }
}