import { Component, OnInit } from '@angular/core';
import { PhilosophyService } from '../service/philosophy.service';

// models
import { CountryGroup } from '../service/model/country_group.model';
import { Country } from '../service/model/country.model';
import { Dictionary } from '../service/model/dictionary.model';
import { Files } from '../service/model/files.model';
import { Idiom } from '../service/model/idiom.model';
import { Opus } from '../service/model/opus.model';
import { Person } from '../service/model/person.model';
import { School } from '../service/model/school.model';
import { Section } from '../service/model/section.model';

@Component({
  selector: 'philosophy-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [PhilosophyService]
})
export class PhilosophyAdmin implements OnInit {
  persons: Person[] = [];
  files: Files[] = [];
  opuses: Opus[] = [];
  idioms: Idiom[] = [];
  dictionaries: Dictionary[] = [];
  country_groups: CountryGroup[] = [];
  countries: Country[] = [];
  schools: School[] = [];
  sections: Section[] = [];
  files_mod: boolean = false;
  idiom_mod: boolean = false;
  country_group_mod: boolean = false;

  person_new: boolean = false;
  person_edit: boolean = true;

  files_new: boolean = false;
  files_edit: boolean = true;
  opus_new: boolean = false;
  opus_edit: boolean = true;
  idiom_edit: boolean = true;
  idiom_new: boolean = false;
  dictionary_new: boolean = false;
  dictionary_edit: boolean = true;
  country_group_new: boolean = false;
  country_group_edit: boolean = true;
  country_new: boolean = false;
  country_edit: boolean = true;
  school_new: boolean = false;
  school_edit: boolean = true;
  section_edit: boolean = true;
  section_new: boolean = false;

  activePerson: Person = new Person();
  activeFiles: Files = new Files();
  activeOpus: Opus = new Opus();
  activeIdiom: Idiom = new Idiom();
  activeDictionary: Dictionary = new Dictionary();
  activeCountryGroup: CountryGroup = new CountryGroup();
  activeCountry: Country = new Country();
  activeSchool: School = new School();
  activeSection: Section = new Section();

  personCountryList: any;
  personPersonList: any;
  personSchoolList: any;
  filesOpusList: any;
  opusSectionList: any;
  dictionaryList: any;
  countryGroupList: any;

  constructor(private philosophyService: PhilosophyService) {}
  //calling the function
  ngOnInit(): void {
    this.getFiles();
    this.getOpuses();
    this.getDictionaries();
    this.getCountryGroups();
    this.getCountries();
    this.getSchools();
    this.getSections();
    this.getIdioms();
    this.getPersons();
    this.getPersonPersonList();
    this.getPersonSchoolList();
  }

  //getters
  getPersons(): void {
    this.getPersonCountryList();
    this.philosophyService.getPersons()
        .subscribe(persons => this.persons = persons);
  }
  getFiles(): void {
    this.getFilesOpusList();
    this.philosophyService.getFiles()
      .subscribe(files => this.files = files);
  }
  getOpuses(): void {
    this.getOpusSectionList();
    this.philosophyService.getOpuses()
        .subscribe(opuses => this.opuses = opuses);
  }
  getDictionaries(): void {
    this.getDictionaryList();
    this.philosophyService.getDictionaries()
      .subscribe(dictionaries => this.dictionaries = dictionaries);
  }
  getCountryGroups(): void {
    this.philosophyService.getCountryGroups()
      .subscribe(country_groups => this.country_groups = country_groups);
  }
  getCountries(): void {
    this.getCountryGroupList();
    this.philosophyService.getCountries()
      .subscribe(countries => this.countries = countries);
  }
  getSchools(): void {
    this.philosophyService.getSchools()
      .subscribe(schools => this.schools = schools);
  }
  getSections(): void {
    this.philosophyService.getSections()
      .subscribe(sections => this.sections = sections);
  }
  getIdioms(): void {
    this.philosophyService.getIdioms()
      .subscribe(idioms => this.idioms = idioms);
  }

  //options
  getPersonCountryList(): void {
    this.philosophyService.getPersonCountryList()
        .subscribe(personCountryList => this.personCountryList = personCountryList);
  }
  getPersonPersonList(): void {
    this.philosophyService.getPersonPersonList()
        .subscribe(personPersonList => this.personPersonList = personPersonList);
  }
  getPersonSchoolList(): void {
    this.philosophyService.getPersonSchoolList()
        .subscribe(personSchoolList => this.personSchoolList = personSchoolList);
  }
  getFilesOpusList(): void {
    this.philosophyService.getFilesOpusList()
        .subscribe(filesOpusList => this.filesOpusList = filesOpusList);
  }
  getOpusSectionList(): void {
    this.philosophyService.getOpusSectionList()
        .subscribe(opusSectionList => this.opusSectionList = opusSectionList);
  }
  getDictionaryList(): void {
    this.philosophyService.getDictionaryList()
        .subscribe(dictionaryList => this.dictionaryList = dictionaryList);
  }
  getCountryGroupList(): void {
    this.philosophyService.getCountryGroupList()
        .subscribe(countryGroupList => this.countryGroupList = countryGroupList);
  }

  //переключалка подформ
  tabForm(el: string): void {
    switch (el) {
      case 'files_mod': this.files_mod = !this.files_mod;break;
      case 'idiom_mod': this.idiom_mod = !this.idiom_mod;break;
      case 'country_group_mod': this.country_group_mod = !this.country_group_mod;break;
    }
  }

  //editors
  editPerson(identificator: number): void {
    this.philosophyService.getPerson(identificator)
        .subscribe(person => this.activePerson = person as Person);
  }
  editFiles(identificator: number): void {
    this.philosophyService.getFile(identificator)
        .subscribe(file => this.activeFiles = file as Files);
  }
  editOpus(identificator: number): void {
    this.philosophyService.getOpus(identificator)
        .subscribe(opus => this.activeOpus = opus as Opus);
  }
  editDictionary(identificator: number): void {
    this.philosophyService.getDictionary(identificator)
        .subscribe(dictionary => this.activeDictionary = dictionary as Dictionary);
  }
  editCountry_group(identificator: number): void {
    this.philosophyService.getCountryGroup(identificator)
        .subscribe(countryGroup => this.activeCountryGroup = countryGroup as CountryGroup);
  }
  editCountry(identificator: number): void {
    this.philosophyService.getCountry(identificator)
        .subscribe(country => this.activeCountry = country as Country);
  }
  editIdiom(identificator: number): void {
    this.philosophyService.getIdiom(identificator)
        .subscribe(idiom => this.activeIdiom = idiom as Idiom);
  }
  editSchool(identificator: number): void {
    this.philosophyService.getSchool(identificator)
        .subscribe(school => this.activeSchool = school as School);
  }
  editSection(identificator: number): void {
    this.philosophyService.getSection(identificator)
        .subscribe(section => this.activeSection = section as Section);
  }

  //deletes
  delPerson(identificator: number): void {
    this.philosophyService.delPerson(identificator)
      .subscribe(data => {console.log(data);this.getPersons();});
  }
  delFiles(identificator: number): void {
    this.philosophyService.delFiles(identificator)
      .subscribe(data => {console.log(data);this.getFiles();});
  }
  delOpus(identificator: number): void {
    this.philosophyService.delOpus(identificator)
      .subscribe(data => {console.log(data);this.getOpuses();});
  }
  delIdiom(identificator: number): void {
    this.philosophyService.delIdiom(identificator)
      .subscribe(data => {console.log(data);this.getIdioms();});
  }
  delDictionary(identificator: number): void {
    this.philosophyService.delDictionary(identificator)
      .subscribe(data => {console.log(data);this.getDictionaries();});
  }
  delCountry_group(identificator: number): void {
    this.philosophyService.delCountry_group(identificator)
      .subscribe(data => {console.log(data);this.getCountryGroups();});
  }
  delCountry(identificator: number): void {
    this.philosophyService.delCountry(identificator)
      .subscribe(data => {console.log(data);this.getCountries();});
  }
  delSchool(identificator: number): void {
    this.philosophyService.delSchool(identificator)
      .subscribe(data => {console.log(data);this.getSchools();});
  }
  delSection(identificator: number): void {
    this.philosophyService.delSection(identificator)
      .subscribe(data => {console.log(data);this.getSections();});
  }

  //submits
  personSubmit(): void {
    this.philosophyService.modPerson(this.activePerson)
      .subscribe(data => console.log(data));
  }
  filesSubmit(): void {
    this.philosophyService.modFiles(this.activeFiles)
      .subscribe(data => console.log(data));
  }
  idiomSubmit(): void {
    this.philosophyService.modIdiom(this.activeIdiom)
      .subscribe(data => console.log(data));
  }
  countryGroupSubmit(): void {
    this.philosophyService.modCountryGroup(this.activeCountryGroup)
      .subscribe(data => console.log(data));
  }
  countrySubmit(): void {
    this.philosophyService.modCountry(this.activeCountry)
        .subscribe(data => console.log(data));
  }
  schoolSubmit(): void {
    this.philosophyService.modSchool(this.activeSchool)
      .subscribe(data => console.log(data));
  }
  sectionSubmit(): void {
    this.philosophyService.modSection(this.activeSection)
      .subscribe(data => console.log(data));
  }
  opusSubmit(): void {
    this.philosophyService.modOpus(this.activeOpus)
      .subscribe(data => console.log(data));
  }
  dictionarySubmit(): void {
    this.philosophyService.modDictionary(this.activeDictionary)
      .subscribe(data => console.log(data));
  }

}
