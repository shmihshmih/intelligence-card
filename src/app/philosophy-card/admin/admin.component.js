"use strict";
var core_1 = require("@angular/core");
var philosophy_service_1 = require("../service/philosophy.service");
var PhilosophyAdminComponent = (function () {
    function PhilosophyAdminComponent(philosophyService) {
        this.philosophyService = philosophyService;
    }
    PhilosophyAdminComponent.prototype.ngOnInit = function () {
        console.log(this.philosophyService);
        this.getPersons();
    };
    PhilosophyAdminComponent.prototype.getPersons = function () {
        var _this = this;
        return this.philosophyService.getPersons().
            then(function (persons) { return _this.persons = persons; });
    };
    return PhilosophyAdminComponent;
}());
PhilosophyAdminComponent = __decorate([
    core_1.Component({
        selector: 'philosophy-admin',
        templateUrl: './admin.component.html',
        styleUrls: ['admin.component.scss'],
        providers: [philosophy_service_1.PhilosophyService]
    }),
    __metadata("design:paramtypes", [philosophy_service_1.PhilosophyService])
], PhilosophyAdminComponent);
exports.PhilosophyAdminComponent = PhilosophyAdminComponent;
//# sourceMappingURL=admin.component.js.map