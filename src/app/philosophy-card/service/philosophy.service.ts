import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { CountryGroup } from './model/country_group.model';
import { Country } from './model/country.model';
import { Dictionary } from './model/dictionary.model';
import { Files } from './model/files.model';
import { Idiom } from './model/idiom.model';
import { Opus } from './model/opus.model';
import { Person } from './model/person.model';
import { School } from './model/school.model';
import { Section } from './model/section.model';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

@Injectable()
export class PhilosophyService {
  constructor (private http: Http){};

  //getters
  getCountryGroups(): Observable<CountryGroup[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_country_group.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getCountryGroup(id: number): Observable<CountryGroup> {
    return this.getCountryGroups()
        .map(countryGroups => countryGroups.filter(countryGroup => countryGroup.id === id)[0]);
  }
  getCountries(): Observable<Country[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_country.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getCountry(id: number): Observable<Country> {
    return this.getCountries()
        .map(countries => countries.filter(country => country.id === id)[0]);
  }
  getDictionaries(): Observable<Dictionary[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_dictionary.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getDictionary(id: number): Observable<Dictionary> {
    return this.getDictionaries()
        .map(dictionaries => dictionaries.filter(dictionary => dictionary.id === id)[0]);
  }
  getFiles(): Observable<Files[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_files.php')
        .map(this.extractData)
        .catch(this.handleError)
  }
  getFile(id: number): Observable<Files> {
    return this.getFiles()
        .map(files => files.filter(file => file.id === id)[0]);
  }
  getIdioms(): Observable<Idiom[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_idiom.php')
        .map(this.extractData)
        .catch(this.handleError)
  }
  getIdiom(id: number): Observable<Idiom> {
    return this.getIdioms()
        .map(idioms => idioms.filter(idiom => idiom.id === id)[0]);
  }
  getOpuses(): Observable<Opus[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_opus.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getOpus(id: number): Observable<Opus> {
    return this.getOpuses()
        .map(opuses => opuses.filter(opus => opus.id === id)[0]);
  }
  getPersons(): Observable<Person[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_person.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getPerson(id: number): Observable<Person> {
    return this.getPersons()
        .map(persons => persons.filter(person => person.id === id)[0]);
  }
  getSchools(): Observable<School[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_school.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getSchool(id: number): Observable<School> {
    return this.getSchools()
        .map(schools => schools.filter(school => school.id === id)[0]);
  }
  getSections(): Observable<Section[]> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_section.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getSection(id: number): Observable<Section> {
    return this.getSections()
        .map(sections => sections.filter(section => section.id === id)[0]);
  }
  getPersonCountryList(): Observable<any> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_person_country_list.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getPersonPersonList(): Observable<any> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_person_person_list.php')
        .map(this.extractData)
        .catch(this.handleError)
  }
  getPersonSchoolList(): Observable<any> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_person_school_list.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getFilesOpusList(): Observable<any> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_files_opus.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getOpusSectionList(): Observable<any> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_opus_section.php')
        .map(this.extractData)
        .catch(this.handleError)
  }
  getDictionaryList(): Observable<any> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_dictionary_list.php')
        .map(this.extractData)
        .catch(this.handleError);
  }
  getCountryGroupList(): Observable<CountryGroup> {
    return this.http
        .get('http://localhost/my-app/src/core/view/v_country_country_group.php')
        .map(this.extractData)
        .catch(this.handleError);
  }

  //editors
  modCountry(country: Country): Observable<any> {
    let headers = new Headers({'content-type':'application/json;charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/model/m_country.php', { country }, options)
      .map(this.extractData)
      .catch(this.handleError)
  }
  modPerson(person: Person): Observable<any> {
    let headers = new Headers({'content-type': 'application/json; charset=utf-8'});
    let options = new RequestOptions({headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/model/m_person.php', { person }, options)
      .map(this.extractData)
      .catch(this.handleError)
  }
  modFiles(files: Files): Observable<any> {
    let headers = new Headers({'content-type': 'application/json;charset=utf-8'});
    let options = new RequestOptions({ headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/model/m_files.php', { files } , options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modOpus(opus: Opus): Observable<any> {
    let headers = new Headers({'content-type': 'application/json;charset=utf-8;'});
    let options = new RequestOptions({ headers:headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_opus.php', { opus }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modIdiom(idiom: Idiom): Observable<any> {
    let headers = new Headers({'content-type': 'application/json;charset=utf-8'});
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_idiom.php', { idiom } ,options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modDictionary(dictionary: Dictionary): Observable<any> {
    let headers = new Headers({ 'content-type': 'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_dictionary.php', { dictionary }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modCountryGroup( countryGroup: CountryGroup): Observable<any> {
    let headers = new Headers({ 'content-type': 'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_country_group.php', { countryGroup }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modSchool( school: School ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_school.php', { school }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  modSection( section: Section): Observable<any> {
    let headers = new Headers({ 'content-type': 'application/json;charset=utf-8;' });
    let options = new RequestOptions({ headers: headers});
    return this.http
      .post('http://localhost/my-app/src/core/model/m_section.php', { section }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  //deletes
  delPerson( del: number): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_person.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delCountry( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_country.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delDictionary( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_dictionary.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delFiles( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_files.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delOpus( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_opus.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delIdiom( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_idiom.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delCountry_group( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_country_group.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delSchool( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_school.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  delSection( del: number ): Observable<any> {
    let headers = new Headers({ 'content-type':'application/json;charset=utf-8' });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post('http://localhost/my-app/src/core/model/m_section.php', { del }, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getLog(): Observable<any> {
    return this.http
      .get('http://localhost/my-app/src/core/view/v_log.php')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getAllPersons(): Observable<any> {
    return this.http
      .get('http://localhost/my-app/src/core/view/v_visual.php')
      .map(this.extractData)
      .catch(this.handleError);
  }

  //common
  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }
  private  handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
