import { CountryGroup } from '../model/country_group.model';

export const countryGroups: CountryGroup[] = [
  {id: 1, caption: 'Срединоземноморье'},
  {id: 2, caption: 'Средняя Азия'}
]
