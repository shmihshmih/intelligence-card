import { Person } from '../model/person.model';

export const persons: Person[] = [
  {id: 1, name: 'fsd1', surname: 'wer', last_name: 'sdfs', birth_date: 'asd', death_date: 'dfgdf', country: 2, parent: 2, school:5,key_word: 'sfd', annotation: 'wefw', link: 'wefwe',portrait: 'sfds', bibliography: 'sfdfsd'},
  {id: 2, name: 'fsd2', surname: 'wer', last_name: 'sdfs', birth_date: 'asd', death_date: 'dfgdf', country: 2, parent: 2, school:5,key_word: 'sfd', annotation: 'wefw', link: 'wefwe',portrait: 'sfds', bibliography: 'sfdfsd'},
  {id: 3, name: 'fsd3', surname: 'wer', last_name: 'sdfs', birth_date: 'asd', death_date: 'dfgdf', country: 2, parent: 2, school:5,key_word: 'sfd', annotation: 'wefw', link: 'wefwe',portrait: 'sfds', bibliography: 'sfdfsd'},
]
