export class Opus{
  id: number;
  pid: number;
  caption: string;
  annotation: string;
  cover: string;
  published: string;
  bibliography: string;
  key_words: string;
  section: number;
}
