export class School {
  id: number;
  school: string;
  annotation: string;
  parent: number;
  bibliography: string;
}
