export class Section {
  id: number;
  pid: number;
  caption: string;
  annotation: string;
}
