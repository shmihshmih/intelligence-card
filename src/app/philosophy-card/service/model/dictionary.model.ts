export class Dictionary {
  id: number;
  pid: number;
  parent: number;
  caption: string;
  description: string;
  bibliography: string;
  foreign_lang: string;
}
