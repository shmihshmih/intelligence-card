export class Idiom {
  id: number;
  pid: number;
  idiom: string;
  original: string;
}
