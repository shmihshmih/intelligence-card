export class Files {
  id: number;
  pid: number;
  caption: string;
  author: string;
  link: string;
  add_date: string;
  visible: boolean;
  bibliography: string;
  type: string;
  key_word: string;
  format: string;
}
