export class Country {
  id: number;
  pid: number;
  country: string;
  language: string;
}
