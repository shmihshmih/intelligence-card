export class Person {
  id: number;
  name: string;
  surname: string;
  last_name: string;
  birth_date: string;
  death_date: string;
  country: number;
  parent: number;
  school: number;
  key_word: string;
  annotation: string;
  link: string;
  portrait: string;
  bibliography: string;
}
