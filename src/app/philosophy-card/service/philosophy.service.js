"use strict";
var core_1 = require("@angular/core");
var country_group_mock_1 = require("./mock/country_group.mock");
var country_mock_1 = require("./mock/country.mock");
var dictionary_mock_1 = require("./mock/dictionary.mock");
var files_mock_1 = require("./mock/files.mock");
var idiom_mock_1 = require("./mock/idiom.mock");
var opus_mock_1 = require("./mock/opus.mock");
var person_mock_1 = require("./mock/person.mock");
var school_mock_1 = require("./mock/school.mock");
var section_mock_1 = require("./mock/section.mock");
var PhilosophyService = (function () {
    function PhilosophyService() {
    }
    PhilosophyService.prototype.getCountryGroups = function () {
        console.log('cg');
        return Promise.resolve(country_group_mock_1.countryGroups);
    };
    PhilosophyService.prototype.getCountries = function () {
        return Promise.resolve(country_mock_1.countryes);
    };
    PhilosophyService.prototype.getDictionaries = function () {
        return Promise.resolve(dictionary_mock_1.dictionaries);
    };
    PhilosophyService.prototype.getFiles = function () {
        return Promise.resolve(files_mock_1.filess);
    };
    PhilosophyService.prototype.getIdioms = function () {
        return Promise.resolve(idiom_mock_1.idioms);
    };
    PhilosophyService.prototype.getOpuses = function () {
        return Promise.resolve(opus_mock_1.opuses);
    };
    PhilosophyService.prototype.getPersons = function () {
        return Promise.resolve(person_mock_1.persons);
    };
    PhilosophyService.prototype.getSchools = function () {
        return Promise.resolve(school_mock_1.schools);
    };
    PhilosophyService.prototype.getSections = function () {
        return Promise.resolve(section_mock_1.sections);
    };
    return PhilosophyService;
}());
PhilosophyService = __decorate([
    core_1.Injectable()
], PhilosophyService);
exports.PhilosophyService = PhilosophyService;
//# sourceMappingURL=philosophy.service.js.map