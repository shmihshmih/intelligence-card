--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

-- Started on 2017-05-08 03:12:26 MSK

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 9 (class 2615 OID 16392)
-- Name: core; Type: SCHEMA; Schema: -; Owner: shmihshmih
--

CREATE SCHEMA core;


ALTER SCHEMA core OWNER TO shmihshmih;

--
-- TOC entry 8 (class 2615 OID 16391)
-- Name: literature; Type: SCHEMA; Schema: -; Owner: shmihshmih
--

CREATE SCHEMA literature;


ALTER SCHEMA literature OWNER TO shmihshmih;

--
-- TOC entry 1 (class 3079 OID 13310)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3296 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = core, pg_catalog;

--
-- TOC entry 209 (class 1255 OID 27690)
-- Name: f_gen_id(); Type: FUNCTION; Schema: core; Owner: shmihshmih
--

CREATE FUNCTION f_gen_id() RETURNS bigint
    LANGUAGE sql
    AS $$
select nextval('core.main_seq')::bigint;
$$;


ALTER FUNCTION core.f_gen_id() OWNER TO shmihshmih;

SET search_path = literature, pg_catalog;

--
-- TOC entry 206 (class 1255 OID 27683)
-- Name: f_country8add(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_country8add(pn_id bigint, pv_caption character varying, pv_language character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  INSERT INTO 
  literature.t_country
(
  id,
  caption,
  language
)
VALUES (
  pn_id,
  pv_caption,
  pv_language
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_country8add(pn_id bigint, pv_caption character varying, pv_language character varying) OWNER TO shmihshmih;

--
-- TOC entry 207 (class 1255 OID 27684)
-- Name: f_country8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_country8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_country 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_country8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 222 (class 1255 OID 27694)
-- Name: f_country8mod(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_country8mod(pn_id bigint, pv_caption character varying, pv_language character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
	n_id  bigint;
BEGIN
	if pn_id is null then 
    	n_id := literature.f_country8add(
    	pn_id := core.f_gen_id(),
        pv_caption := pv_caption,
        pv_language := pv_language);
	  else 
      	n_id := literature.f_country8upd(
      	pn_id := pn_id,
        pv_caption := pv_caption,
        pv_language := pv_language);
    end if;
	return n_id;
END;
$$;


ALTER FUNCTION literature.f_country8mod(pn_id bigint, pv_caption character varying, pv_language character varying) OWNER TO shmihshmih;

--
-- TOC entry 208 (class 1255 OID 27685)
-- Name: f_country8upd(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_country8upd(pn_id bigint, pv_caption character varying, pv_language character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_country 
SET 
  caption = pv_caption,
  language = pv_language
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_country8upd(pn_id bigint, pv_caption character varying, pv_language character varying) OWNER TO shmihshmih;

--
-- TOC entry 223 (class 1255 OID 27695)
-- Name: f_cycle8add(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_cycle8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_cycle
(
  id,
  caption,
  description,
  link
)
VALUES (
  pn_id,
  pv_caption,
  pv_description,
  pv_link
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_cycle8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO shmihshmih;

--
-- TOC entry 225 (class 1255 OID 27697)
-- Name: f_cycle8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_cycle8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_cycle 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_cycle8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 226 (class 1255 OID 27698)
-- Name: f_cycle8mod(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_cycle8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then 
  	n_id := literature.f_cycle8add(
    	pn_id := core.f_gen_id(),
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_link := pv_link);
   else
    n_id := literature.f_cycle8upd(
    	pn_id := pn_id,
        pv_caption := pv_caption,
        pv_description := pv_description,
        pv_link := pv_link);
   end if;
   return n_id;
END;
$$;


ALTER FUNCTION literature.f_cycle8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO shmihshmih;

--
-- TOC entry 224 (class 1255 OID 27696)
-- Name: f_cycle8upd(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_cycle8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_cycle 
SET 
  caption = pv_caption,
  description = pv_description,
  link = pv_link
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_cycle8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO shmihshmih;

--
-- TOC entry 230 (class 1255 OID 27699)
-- Name: f_genre8add(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_genre8add(pn_id bigint, pv_caption character varying, pv_description character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_genre
(
  id,
  caption,
  description
)
VALUES (
  pn_id,
  pv_caption,
  pv_description
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_genre8add(pn_id bigint, pv_caption character varying, pv_description character varying) OWNER TO shmihshmih;

--
-- TOC entry 228 (class 1255 OID 27701)
-- Name: f_genre8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_genre8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_genre 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_genre8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 229 (class 1255 OID 27702)
-- Name: f_genre8mod(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_genre8mod(pn_id bigint, pv_caption character varying, pv_description character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
	if pn_id is null then
    	n_id := literature.f_genre8add(
        	pn_id := core.f_gen_id(),
            pv_caption := pv_caption,
            pv_description := pv_description);
      else 
      	n_id := literature.f_genre8upd(
        	pn_id := pn_id,
            pv_caption := pv_caption,
            pv_description := pv_description);
    end if;
    return n_id;
END;
$$;


ALTER FUNCTION literature.f_genre8mod(pn_id bigint, pv_caption character varying, pv_description character varying) OWNER TO shmihshmih;

--
-- TOC entry 227 (class 1255 OID 27700)
-- Name: f_genre8upd(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_genre8upd(pn_id bigint, pv_caption character varying, pv_description character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_genre 
SET 
  caption = pv_caption,
  description = pv_description
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_genre8upd(pn_id bigint, pv_caption character varying, pv_description character varying) OWNER TO shmihshmih;

--
-- TOC entry 231 (class 1255 OID 27703)
-- Name: f_group_person8add(bigint, character varying, character varying, character varying, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_group_person8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying, pn_previous bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_group_person
(
  id,
  caption,
  description,
  link,
  previous
)
VALUES (
  pn_id,
  pv_caption,
  pv_description,
  pv_link,
  pn_previous
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_group_person8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying, pn_previous bigint) OWNER TO shmihshmih;

--
-- TOC entry 234 (class 1255 OID 27707)
-- Name: f_group_person8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_group_person8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_group_person 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_group_person8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 233 (class 1255 OID 27706)
-- Name: f_group_person8mod(bigint, character varying, character varying, character varying, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_group_person8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying, pn_previous bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then 
    n_id := literature.f_group_person8add(
      pn_id := core.f_gen_id(),
      pv_caption := pv_caption,
      pv_description := pv_description,
      pv_link := pv_link,
      pn_previous := pn_previous);
   else
    n_id := literature.f_group_person8upd(
      pn_id := pn_id,
      pv_caption := pv_caption,
      pv_description := pv_description,
      pv_link := pv_link,
      pn_previous := pn_previous);
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION literature.f_group_person8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying, pn_previous bigint) OWNER TO shmihshmih;

--
-- TOC entry 232 (class 1255 OID 27704)
-- Name: f_group_person8upd(bigint, character varying, character varying, character varying, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_group_person8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying, pn_previous bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_group_person 
SET 
  caption = pv_caption,
  description = pv_description,
  link = pv_link,
  previous = pn_previous
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_group_person8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying, pn_previous bigint) OWNER TO shmihshmih;

--
-- TOC entry 258 (class 1255 OID 27759)
-- Name: f_log8add(bigint, bigint, character varying, timestamp without time zone, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_log8add(pn_id bigint, pn_pid bigint, pv_changed_table character varying, pd_timed timestamp without time zone, pv_action_type character varying, pv_revert character varying, pv_message character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_log
(
  id,
  pid,
  changed_table,
  timed,
  action_type,
  revert,
  message
)
VALUES (
  pn_id,
  pn_pid,
  pv_changed_table,
  pd_timed,
  pv_action_type,
  pv_revert,
  pv_message
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_log8add(pn_id bigint, pn_pid bigint, pv_changed_table character varying, pd_timed timestamp without time zone, pv_action_type character varying, pv_revert character varying, pv_message character varying) OWNER TO shmihshmih;

--
-- TOC entry 260 (class 1255 OID 27761)
-- Name: f_log8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_log8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_log 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_log8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 259 (class 1255 OID 27760)
-- Name: f_log8upd(bigint, bigint, character varying, timestamp without time zone, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_log8upd(pn_id bigint, pn_pid bigint, pv_changed_table character varying, pd_timed timestamp without time zone, pv_action_type character varying, pv_revert character varying, pv_message character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_log 
SET 
  pid = pn_pid,
  changed_table = pv_changed_table,
  timed = pd_timed,
  action_type = pv_action_type,
  revert = pv_revert,
  message = pv_message
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_log8upd(pn_id bigint, pn_pid bigint, pv_changed_table character varying, pd_timed timestamp without time zone, pv_action_type character varying, pv_revert character varying, pv_message character varying) OWNER TO shmihshmih;

--
-- TOC entry 235 (class 1255 OID 27708)
-- Name: f_opus8add(bigint, bigint, character varying, character varying, character varying, date, character varying, character varying, character varying, bigint, bigint, bigint, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus8add(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_annotation character varying, pv_cover character varying, pd_published date, pv_bibliography character varying, pv_key_word character varying, pv_original_lang character varying, pn_cycle bigint, pn_cycle_order bigint, pn_genre bigint, pn_type bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_opus
(
  id,
  pid,
  caption,
  annotation,
  cover,
  published,
  bibliography,
  key_word,
  original_lang,
  cycle,
  cycle_order,
  genre,
  type
)
VALUES (
  pn_id,
  pn_pid,
  pv_caption,
  pv_annotation,
  pv_cover,
  pd_published,
  pv_bibliography,
  pv_key_word,
  pv_original_lang,
  pn_cycle,
  pn_cycle_order,
  pn_genre,
  pn_type
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_opus8add(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_annotation character varying, pv_cover character varying, pd_published date, pv_bibliography character varying, pv_key_word character varying, pv_original_lang character varying, pn_cycle bigint, pn_cycle_order bigint, pn_genre bigint, pn_type bigint) OWNER TO shmihshmih;

--
-- TOC entry 239 (class 1255 OID 27711)
-- Name: f_opus8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_opus 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_opus8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 238 (class 1255 OID 27710)
-- Name: f_opus8mod(bigint, bigint, character varying, character varying, character varying, date, character varying, character varying, character varying, bigint, bigint, bigint, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus8mod(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_annotation character varying, pv_cover character varying, pd_published date, pv_bibliography character varying, pv_key_word character varying, pv_original_lang character varying, pn_cycle bigint, pn_cycle_order bigint, pn_genre bigint, pn_type bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null THEN
    n_id := literature.f_opus8add(
      pn_id := core.f_gen_id(),
      pn_pid := pn_pid,
      pv_caption := pv_caption,
      pv_annotation := pv_annotation,
      pv_cover := pv_cover,
      pd_published := pd_published,
      pv_bibliography := pv_bibliography,
      pv_key_word := pv_key_word,
      pv_original_lang := pv_original_lang,
      pn_cycle := pn_cycle,
      pn_cycle_order := pn_cycle_order,
      pn_genre := pn_genre,
      pn_type := pn_type);
   else 
    n_id := literature.f_opus8upd(
      pn_id := pn_id,
      pn_pid := pn_pid,
      pv_caption := pv_caption,
      pv_annotation := pv_annotation,
      pv_cover := pv_cover,
      pd_published := pd_published,
      pv_bibliography := pv_bibliography,
      pv_key_word := pv_key_word,
      pv_original_lang := pv_original_lang,
      pn_cycle := pn_cycle,
      pn_cycle_order := pn_cycle_order,
      pn_genre := pn_genre,
      pn_type := pn_type);
   end if;
   return n_id;
END;
$$;


ALTER FUNCTION literature.f_opus8mod(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_annotation character varying, pv_cover character varying, pd_published date, pv_bibliography character varying, pv_key_word character varying, pv_original_lang character varying, pn_cycle bigint, pn_cycle_order bigint, pn_genre bigint, pn_type bigint) OWNER TO shmihshmih;

--
-- TOC entry 237 (class 1255 OID 27709)
-- Name: f_opus8upd(bigint, bigint, character varying, character varying, character varying, date, character varying, character varying, character varying, bigint, bigint, bigint, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus8upd(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_annotation character varying, pv_cover character varying, pd_published date, pv_bibliography character varying, pv_key_word character varying, pv_original_lang character varying, pn_cycle bigint, pn_cycle_order bigint, pn_genre bigint, pn_type bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_opus 
SET 
  pid = pn_pid,
  caption = pv_caption,
  annotation = pv_annotation,
  cover = pv_cover,
  published = pd_published,
  bibliography = pv_bibliography,
  key_word = pv_key_word,
  original_lang = pv_original_lang,
  cycle = pn_cycle,
  cycle_order = pn_cycle_order,
  genre = pn_genre,
  type = pn_type
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_opus8upd(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_annotation character varying, pv_cover character varying, pd_published date, pv_bibliography character varying, pv_key_word character varying, pv_original_lang character varying, pn_cycle bigint, pn_cycle_order bigint, pn_genre bigint, pn_type bigint) OWNER TO shmihshmih;

--
-- TOC entry 240 (class 1255 OID 27712)
-- Name: f_opus_theme8add(bigint, bigint, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus_theme8add(pn_id bigint, pn_opus bigint, pn_theme bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_opus_theme
(
  id,
  opus,
  theme
)
VALUES (
  pn_id,
  pn_opus,
  pn_theme
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_opus_theme8add(pn_id bigint, pn_opus bigint, pn_theme bigint) OWNER TO shmihshmih;

--
-- TOC entry 243 (class 1255 OID 27716)
-- Name: f_opus_theme8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus_theme8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_opus_theme 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_opus_theme8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 242 (class 1255 OID 27715)
-- Name: f_opus_theme8mod(bigint, bigint, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus_theme8mod(pn_id bigint, pn_opus bigint, pn_theme bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := literature.f_opus_theme8add(
      pn_id := core.f_gen_id(),
      pn_opus := pn_opus,
      pn_theme := pn_theme);
   else
    n_id := literature.f_opus_theme8upd(
      pn_id := pn_id,
      pn_opus := pn_opus,
      pn_theme := pn_theme);
  end if;
  return pn_id;
END;
$$;


ALTER FUNCTION literature.f_opus_theme8mod(pn_id bigint, pn_opus bigint, pn_theme bigint) OWNER TO shmihshmih;

--
-- TOC entry 241 (class 1255 OID 27713)
-- Name: f_opus_theme8upd(bigint, bigint, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_opus_theme8upd(pn_id bigint, pn_opus bigint, pn_theme bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_opus_theme 
SET 
  opus = pn_opus,
  theme = pn_theme
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_opus_theme8upd(pn_id bigint, pn_opus bigint, pn_theme bigint) OWNER TO shmihshmih;

--
-- TOC entry 244 (class 1255 OID 27717)
-- Name: f_person8add(bigint, character varying, character varying, character varying, date, date, bigint, character varying, character varying, character varying, character varying, character varying, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_person8add(pn_id bigint, pv_name character varying, pv_surname character varying, pv_last_name character varying, pd_birth_date date, pd_death_date date, pn_country bigint, pv_key_word character varying, pv_annotation character varying, pv_link character varying, pv_portrait character varying, pv_bibliography character varying, pn_group_person bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_person
(
  id,
  name,
  surname,
  last_name,
  birth_date,
  death_date,
  country,
  key_word,
  annotation,
  link,
  portrait,
  bibliography,
  group_person
)
VALUES (
  pn_id,
  pv_name,
  pv_surname,
  pv_last_name,
  pd_birth_date,
  pd_death_date,
  pn_country,
  pv_key_word,
  pv_annotation,
  pv_link,
  pv_portrait,
  pv_bibliography,
  pn_group_person
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_person8add(pn_id bigint, pv_name character varying, pv_surname character varying, pv_last_name character varying, pd_birth_date date, pd_death_date date, pn_country bigint, pv_key_word character varying, pv_annotation character varying, pv_link character varying, pv_portrait character varying, pv_bibliography character varying, pn_group_person bigint) OWNER TO shmihshmih;

--
-- TOC entry 246 (class 1255 OID 27723)
-- Name: f_person8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_person8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_person 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_person8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 245 (class 1255 OID 27722)
-- Name: f_person8mod(bigint, character varying, character varying, character varying, date, date, bigint, character varying, character varying, character varying, character varying, character varying, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_person8mod(pn_id bigint, pv_name character varying, pv_surname character varying, pv_last_name character varying, pd_birth_date date, pd_death_date date, pn_country bigint, pv_key_word character varying, pv_annotation character varying, pv_link character varying, pv_portrait character varying, pv_bibliography character varying, pn_group_person bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := literature.f_person8add(
      pn_id := core.f_gen_id(),
      pv_name := pv_name,
      pv_surname := pv_surname,
      pv_last_name := pv_last_name,
      pd_birth_date := pd_birth_date,
      pd_death_date := pd_death_date,
      pn_country := pn_country,
      pv_key_word := pv_key_word,
      pv_annotation := pv_annotation,
      pv_link := pv_link,
      pv_portrait := pv_portrait,
      pv_bibliography := pv_bibliography,
      pn_group_person := pn_group_person);
   else 
    n_id := literature.f_person8upd(
      pn_id := pn_id,
  	  pv_name := pv_name,
      pv_surname := pv_surname,
      pv_last_name := pv_last_name,
      pd_birth_date := pd_birth_date,
      pd_death_date := pd_death_date,
      pn_country := pn_country,
      pv_key_word := pv_key_word,
      pv_annotation := pv_annotation,
      pv_link := pv_link,
      pv_portrait := pv_portrait,
      pv_bibliography := pv_bibliography,
      pn_group_person := pn_group_person
    );
   end if;
  return n_id;
END;
$$;


ALTER FUNCTION literature.f_person8mod(pn_id bigint, pv_name character varying, pv_surname character varying, pv_last_name character varying, pd_birth_date date, pd_death_date date, pn_country bigint, pv_key_word character varying, pv_annotation character varying, pv_link character varying, pv_portrait character varying, pv_bibliography character varying, pn_group_person bigint) OWNER TO shmihshmih;

--
-- TOC entry 261 (class 1255 OID 27791)
-- Name: f_person8upd(bigint, character varying, character varying, character varying, date, date, bigint, character varying, character varying, character varying, character varying, character varying, bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_person8upd(pn_id bigint, pv_name character varying, pv_surname character varying, pv_last_name character varying, pd_birth_date date, pd_death_date date, pn_country bigint, pv_key_word character varying, pv_annotation character varying, pv_link character varying, pv_portrait character varying, pv_bibliography character varying, pn_group_person bigint) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_person 
SET 
  name = pv_name,
  surname = pv_surname,
  last_name = pv_last_name,
  birth_date = pd_birth_date,
  death_date = pd_death_date,
  country = pn_country,
  key_word = pv_key_word,
  annotation = pv_annotation,
  link = pv_link,
  portrait = pv_portrait,
  bibliography = pv_bibliography,
  group_person = pn_group_person
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_person8upd(pn_id bigint, pv_name character varying, pv_surname character varying, pv_last_name character varying, pd_birth_date date, pd_death_date date, pn_country bigint, pv_key_word character varying, pv_annotation character varying, pv_link character varying, pv_portrait character varying, pv_bibliography character varying, pn_group_person bigint) OWNER TO shmihshmih;

--
-- TOC entry 247 (class 1255 OID 27725)
-- Name: f_phrase8add(bigint, bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_phrase8add(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_original character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_phrase
(
  id,
  pid,
  caption,
  original
)
VALUES (
  pn_id,
  pn_pid,
  pv_caption,
  pv_original
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_phrase8add(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_original character varying) OWNER TO shmihshmih;

--
-- TOC entry 250 (class 1255 OID 27729)
-- Name: f_phrase8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_phrase8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_phrase 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_phrase8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 249 (class 1255 OID 27728)
-- Name: f_phrase8mod(bigint, bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_phrase8mod(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_original character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then 
    n_id := literature.f_phrase8add(
      pn_id := core.f_gen_id(),
      pn_pid := pn_pid,
      pv_caption := pv_caption,
      pv_original := pv_original
      );
   else 
    n_id := literature.f_phrase8upd(
      pn_id := pn_id,
      pn_pid := pn_pid,
      pv_caption := pv_caption,
      pv_original := pv_original);
  end if;
  return pn_id;
END;
$$;


ALTER FUNCTION literature.f_phrase8mod(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_original character varying) OWNER TO shmihshmih;

--
-- TOC entry 248 (class 1255 OID 27726)
-- Name: f_phrase8upd(bigint, bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_phrase8upd(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_original character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_phrase 
SET 
  pid = pn_pid,
  caption = pv_caption,
  original = pv_original
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_phrase8upd(pn_id bigint, pn_pid bigint, pv_caption character varying, pv_original character varying) OWNER TO shmihshmih;

--
-- TOC entry 251 (class 1255 OID 27730)
-- Name: f_theme8add(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_theme8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
INSERT INTO 
  literature.t_theme
(
  id,
  caption,
  description,
  link
)
VALUES (
  pn_id,
  pv_caption,
  pv_description,
  pv_link
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_theme8add(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO shmihshmih;

--
-- TOC entry 254 (class 1255 OID 27733)
-- Name: f_theme8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_theme8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_theme 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_theme8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 253 (class 1255 OID 27732)
-- Name: f_theme8mod(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_theme8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then 
    n_id := literature.f_theme8add(
      pn_id := core.f_gen_id(),
      pv_caption := pv_caption,
      pv_description := pv_description,
      pv_link := pv_link);
   ELSE
    n_id := literature.f_theme8upd(
      pn_id := pn_id,
      pv_caption := pv_caption,
      pv_description := pv_description,
      pv_link := pv_link);
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION literature.f_theme8mod(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO shmihshmih;

--
-- TOC entry 252 (class 1255 OID 27731)
-- Name: f_theme8upd(bigint, character varying, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_theme8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_theme 
SET 
  caption = pv_caption,
  description = pv_description,
  link = pv_link
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_theme8upd(pn_id bigint, pv_caption character varying, pv_description character varying, pv_link character varying) OWNER TO shmihshmih;

--
-- TOC entry 255 (class 1255 OID 27735)
-- Name: f_type_opus8add(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_type_opus8add(pn_id bigint, pv_caption character varying, pv_description character varying) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
begin
INSERT INTO 
  literature.t_type_opus
(
  id,
  caption,
  description
)
VALUES (
  pn_id,
  pv_caption,
  pv_description
);
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_type_opus8add(pn_id bigint, pv_caption character varying, pv_description character varying) OWNER TO shmihshmih;

--
-- TOC entry 257 (class 1255 OID 27738)
-- Name: f_type_opus8del(bigint); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_type_opus8del(pn_id bigint) RETURNS void
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
DELETE FROM 
  literature.t_type_opus 
WHERE 
  id = pn_id
;
END;
$$;


ALTER FUNCTION literature.f_type_opus8del(pn_id bigint) OWNER TO shmihshmih;

--
-- TOC entry 236 (class 1255 OID 27737)
-- Name: f_type_opus8mod(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_type_opus8mod(pn_id bigint, pv_caption character varying, pv_description character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
DECLARE
  n_id  bigint;
BEGIN
  if pn_id is null then
    n_id := literature.f_type_opus8add(
      pn_id := core.f_gen_id(),
      pv_caption := pv_caption,
      pv_description := pv_description);
   else
    n_id := literature.f_type_opus8upd(
      pn_id := pn_id,
      pv_caption := pv_caption,
      pv_description := pv_description);
  end if;
  return n_id;
END;
$$;


ALTER FUNCTION literature.f_type_opus8mod(pn_id bigint, pv_caption character varying, pv_description character varying) OWNER TO shmihshmih;

--
-- TOC entry 256 (class 1255 OID 27736)
-- Name: f_type_opus8upd(bigint, character varying, character varying); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION f_type_opus8upd(pn_id bigint, pv_caption character varying, pv_description character varying) RETURNS bigint
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
UPDATE 
  literature.t_type_opus 
SET 
  caption = pv_caption,
  description = pv_description
WHERE 
  id = pn_id
;
return pn_id;
END;
$$;


ALTER FUNCTION literature.f_type_opus8upd(pn_id bigint, pv_caption character varying, pv_description character varying) OWNER TO shmihshmih;

--
-- TOC entry 265 (class 1255 OID 27766)
-- Name: tr_country8add(); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION tr_country8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_country8del('||NEW.id||');')::varchar,
       ('Добавлена страна '||NEW.caption||'('||NEW.id||')'||'. Язык:'||NEW.language)::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_country8add('||old.id||','||OLD.caption||','||OLD.language||');')::varchar,
       ('Изменена страна '||OLD.caption||'('||OLD.id||')'||'. Язык:'||OLD.language||' ('||NEW.id||'-'||new.caption||'-'||NEW.language||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_country8add('||old.id||','||OLD.caption||','||OLD.language||');')::varchar,
       ('Удалена страна '||OLD.caption||'('||OLD.id||')'||'. Язык:'||OLD.language)::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION literature.tr_country8add() OWNER TO shmihshmih;

--
-- TOC entry 264 (class 1255 OID 27773)
-- Name: tr_cycle8add(); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION tr_cycle8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_cycle8del('||NEW.id||');')::varchar,
       ('Добавлен цикл '||NEW.caption||'('||NEW.id||'-'||NEW.description||'-'||NEW.link||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_country8add('||old.id||','||OLD.caption||','||OLD.description||','||OLD.link||');')::varchar,
       ('Изменен цикл '||OLD.caption||'('||OLD.id||'-'||OLD.description||'-'||OLD.link||') => '||NEW.caption||'('||NEW.id||'-'||NEW.description||'-'||OLD.link||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_country8add('||old.id||','||OLD.caption||','||OLD.description||','||old.link||');')::varchar,
       ('Удален цикл '||OLD.caption||'('||OLD.id||'-'||OLD.description||'-'||old.link||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION literature.tr_cycle8add() OWNER TO shmihshmih;

--
-- TOC entry 266 (class 1255 OID 27775)
-- Name: tr_genre8add(); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION tr_genre8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_genre8del('||NEW.id||');')::varchar,
       ('Добавлен жанр '||NEW.caption||'('||NEW.id||'-'||NEW.description||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_genre8add('||old.id||','||OLD.caption||','||OLD.description||');')::varchar,
       ('Изменен жанр '||OLD.caption||'('||OLD.id||'-'||OLD.description||') => '||NEW.caption||'('||NEW.id||'-'||NEW.description||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_genre8add('||old.id||','||OLD.caption||','||OLD.description||');')::varchar,
       ('Удален жанр '||OLD.caption||'('||OLD.id||'-'||OLD.description||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION literature.tr_genre8add() OWNER TO shmihshmih;

--
-- TOC entry 269 (class 1255 OID 27779)
-- Name: tr_group_person8add(); Type: FUNCTION; Schema: literature; Owner: shmihshmih
--

CREATE FUNCTION tr_group_person8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_group_person8del('||NEW.id||');')::varchar,
       ('Добавлена группа писателей '||NEW.caption||'('||NEW.id||'-'||NEW.description||'-'||coalesce(NEW.link, ' ')||'-'||coalesce(new.previous, 0)||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_group_person8add('||old.id||','||OLD.caption||','||OLD.description||','||coalesce(OLD.link, ' ')||','||coalesce(old.previous, 0)||');')::varchar,
       ('Изменена группа писателей '||OLD.caption||'('||OLD.id||'-'||OLD.description||'-'||coalesce(OLD.link, ' ')||'-'||coalesce(old.previous, 0)||') => '||NEW.caption||'('||NEW.id||'-'||NEW.description||'-'||coalesce(new.link, ' ')||'-'||coalesce(new.previous, 0)||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_group_person8add('||old.id||','||OLD.caption||','||OLD.description||','||coalesce(old.link, ' ')||','||coalesce(old.previous, 0)||');')::varchar,
       ('Удалена группа писателей '||OLD.caption||'('||OLD.id||'-'||OLD.description||'-'||coalesce(old.link, ' ')||'-'||coalesce(old.previous, 0)||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION literature.tr_group_person8add() OWNER TO shmihshmih;

SET search_path = public, pg_catalog;

--
-- TOC entry 267 (class 1255 OID 27781)
-- Name: tr_opus8add(); Type: FUNCTION; Schema: public; Owner: shmihshmih
--

CREATE FUNCTION tr_opus8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_opus8del('||NEW.id||');')::varchar,
       ('Добавлено произведение '||NEW.caption||'('||NEW.id||'-'||coalesce(NEW.pid, 0)||'-'||new.caption||'-'||new.annotation||'-'||coalesce(new.cover, ' ')||'-'||new.published||'-'||coalesce(new.bibliography, ' ')||'-'||new.key_word||'-'||new.original_lang||'-'||coalesce(NEW.cycle, 0)||'-'||coalesce(new.cycle_order, 0)||'-'||new.genre||'-'||new.type||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_genre8add('||old.id||coalesce(old.pid, 0)||','||OLD.caption||','||old.annotation||','||coalesce(old.cover, ' ')||','||old.published||','||coalesce(old.bibliography, ' ')||','||old.key_word||','||old.original_lang||','||coalesce(old.cycle, 0)||','||coalesce(old.cycle_order, 0)||','||old.genre||','||old.type||');')::varchar,
       ('Изменено произведение '||old.caption||'('||old.id||'-'||coalesce(old.pid, 0)||'-'||old.caption||'-'||old.annotation||'-'||coalesce(old.cover, ' ')||'-'||old.published||'-'||coalesce(old.bibliography, ' ')||'-'||old.key_word||'-'||old.original_lang||'-'||coalesce(old.cycle, 0)||'-'||coalesce(old.cycle_order, 0)||'-'||old.genre||'-'||old.type||') => '||NEW.caption||'('||NEW.id||'-'||coalesce(NEW.pid, 0)||'-'||new.caption||'-'||new.annotation||'-'||coalesce(new.cover, ' ')||'-'||new.published||'-'||coalesce(new.bibliography, ' ')||'-'||new.key_word||'-'||new.original_lang||'-'||coalesce(NEW.cycle, 0)||'-'||coalesce(new.cycle_order, 0)||'-'||new.genre||'-'||new.type||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_genre8add('||old.id||coalesce(old.pid, 0)||','||OLD.caption||','||old.annotation||','||coalesce(old.cover, ' ')||','||old.published||','||coalesce(old.bibliography, ' ')||','||old.key_word||','||old.original_lang||','||coalesce(old.cycle, 0)||','||coalesce(old.cycle_order, 0)||','||old.genre||','||old.type||');')::varchar,
       ('Удалено произведение '||old.caption||'('||old.id||'-'||coalesce(old.pid, 0)||'-'||old.caption||'-'||old.annotation||'-'||coalesce(old.cover, ' ')||'-'||old.published||'-'||coalesce(old.bibliography, ' ')||'-'||old.key_word||'-'||old.original_lang||'-'||coalesce(old.cycle, 0)||'-'||coalesce(old.cycle_order, 0)||'-'||old.genre||'-'||old.type||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION public.tr_opus8add() OWNER TO shmihshmih;

--
-- TOC entry 271 (class 1255 OID 27785)
-- Name: tr_opus_theme8add(); Type: FUNCTION; Schema: public; Owner: shmihshmih
--

CREATE FUNCTION tr_opus_theme8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_opus_theme8del('||NEW.id||');')::varchar,
       ('Добавлена тема произведения ('||NEW.id||'-'||NEW.opus||'-'||NEW.theme||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_opus_theme8add('||old.id||','||OLD.opus||','||OLD.theme||');')::varchar,
       ('Изменена тема произведения ('||OLD.id||'-'||OLD.opus||'-'||OLD.theme||') => ('||NEW.id||'-'||NEW.opus||'-'||OLD.theme||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_opus_theme8add('||old.id||','||OLD.opus||','||OLD.theme||');')::varchar,
       ('Удалена тема произведения '||'('||OLD.id||'-'||OLD.opus||'-'||old.theme||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION public.tr_opus_theme8add() OWNER TO shmihshmih;

--
-- TOC entry 262 (class 1255 OID 27789)
-- Name: tr_person8add(); Type: FUNCTION; Schema: public; Owner: shmihshmih
--

CREATE FUNCTION tr_person8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_person8del('||NEW.id||');')::varchar,
       ('Добавлен писатель '||NEW.name||' '||coalesce(NEW.surname, ' ')||'('||NEW.id||'-'||coalesce(NEW.last_name, ' ')||'-'||NEW.birth_date||'-'||coalesce(new.death_date, '01-01-0001')||'-'||new.country||'-'||new.key_word||'-'||new.annotation||'-'||new.link||'-'||new.portrait||'-'||COALESCE(new.bibliography, ' ')||'-'||coalesce(new.group_person, 0)||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_person8add('||old.id||','||OLD.name||','||coalesce(OLD.surname, ' ')||','||coalesce(OLD.last_name, ' ')||','||old.birth_date||','||coalesce(old.death_date, '01-01-0001')||','||old.country||','||old.key_word||','||old.annotation||','||old.link||','||old.link||','||old.portrait||','||coalesce(OLD.bibliography, ' ')||','||coalesce(old.group_person, 0)||');')::varchar,
       ('Изменен писатель '||old.name||' '||old.surname||'('||old.id||'-'||coalesce(old.last_name, ' ')||'-'||old.birth_date||'-'||coalesce(old.death_date, '01-01-0001')||'-'||old.country||'-'||old.key_word||'-'||old.annotation||'-'||old.link||'-'||old.portrait||'-'||coalesce(old.bibliography, ' ')||'-'||COALESCE(old.group_person, 0)||') => '||NEW.name||' '||coalesce(NEW.surname, ' ')||'('||NEW.id||'-'||coalesce(NEW.last_name, ' ')||'-'||NEW.birth_date||'-'||coalesce(new.death_date, '01-01-0001')||'-'||new.country||'-'||new.key_word||'-'||new.annotation||'-'||new.link||'-'||new.portrait||'-'||coalesce(new.bibliography, ' ')||'-'||coalesce(new.group_person, 0)||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_person8add('||old.id||','||OLD.name||','||coalesce(OLD.surname, ' ')||','||coalesce(OLD.last_name, ' ')||','||old.birth_date||','||coalesce(old.death_date, '01-01-0001')||','||old.country||','||old.key_word||','||old.annotation||','||old.link||','||old.link||','||old.portrait||','||coalesce(OLD.bibliography, ' ')||','||coalesce(old.group_person, 0)||');')::varchar,
       ('Удален писатель '||old.name||' '||coalesce(old.surname, ' ')||'('||old.id||'-'||coalesce(old.last_name, ' ')||'-'||old.birth_date||'-'||coalesce(old.death_date, '01-01-0001')||'-'||old.country||'-'||old.key_word||'-'||old.annotation||'-'||old.link||'-'||old.portrait||'-'||COALESCE(old.bibliography, ' ')||'-'||coalesce(old.group_person, 0)||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION public.tr_person8add() OWNER TO shmihshmih;

--
-- TOC entry 263 (class 1255 OID 27792)
-- Name: tr_phrase8add(); Type: FUNCTION; Schema: public; Owner: shmihshmih
--

CREATE FUNCTION tr_phrase8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_phrase8del('||NEW.id||');')::varchar,
       ('Добавлена фраза '||NEW.caption||'('||NEW.id||'-'||coalesce(NEW.pid, 0)||'-'||coalesce(NEW.original, ' ')||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_phrase8add('||old.id||','||OLD.caption||','||coalesce(OLD.pid, 0)||','||coalesce(OLD.original, ' ')||');')::varchar,
       ('Изменена фраза '||OLD.caption||'('||OLD.id||'-'||coalesce(OLD.pid, 0)||'-'||coalesce(OLD.original, ' ')||') => '||NEW.caption||'('||NEW.id||'-'||coalesce(NEW.pid, 0)||'-'||coalesce(OLD.original, ' ')||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_phrase8add('||old.id||','||OLD.caption||','||coalesce(OLD.pid, 0)||','||coalesce(OLD.original, ' ')||');')::varchar,
       ('Удалена фраза '||old.caption||'('||old.id||'-'||coalesce(old.pid, 0)||'-'||coalesce(old.original, ' ')||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION public.tr_phrase8add() OWNER TO shmihshmih;

--
-- TOC entry 268 (class 1255 OID 27787)
-- Name: tr_theme8add(); Type: FUNCTION; Schema: public; Owner: shmihshmih
--

CREATE FUNCTION tr_theme8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_theme8del('||NEW.id||');')::varchar,
       ('Добавлена тема '||NEW.caption||'('||NEW.id||'-'||NEW.description||'-'||coalesce(NEW.link, ' ')||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_theme8add('||old.id||','||OLD.caption||','||OLD.description||','||coalesce(OLD.link, ' ')||');')::varchar,
       ('Изменена тема '||OLD.caption||'('||OLD.id||'-'||OLD.description||'-'||coalesce(OLD.link, ' ')||') => '||NEW.caption||'('||NEW.id||'-'||NEW.description||'-'||coalesce(OLD.link, ' ')||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_theme8add('||old.id||','||OLD.caption||','||OLD.description||','||coalesce(old.link, ' ')||');')::varchar,
       ('Удалена тема '||OLD.caption||'('||OLD.id||'-'||OLD.description||'-'||coalesce(old.link, ' ')||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION public.tr_theme8add() OWNER TO shmihshmih;

--
-- TOC entry 270 (class 1255 OID 27783)
-- Name: tr_type_opus8add(); Type: FUNCTION; Schema: public; Owner: shmihshmih
--

CREATE FUNCTION tr_type_opus8add() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  if TG_OP = 'INSERT' then 
    perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_type_opus8del('||NEW.id||');')::varchar,
       ('Добавлен тип литературы '||NEW.caption||'('||NEW.id||'-'||NEW.description||')')::varchar);
    return new;
  end if;
  if TG_OP = 'UPDATE' then 
      perform literature.f_log8add(
  	   core.f_gen_id(),
       NEW.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_type_opus8add('||old.id||','||OLD.caption||','||OLD.description||');')::varchar,
       ('Изменен тип литературы '||OLD.caption||'('||OLD.id||'-'||OLD.description||') => '||NEW.caption||'('||NEW.id||'-'||NEW.description||')')::varchar);
    return new;
  end if;
  if TG_OP = 'DELETE' then
        perform literature.f_log8add(
  	   core.f_gen_id(),
       old.id,
       TG_TABLE_NAME::varchar,
       CURRENT_TIMESTAMP::TIMESTAMP,
       TG_OP::varchar,
       ('perform literature.f_type_opus8add('||old.id||','||OLD.caption||','||OLD.description||');')::varchar,
       ('Удален тип литературы '||OLD.caption||'('||OLD.id||'-'||OLD.description||')')::varchar);
    return null;
  end if;
END;
$$;


ALTER FUNCTION public.tr_type_opus8add() OWNER TO shmihshmih;

SET search_path = core, pg_catalog;

--
-- TOC entry 202 (class 1259 OID 27686)
-- Name: main_seq; Type: SEQUENCE; Schema: core; Owner: shmihshmih
--

CREATE SEQUENCE main_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE main_seq OWNER TO shmihshmih;

SET search_path = literature, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 27495)
-- Name: t_country; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_country (
    id bigint NOT NULL,
    caption character varying(50) NOT NULL,
    language character varying(50) NOT NULL
);
ALTER TABLE ONLY t_country ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_country ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_country ALTER COLUMN language SET STATISTICS 0;


ALTER TABLE t_country OWNER TO shmihshmih;

--
-- TOC entry 3297 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_country.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_country.id IS 'Идентификатор';


--
-- TOC entry 3298 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_country.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_country.caption IS 'Название';


--
-- TOC entry 3299 (class 0 OID 0)
-- Dependencies: 184
-- Name: COLUMN t_country.language; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_country.language IS 'Язык';


--
-- TOC entry 188 (class 1259 OID 27554)
-- Name: t_cycle; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_cycle (
    id bigint NOT NULL,
    caption character varying(50) NOT NULL,
    description character varying(500) NOT NULL,
    link character varying(500) NOT NULL
);
ALTER TABLE ONLY t_cycle ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_cycle ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_cycle ALTER COLUMN description SET STATISTICS 0;
ALTER TABLE ONLY t_cycle ALTER COLUMN link SET STATISTICS 0;


ALTER TABLE t_cycle OWNER TO shmihshmih;

--
-- TOC entry 3300 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_cycle.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_cycle.id IS 'Идентификатор';


--
-- TOC entry 3301 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_cycle.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_cycle.caption IS 'Название';


--
-- TOC entry 3302 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_cycle.description; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_cycle.description IS 'Описание';


--
-- TOC entry 3303 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN t_cycle.link; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_cycle.link IS 'Ссылка';


--
-- TOC entry 189 (class 1259 OID 27576)
-- Name: t_genre; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_genre (
    id bigint NOT NULL,
    caption character varying(50) NOT NULL,
    description character varying(500) NOT NULL
);
ALTER TABLE ONLY t_genre ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_genre ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_genre ALTER COLUMN description SET STATISTICS 0;


ALTER TABLE t_genre OWNER TO shmihshmih;

--
-- TOC entry 3304 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN t_genre.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_genre.id IS 'Идентификатор';


--
-- TOC entry 3305 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN t_genre.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_genre.caption IS 'Название';


--
-- TOC entry 3306 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN t_genre.description; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_genre.description IS 'Описание';


--
-- TOC entry 185 (class 1259 OID 27505)
-- Name: t_group_person; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_group_person (
    id bigint NOT NULL,
    caption character varying(50) NOT NULL,
    description character varying(500) NOT NULL,
    link character varying(500),
    previous bigint
);
ALTER TABLE ONLY t_group_person ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_group_person ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_group_person ALTER COLUMN description SET STATISTICS 0;
ALTER TABLE ONLY t_group_person ALTER COLUMN link SET STATISTICS 0;
ALTER TABLE ONLY t_group_person ALTER COLUMN previous SET STATISTICS 0;


ALTER TABLE t_group_person OWNER TO shmihshmih;

--
-- TOC entry 3307 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_group_person.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_group_person.id IS 'Идентификатор';


--
-- TOC entry 3308 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_group_person.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_group_person.caption IS 'Название';


--
-- TOC entry 3309 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_group_person.description; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_group_person.description IS 'Описание';


--
-- TOC entry 3310 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_group_person.link; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_group_person.link IS 'Ссылка';


--
-- TOC entry 3311 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN t_group_person.previous; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_group_person.previous IS 'Предыдущее движение';


--
-- TOC entry 203 (class 1259 OID 27739)
-- Name: t_log; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_log (
    id bigint NOT NULL,
    pid bigint NOT NULL,
    changed_table character varying(20) NOT NULL,
    timed timestamp(6) without time zone NOT NULL,
    action_type character varying(20) NOT NULL,
    revert character varying NOT NULL,
    message character varying NOT NULL
);
ALTER TABLE ONLY t_log ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN pid SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN changed_table SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN timed SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN action_type SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN revert SET STATISTICS 0;
ALTER TABLE ONLY t_log ALTER COLUMN message SET STATISTICS 0;


ALTER TABLE t_log OWNER TO shmihshmih;

--
-- TOC entry 3312 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN t_log.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_log.id IS 'Идентификатор';


--
-- TOC entry 3313 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN t_log.pid; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_log.pid IS 'ID изменяемой записи';


--
-- TOC entry 3314 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN t_log.changed_table; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_log.changed_table IS 'Изменяемая таблица';


--
-- TOC entry 3315 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN t_log.timed; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_log.timed IS 'Дата изменения';


--
-- TOC entry 3316 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN t_log.action_type; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_log.action_type IS 'Тип действия';


--
-- TOC entry 3317 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN t_log.revert; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_log.revert IS 'Обратный скрипт';


--
-- TOC entry 3318 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN t_log.message; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_log.message IS 'Текст изменения';


--
-- TOC entry 187 (class 1259 OID 27542)
-- Name: t_opus; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_opus (
    id bigint NOT NULL,
    pid bigint,
    caption character varying(100) NOT NULL,
    annotation character varying(500) NOT NULL,
    cover character varying(500),
    published date NOT NULL,
    bibliography character varying(500),
    key_word character varying(500) NOT NULL,
    original_lang character varying(50) NOT NULL,
    cycle bigint,
    cycle_order bigint,
    genre bigint NOT NULL,
    type bigint NOT NULL
);
ALTER TABLE ONLY t_opus ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN pid SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN annotation SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN cover SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN published SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN bibliography SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN key_word SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN original_lang SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN cycle SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN cycle_order SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN genre SET STATISTICS 0;
ALTER TABLE ONLY t_opus ALTER COLUMN type SET STATISTICS 0;


ALTER TABLE t_opus OWNER TO shmihshmih;

--
-- TOC entry 3319 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.id IS 'Идентификатор';


--
-- TOC entry 3320 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.pid; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.pid IS 'Автор';


--
-- TOC entry 3321 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.caption IS 'Название';


--
-- TOC entry 3322 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.annotation; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.annotation IS 'Аннотация';


--
-- TOC entry 3323 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.cover; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.cover IS 'Обложка';


--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.published; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.published IS 'Опубликовано';


--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.bibliography; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.bibliography IS 'Библиография';


--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.key_word; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.key_word IS 'Ключи';


--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.original_lang; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.original_lang IS 'Оригинал';


--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.cycle; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.cycle IS 'Цикл';


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.cycle_order; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.cycle_order IS 'Порядок в цмкле';


--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.genre; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.genre IS 'Жанр';


--
-- TOC entry 3331 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN t_opus.type; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus.type IS 'Тип литературы';


--
-- TOC entry 192 (class 1259 OID 27622)
-- Name: t_opus_theme; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_opus_theme (
    id bigint NOT NULL,
    opus bigint NOT NULL,
    theme bigint NOT NULL
);
ALTER TABLE ONLY t_opus_theme ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_opus_theme ALTER COLUMN opus SET STATISTICS 0;
ALTER TABLE ONLY t_opus_theme ALTER COLUMN theme SET STATISTICS 0;


ALTER TABLE t_opus_theme OWNER TO shmihshmih;

--
-- TOC entry 3332 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN t_opus_theme.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus_theme.id IS 'Идентификатор';


--
-- TOC entry 3333 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN t_opus_theme.opus; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus_theme.opus IS 'Произведение';


--
-- TOC entry 3334 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN t_opus_theme.theme; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_opus_theme.theme IS 'Тема';


--
-- TOC entry 183 (class 1259 OID 27487)
-- Name: t_person; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_person (
    id bigint NOT NULL,
    name character varying(20) NOT NULL,
    surname character varying(20),
    last_name character varying(20),
    birth_date date NOT NULL,
    death_date date,
    country bigint NOT NULL,
    key_word character varying(500) NOT NULL,
    annotation character varying(500) NOT NULL,
    link character varying(500) NOT NULL,
    portrait character varying(500) NOT NULL,
    bibliography character varying(500),
    group_person bigint
);
ALTER TABLE ONLY t_person ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN name SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN surname SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN last_name SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN birth_date SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN death_date SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN country SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN key_word SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN annotation SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN link SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN portrait SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN bibliography SET STATISTICS 0;
ALTER TABLE ONLY t_person ALTER COLUMN group_person SET STATISTICS 0;


ALTER TABLE t_person OWNER TO shmihshmih;

--
-- TOC entry 3335 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.id IS 'Идентификатор';


--
-- TOC entry 3336 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.name; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.name IS 'Имя';


--
-- TOC entry 3337 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.surname; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.surname IS 'Фамилия';


--
-- TOC entry 3338 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.last_name; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.last_name IS 'Отчество';


--
-- TOC entry 3339 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.birth_date; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.birth_date IS 'Дата рождения';


--
-- TOC entry 3340 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.death_date; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.death_date IS 'Дата смерти';


--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.country; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.country IS 'Страна';


--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.key_word; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.key_word IS 'Ключевые слова';


--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.annotation; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.annotation IS 'Аннотация';


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.link; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.link IS 'Ссылка';


--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.portrait; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.portrait IS 'Портрет';


--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.bibliography; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.bibliography IS 'Библиография';


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN t_person.group_person; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_person.group_person IS 'Движение';


--
-- TOC entry 186 (class 1259 OID 27527)
-- Name: t_phrase; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_phrase (
    id bigint NOT NULL,
    pid bigint,
    caption character varying(500) NOT NULL,
    original character varying(500)
);
ALTER TABLE ONLY t_phrase ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_phrase ALTER COLUMN pid SET STATISTICS 0;
ALTER TABLE ONLY t_phrase ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_phrase ALTER COLUMN original SET STATISTICS 0;


ALTER TABLE t_phrase OWNER TO shmihshmih;

--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_phrase.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_phrase.id IS 'Идентификатор';


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_phrase.pid; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_phrase.pid IS 'Автор';


--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_phrase.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_phrase.caption IS 'Фраза';


--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN t_phrase.original; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_phrase.original IS 'Оригинал';


--
-- TOC entry 191 (class 1259 OID 27610)
-- Name: t_theme; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_theme (
    id bigint NOT NULL,
    caption character varying(50) NOT NULL,
    description character varying(500) NOT NULL,
    link character varying(500)
);
ALTER TABLE ONLY t_theme ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_theme ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_theme ALTER COLUMN description SET STATISTICS 0;
ALTER TABLE ONLY t_theme ALTER COLUMN link SET STATISTICS 0;


ALTER TABLE t_theme OWNER TO shmihshmih;

--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_theme.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_theme.id IS 'Идентификатор';


--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_theme.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_theme.caption IS 'Название';


--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_theme.description; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_theme.description IS 'Описание';


--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN t_theme.link; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_theme.link IS 'Ссылка';


--
-- TOC entry 190 (class 1259 OID 27593)
-- Name: t_type_opus; Type: TABLE; Schema: literature; Owner: shmihshmih
--

CREATE TABLE t_type_opus (
    id bigint NOT NULL,
    caption character varying(50) NOT NULL,
    description character varying(500) NOT NULL
);
ALTER TABLE ONLY t_type_opus ALTER COLUMN id SET STATISTICS 0;
ALTER TABLE ONLY t_type_opus ALTER COLUMN caption SET STATISTICS 0;
ALTER TABLE ONLY t_type_opus ALTER COLUMN description SET STATISTICS 0;


ALTER TABLE t_type_opus OWNER TO shmihshmih;

--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_type_opus.id; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_type_opus.id IS 'Идентификатор';


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_type_opus.caption; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_type_opus.caption IS 'Название';


--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN t_type_opus.description; Type: COMMENT; Schema: literature; Owner: shmihshmih
--

COMMENT ON COLUMN t_type_opus.description IS 'Описание';


--
-- TOC entry 193 (class 1259 OID 27637)
-- Name: v_country; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_country AS
 SELECT t_country.id,
    t_country.caption,
    t_country.language
   FROM t_country;


ALTER TABLE v_country OWNER TO shmihshmih;

--
-- TOC entry 194 (class 1259 OID 27645)
-- Name: v_cycle; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_cycle AS
 SELECT t_cycle.id,
    t_cycle.caption,
    t_cycle.description,
    t_cycle.link
   FROM t_cycle;


ALTER TABLE v_cycle OWNER TO shmihshmih;

--
-- TOC entry 195 (class 1259 OID 27649)
-- Name: v_genre; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_genre AS
 SELECT t_genre.id,
    t_genre.caption,
    t_genre.description
   FROM t_genre;


ALTER TABLE v_genre OWNER TO shmihshmih;

--
-- TOC entry 196 (class 1259 OID 27654)
-- Name: v_group_person; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_group_person AS
 SELECT t_group_person.id,
    t_group_person.caption,
    t_group_person.description,
    t_group_person.link,
    t_group_person.previous
   FROM t_group_person;


ALTER TABLE v_group_person OWNER TO shmihshmih;

--
-- TOC entry 204 (class 1259 OID 27755)
-- Name: v_log; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_log AS
 SELECT t_log.id,
    t_log.pid,
    t_log.changed_table,
    t_log.timed,
    t_log.action_type,
    t_log.revert,
    t_log.message
   FROM t_log;


ALTER TABLE v_log OWNER TO shmihshmih;

--
-- TOC entry 197 (class 1259 OID 27658)
-- Name: v_opus; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_opus AS
 SELECT t_opus.id,
    t_opus.pid,
    t_opus.caption,
    t_opus.annotation,
    t_opus.cover,
    t_opus.published,
    t_opus.bibliography,
    t_opus.key_word,
    t_opus.original_lang,
    t_opus.cycle,
    t_opus.cycle_order,
    t_opus.genre,
    t_opus.type
   FROM t_opus;


ALTER TABLE v_opus OWNER TO shmihshmih;

--
-- TOC entry 198 (class 1259 OID 27662)
-- Name: v_opus_theme; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_opus_theme AS
 SELECT t_opus_theme.id,
    t_opus_theme.opus,
    t_opus_theme.theme
   FROM t_opus_theme;


ALTER TABLE v_opus_theme OWNER TO shmihshmih;

--
-- TOC entry 205 (class 1259 OID 27794)
-- Name: v_person; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_person AS
 SELECT t_person.id,
    t_person.name,
    t_person.surname,
    t_person.last_name,
    t_person.birth_date,
    t_person.death_date,
    t_person.country,
    t_person.key_word,
    t_person.annotation,
    t_person.link,
    t_person.portrait,
    t_person.bibliography,
    t_person.group_person,
    t_country.caption AS country_caption,
    t_group_person.caption AS group_person_caption
   FROM ((t_person
     JOIN t_country ON ((t_person.country = t_country.id)))
     LEFT JOIN t_group_person ON ((t_person.group_person = t_group_person.id)));


ALTER TABLE v_person OWNER TO shmihshmih;

--
-- TOC entry 199 (class 1259 OID 27670)
-- Name: v_phrase; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_phrase AS
 SELECT t_phrase.id,
    t_phrase.pid,
    t_phrase.caption,
    t_phrase.original
   FROM t_phrase;


ALTER TABLE v_phrase OWNER TO shmihshmih;

--
-- TOC entry 200 (class 1259 OID 27674)
-- Name: v_theme; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_theme AS
 SELECT t_theme.id,
    t_theme.caption,
    t_theme.description,
    t_theme.link
   FROM t_theme;


ALTER TABLE v_theme OWNER TO shmihshmih;

--
-- TOC entry 201 (class 1259 OID 27678)
-- Name: v_type_opus; Type: VIEW; Schema: literature; Owner: shmihshmih
--

CREATE VIEW v_type_opus AS
 SELECT t_type_opus.id,
    t_type_opus.caption,
    t_type_opus.description
   FROM t_type_opus;


ALTER TABLE v_type_opus OWNER TO shmihshmih;

SET search_path = core, pg_catalog;

--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 202
-- Name: main_seq; Type: SEQUENCE SET; Schema: core; Owner: shmihshmih
--

SELECT pg_catalog.setval('main_seq', 592, true);


SET search_path = literature, pg_catalog;

--
-- TOC entry 3278 (class 0 OID 27495)
-- Dependencies: 184
-- Data for Name: t_country; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_country (id, caption, language) FROM stdin;
2	Россия	Русский
\.


--
-- TOC entry 3282 (class 0 OID 27554)
-- Dependencies: 188
-- Data for Name: t_cycle; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_cycle (id, caption, description, link) FROM stdin;
25	Хождения по мукам	«Хождение по мукам» — трилогия романов А. Н. Толстого, прослеживающая судьбы русской интеллигенции накануне, во время и после революционных событий 1917 года. Состоит из романов «Сёстры» (1921—1922), «Восемнадцатый год» (1927—1928) и «Хмурое утро» (1940—1941).	https://ru.wikipedia.org/wiki/%D0%A5%D0%BE%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5_%D0%BF%D0%BE_%D0%BC%D1%83%D0%BA%D0%B0%D0%BC
\.


--
-- TOC entry 3283 (class 0 OID 27576)
-- Dependencies: 189
-- Data for Name: t_genre; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_genre (id, caption, description) FROM stdin;
59	Роман	Прозаическиая фыорма
197	Поэма	В стихах
\.


--
-- TOC entry 3279 (class 0 OID 27505)
-- Dependencies: 185
-- Data for Name: t_group_person; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_group_person (id, caption, description, link, previous) FROM stdin;
66	Футуристы	Футуристы	Футуристы	\N
\.


--
-- TOC entry 3288 (class 0 OID 27739)
-- Dependencies: 203
-- Data for Name: t_log; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_log (id, pid, changed_table, timed, action_type, revert, message) FROM stdin;
24	5	country	2017-04-28 11:50:00.090313	i	perform literature.f_country8del(5);	Добавлена страна ghjfjh(fdhfghdf). Язык:dghfghfgh
34	33	country	2017-04-28 11:57:15.07845	i	perform literature.f_country8del(33);	Добавлена страна lolo(33). Язык:lolo
36	35	t_country	2017-04-28 12:11:41.998281	INSERT	perform literature.f_country8del(35);	Добавлена страна lolor(35). Язык:lolor
37	35	t_country	2017-04-28 12:27:23.280219	UPDATE	perform literature.f_country8del(35);	Изменена страна lolor(35). Язык:lolor (35-kleo-monako)
38	35	t_country	2017-04-28 12:30:11.646761	UPDATE	perform literature.f_country8add(35,kleo,monako);	Изменена страна kleo(35). Язык:monako (35-kleode-monakode)
40	35	t_country	2017-04-28 12:31:32.795642	DELETE	perform literature.f_country8add(35,kleode,monakode);	Удалена страна kleode(35). Язык:monakode
42	41	t_cycle	2017-04-28 19:10:11.828377	INSERT	perform literature.f_cycle8del(41);	Добавлен цикл ffg(41-ggf-rrt)
43	41	t_cycle	2017-04-28 19:12:04.887821	UPDATE	perform literature.f_country8add(41,ffg,ggf,rrt);	ИзмененН цикл ffg(41-ggfrrt) => (changed(41-changedchanged)
44	41	t_cycle	2017-04-28 19:15:51.245801	DELETE	perform literature.f_country8add(41,changed,changed,changed);	Удален цикл changed(41-changed-changed)
46	45	t_genre	2017-04-28 19:20:02.053785	INSERT	perform literature.f_genre8del(45);	Добавлен жанр gena(45-gena)
47	45	t_genre	2017-04-28 19:20:21.211214	UPDATE	perform literature.f_genre8add(45,gena,gena);	ИзмененН жанр gena(45-gena) => pipa(45-pipa-)
48	45	t_genre	2017-04-28 19:20:32.028377	DELETE	perform literature.f_genre8add(45,pipa,pipa);	Удален жанр pipa(45-pipa)
55	54	t_group_person	2017-04-28 19:42:57.231029	INSERT	perform literature.f_group_person8del(54);	Добавлена группа писателей wer(54-rew-ter-0)
56	54	t_group_person	2017-04-28 19:43:21.224074	UPDATE	perform literature.f_group_person8add(54,wer,rew,ter,0);	Изменена группа писателей wer(54-rew-ter-0) => werchange(54-rewchange-terchange-0)
57	54	t_group_person	2017-04-28 19:43:32.519671	DELETE	perform literature.f_group_person8add(54,werchange,rewchange,terchange,0);	Удалена группа писателей werchange(54-rewchange-terchange-0)
60	59	t_genre	2017-04-28 22:10:56.773637	INSERT	perform literature.f_genre8del(59);	Добавлен жанр nunu(59-titi)
63	62	t_type_opus	2017-04-28 22:20:38.989527	INSERT	perform literature.f_type_opus8del(62);	Добавлен тип литературы dobby(62-doddy)
64	62	t_type_opus	2017-04-28 22:20:49.796547	UPDATE	perform literature.f_type_opus8add(62,dobby,doddy);	Изменен тип литературы dobby(62-doddy) => fff(62-fff)
66	65	t_opus	2017-04-28 22:21:16.075751	INSERT	perform literature.f_opus8del(65);	Добавлено произведение cap(65-0-cap-ann-cov-1992-05-16-библ-key-orig-0-0-59-62)
68	65	t_opus	2017-04-28 22:21:56.494803	UPDATE	perform literature.f_genre8add(650,cap,ann,cov,1992-05-16,библ,key,orig,0,0,59,62);	Изменено произведение cap(65-0-cap-ann-cov-1992-05-16-библ-key-orig-0-0-59-62) => cap(65-0-cap-ann-cov-1992-05-16-библ-key-orig-0-0-59-62)
70	69	t_theme	2017-04-28 22:57:35.095415	INSERT	perform literature.f_theme8del(69);	Добавлена тема dodo(69-pipi-lilil)
71	69	t_theme	2017-04-28 22:57:51.62291	UPDATE	perform literature.f_theme8add(69,dodo,pipi,lilil);	Изменена тема dodo(69-pipi-lilil) => dodou(69-pipi-lilil)
73	72	t_opus_theme	2017-04-28 22:58:19.065821	INSERT	perform literature.f_opus_theme8del(72);	Добавлена тема произведения (72-65-69)
77	76	t_person	2017-04-28 23:49:56.126605	INSERT	perform literature.f_person8del(76);	Добавлен писатель name suraname(76-last_name-1992-05-16-0001-01-01-2-fgfg fgfg-dfsdfdf-sdfsdfsd-gdhfhdf-gsdfgdsf-0)
78	76	t_person	2017-04-28 23:53:39.551026	UPDATE	perform literature.f_person8add(76,name,suraname,last_name,1992-05-16,0001-01-01,2,fgfg fgfg,dfsdfdf,sdfsdfsd,sdfsdfsd,gdhfhdf,gsdfgdsf,0);	Изменен писатель name suraname(76-last_name-1992-05-16-0001-01-01-2-fgfg fgfg-dfsdfdf-sdfsdfsd-gdhfhdf-gsdfgdsf-0) => ngng suraname(76-last_name-1992-05-16-0001-01-01-2-fgfg fgfg-dfsdfdf-sdfsdfsd-gdhfhdf-gsdfgdsf-0)
79	76	t_person	2017-04-28 23:55:42.608649	DELETE	perform literature.f_person8add(76,ngng,suraname,last_name,1992-05-16,0001-01-01,2,fgfg fgfg,dfsdfdf,sdfsdfsd,sdfsdfsd,gdhfhdf,gsdfgdsf,0);	Удален писатель ngng suraname(76-last_name-1992-05-16-0001-01-01-2-fgfg fgfg-dfsdfdf-sdfsdfsd-gdhfhdf-gsdfgdsf-0)
81	80	t_phrase	2017-04-29 00:01:50.744854	INSERT	perform literature.f_phrase8del(80);	Добавлена фраза dodo(80-0-pipi)
82	80	t_phrase	2017-04-29 00:02:20.295263	UPDATE	perform literature.f_phrase8add(80,dodo,0,pipi);	Изменена фраза dodo(80-0-pipi) => dodor(80-0-pipi)
83	80	t_phrase	2017-04-29 00:02:29.804044	DELETE	perform literature.f_phrase8add(80,dodor,0,pipid);	Удалена фраза dodor(80-0-pipid)
86	2	t_country	2017-05-04 14:29:28.028462	UPDATE	perform literature.f_country8add(2,else,facks);	Изменена страна else(2). Язык:facks (2-elset-facks)
87	2	t_country	2017-05-04 14:29:28.053638	UPDATE	perform literature.f_country8add(2,elset,facks);	Изменена страна elset(2). Язык:facks (2-elset-facks)
89	2	t_country	2017-05-04 14:34:30.930746	UPDATE	perform literature.f_country8add(2,elset,facks);	Изменена страна elset(2). Язык:facks (2-else-facks)
91	33	t_country	2017-05-04 14:39:06.115733	UPDATE	perform literature.f_country8add(33,lolo,lolo);	Изменена страна lolo(33). Язык:lolo (33-lolor-lolo)
93	33	t_country	2017-05-04 14:39:12.268439	UPDATE	perform literature.f_country8add(33,lolor,lolo);	Изменена страна lolor(33). Язык:lolo (33-lolof-lolo)
95	33	t_country	2017-05-04 14:39:18.169089	UPDATE	perform literature.f_country8add(33,lolof,lolo);	Изменена страна lolof(33). Язык:lolo (33-lolorf-lolof)
96	33	t_country	2017-05-04 14:39:22.047979	UPDATE	perform literature.f_country8add(33,lolorf,lolof);	Изменена страна lolorf(33). Язык:lolof (33-lolof-lolo)
99	33	t_country	2017-05-04 14:43:25.257653	UPDATE	perform literature.f_country8add(33,lolof,lolo);	Изменена страна lolof(33). Язык:lolo (33-loloft-lolo)
100	33	t_country	2017-05-04 14:43:25.257658	UPDATE	perform literature.f_country8add(33,loloft,lolo);	Изменена страна loloft(33). Язык:lolo (33-loloft-lolo)
103	33	t_country	2017-05-04 14:43:32.964799	UPDATE	perform literature.f_country8add(33,loloft,lolo);	Изменена страна loloft(33). Язык:lolo (33-lolofth-lolo)
104	33	t_country	2017-05-04 14:43:32.972493	UPDATE	perform literature.f_country8add(33,lolofth,lolo);	Изменена страна lolofth(33). Язык:lolo (33-lolofth-lolo)
107	33	t_country	2017-05-04 14:44:05.687392	UPDATE	perform literature.f_country8add(33,lolofth,lolo);	Изменена страна lolofth(33). Язык:lolo (33-pipi-lolo)
108	33	t_country	2017-05-04 14:44:05.688062	UPDATE	perform literature.f_country8add(33,pipi,lolo);	Изменена страна pipi(33). Язык:lolo (33-pipi-lolo)
110	33	t_country	2017-05-04 14:45:19.932131	UPDATE	perform literature.f_country8add(33,pipi,lolo);	Изменена страна pipi(33). Язык:lolo (33-pipir-lolo)
112	33	t_country	2017-05-04 14:48:02.999972	UPDATE	perform literature.f_country8add(33,pipir,lolo);	Изменена страна pipir(33). Язык:lolo (33-pipirka-lolo)
114	2	t_country	2017-05-04 14:51:22.006983	UPDATE	perform literature.f_country8add(2,else,facks);	Изменена страна else(2). Язык:facks (2-elsert-facks)
116	33	t_country	2017-05-04 15:03:19.492207	UPDATE	perform literature.f_country8add(33,pipirka,lolo);	Изменена страна pipirka(33). Язык:lolo (33-pipi-lolo)
118	2	t_country	2017-05-04 15:05:56.52007	UPDATE	perform literature.f_country8add(2,elsert,facks);	Изменена страна elsert(2). Язык:facks (2-else-facks)
120	33	t_country	2017-05-04 15:21:02.772678	UPDATE	perform literature.f_country8add(33,pipi,lolo);	Изменена страна pipi(33). Язык:lolo (33-pipij-lolo)
122	2	t_country	2017-05-04 15:29:33.146538	UPDATE	perform literature.f_country8add(2,else,facks);	Изменена страна else(2). Язык:facks (2-else2-facks)
124	2	t_country	2017-05-04 15:29:45.567071	UPDATE	perform literature.f_country8add(2,else2,facks);	Изменена страна else2(2). Язык:facks (2-else2-facks)
125	2	t_country	2017-05-04 15:29:47.629496	UPDATE	perform literature.f_country8add(2,else2,facks);	Изменена страна else2(2). Язык:facks (2-else2-facks)
126	2	t_country	2017-05-04 15:29:49.815265	UPDATE	perform literature.f_country8add(2,else2,facks);	Изменена страна else2(2). Язык:facks (2-elseы-facks)
128	33	t_country	2017-05-04 15:40:13.195379	UPDATE	perform literature.f_country8add(33,pipij,lolo);	Изменена страна pipij(33). Язык:lolo (33-pipije-lolo)
130	2	t_country	2017-05-04 15:44:47.136426	UPDATE	perform literature.f_country8add(2,elseы,facks);	Изменена страна elseы(2). Язык:facks (2-else-facks)
132	33	t_country	2017-05-04 16:57:23.854906	UPDATE	perform literature.f_country8add(33,pipije,lolo);	Изменена страна pipije(33). Язык:lolo (33-pipije-lolo)
133	2	t_country	2017-05-04 16:57:25.515609	UPDATE	perform literature.f_country8add(2,else,facks);	Изменена страна else(2). Язык:facks (2-else-facks)
134	33	t_country	2017-05-04 16:57:28.397341	UPDATE	perform literature.f_country8add(33,pipije,lolo);	Изменена страна pipije(33). Язык:lolo (33-pipije-lolo)
136	33	t_country	2017-05-04 17:27:49.607216	UPDATE	perform literature.f_country8add(33,pipije,lolo);	Изменена страна pipije(33). Язык:lolo (33-pipije4-lolo)
137	33	t_country	2017-05-04 17:27:53.362769	UPDATE	perform literature.f_country8add(33,pipije4,lolo);	Изменена страна pipije4(33). Язык:lolo (33-pipije4t-lolo)
139	33	t_country	2017-05-04 17:27:56.562306	UPDATE	perform literature.f_country8add(33,pipije4t,lolo);	Изменена страна pipije4t(33). Язык:lolo (33-pipijeyfd-lolo)
144	69	t_theme	2017-05-04 22:57:18.848837	UPDATE	perform literature.f_theme8add(69,dodou,pipi,lilil);	Изменена тема dodou(69-pipi-lilil) => dodouy(69-pipi-lilil)
159	158	t_country	2017-05-04 23:53:15.822715	INSERT	perform literature.f_country8del(158);	Добавлена страна qwe(158). Язык:ewq
162	161	t_theme	2017-05-04 23:53:29.118551	INSERT	perform literature.f_theme8del(161);	Добавлена тема wea(161-rqaw-reraw)
165	164	t_theme	2017-05-04 23:53:52.279373	INSERT	perform literature.f_theme8del(164);	Добавлена тема ttt(164-ttt-ttt)
166	25	t_cycle	2017-05-05 07:35:24.673333	INSERT	perform literature.f_cycle8del(25);	Добавлен цикл ghjgfj(25-jghfgjgjh-fgjgjfgjf)
174	25	t_cycle	2017-05-05 07:49:18.048692	UPDATE	perform literature.f_country8add(25,ghjgfj,jghfgjgjh,fgjgjfgjf);	Изменен цикл ghjgfj(25-jghfgjgjh-fgjgjfgjf) => google(25-google-fgjgjfgjf)
177	176	t_cycle	2017-05-05 07:50:01.754395	INSERT	perform literature.f_cycle8del(176);	Добавлен цикл sedsd(176-sdsds-dsds)
180	33	t_country	2017-05-05 08:12:28.541425	UPDATE	perform literature.f_country8add(33,pipijeyfd,lolo);	Изменена страна pipijeyfd(33). Язык:lolo (33-moscow-city)
183	182	t_country	2017-05-05 08:12:39.926221	INSERT	perform literature.f_country8del(182);	Добавлена страна ty(182). Язык:ty
185	25	t_cycle	2017-05-05 08:12:54.969357	UPDATE	perform literature.f_country8add(25,google,google,google);	Изменен цикл google(25-google-google) => googlet(25-googlet-google)
187	164	t_theme	2017-05-05 08:13:11.182376	UPDATE	perform literature.f_theme8add(164,ttt,ttt,ttt);	Изменена тема ttt(164-ttt-ttt) => ioio(164-ioio-ttt)
190	189	t_theme	2017-05-05 08:13:30.765868	INSERT	perform literature.f_theme8del(189);	Добавлена тема qwerty(189-qwerty-qwerty)
193	59	t_genre	2017-05-05 08:35:58.056808	UPDATE	perform literature.f_genre8add(59,nunu,titi);	Изменен жанр nunu(59-titi) => nunuk(59-titi)
194	59	t_genre	2017-05-05 08:35:58.092465	UPDATE	perform literature.f_genre8add(59,nunuk,titi);	Изменен жанр nunuk(59-titi) => nunuk(59-titi)
198	197	t_genre	2017-05-05 08:36:17.048178	INSERT	perform literature.f_genre8del(197);	Добавлен жанр kl(197-kl)
201	59	t_genre	2017-05-05 08:40:49.306214	UPDATE	perform literature.f_genre8add(59,nunuk,titi);	Изменен жанр nunuk(59-titi) => nunukф(59-titi)
202	66	t_group_person	2017-05-05 10:20:18.788555	INSERT	perform literature.f_group_person8del(66);	Добавлена группа писателей ffff(66-fffff-fff-0)
203	43	t_person	2017-05-05 10:21:38.600556	INSERT	perform literature.f_person8del(43);	Добавлен писатель fadf fasdfas(43-asdfas-2017-05-05-2017-05-13-2-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66)
204	55	t_phrase	2017-05-05 11:22:12.577869	INSERT	perform literature.f_phrase8del(55);	Добавлена фраза tdrhft(55-0-rtdyty)
212	62	t_type_opus	2017-05-05 17:58:11.589935	UPDATE	perform literature.f_type_opus8add(62,fff,fff);	Изменен тип литературы fff(62-fff) => fffyy(62-fff)
214	55	t_phrase	2017-05-05 17:58:45.460306	UPDATE	perform literature.f_phrase8add(55,tdrhft,0,rtdyty);	Изменена фраза tdrhft(55-0-rtdyty) => tdrhftuuuuuuuu(55-0-rtdyty)
216	176	t_cycle	2017-05-05 18:00:16.867573	UPDATE	perform literature.f_country8add(176,sedsd,sdsds,dsds);	Изменен цикл sedsd(176-sdsds-dsds) => ddd(176-sdsds-dsds)
218	69	t_theme	2017-05-05 18:00:21.442696	UPDATE	perform literature.f_theme8add(69,dodouy,pipi,lilil);	Изменена тема dodouy(69-pipi-lilil) => ddd(69-pipi-lilil)
220	2	t_country	2017-05-05 18:00:24.461557	UPDATE	perform literature.f_country8add(2,else,facks);	Изменена страна else(2). Язык:facks (2-ddd-facks)
222	59	t_genre	2017-05-05 18:00:39.231277	UPDATE	perform literature.f_genre8add(59,nunukф,titi);	Изменен жанр nunukф(59-titi) => ddd(59-titi)
224	55	t_phrase	2017-05-05 18:00:42.888965	UPDATE	perform literature.f_phrase8add(55,tdrhftuuuuuuuu,0,rtdyty);	Изменена фраза tdrhftuuuuuuuu(55-0-rtdyty) => ddd(55-0-rtdyty)
227	62	t_type_opus	2017-05-05 18:01:06.769097	UPDATE	perform literature.f_type_opus8add(62,fffyy,fff);	Изменен тип литературы fffyy(62-fff) => ddd(62-fff)
234	62	t_type_opus	2017-05-05 18:18:24.412824	UPDATE	perform literature.f_type_opus8add(62,ddd,fff);	Изменен тип литературы ddd(62-fff) => dddffff(62-fff)
237	66	t_group_person	2017-05-05 18:30:52.659386	UPDATE	perform literature.f_group_person8add(66,ffff,fffff,fff,0);	Изменена группа писателей ffff(66-fffff-fff-0) => ddd(66-ddd-ddd-0)
240	43	t_person	2017-05-06 06:30:08.607285	UPDATE	perform literature.f_person8add(43,fadf,fasdfas,asdfas,2017-05-05,2017-05-13,2,sadfdfa,asdfdf,asdfasdf,asdfasdf,asdfasd,asdfsadf,66);	Изменен писатель fadf fasdfas(43-asdfas-2017-05-05-2017-05-13-2-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66) => fadfa fasdfas(43-asdfas-2017-05-05-2017-05-13-2-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66)
244	43	t_person	2017-05-06 06:30:58.669985	UPDATE	perform literature.f_person8add(43,fadfa,fasdfas,asdfas,2017-05-05,2017-05-13,2,sadfdfa,asdfdf,asdfasdf,asdfasdf,asdfasd,asdfsadf,66);	Изменен писатель fadfa fasdfas(43-asdfas-2017-05-05-2017-05-13-2-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66) => fadfa fasdfas(43-asdfas-2017-05-01-2017-05-02-2-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66)
246	43	t_person	2017-05-06 06:43:09.663798	UPDATE	perform literature.f_person8add(43,fadfa,fasdfas,asdfas,2017-05-01,2017-05-02,2,sadfdfa,asdfdf,asdfasdf,asdfasdf,asdfasd,asdfsadf,66);	Изменен писатель fadfa fasdfas(43-asdfas-2017-05-01-2017-05-02-2-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66) => fadfa fasdfas(43-asdfas-2017-05-01-2017-05-02-182-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66)
248	65	t_opus	2017-05-06 07:04:26.97909	UPDATE	perform literature.f_genre8add(650,cap,ann,cov,1992-05-16,библ,key,orig,0,0,59,62);	Изменено произведение cap(65-0-cap-ann-cov-1992-05-16-библ-key-orig-0-0-59-62) => cap(65-43-cap-ann-cov-1992-05-16-библ-key-orig-25-0-59-62)
251	158	t_country	2017-05-06 07:22:47.254623	UPDATE	perform literature.f_country8add(158,qwe,ewq);	Изменена страна qwe(158). Язык:ewq (158-Россия-Русский)
253	33	t_country	2017-05-06 07:23:04.658782	UPDATE	perform literature.f_country8add(33,moscow,city);	Изменена страна moscow(33). Язык:city (33-Италия-Итальянский)
255	182	t_country	2017-05-06 07:23:16.300019	UPDATE	perform literature.f_country8add(182,ty,ty);	Изменена страна ty(182). Язык:ty (182-Франция-Французский)
257	2	t_country	2017-05-06 07:23:24.576416	UPDATE	perform literature.f_country8add(2,ddd,facks);	Изменена страна ddd(2). Язык:facks (2-Китай-Китайский)
259	25	t_cycle	2017-05-06 07:33:18.349872	UPDATE	perform literature.f_country8add(25,googlet,googlet,googlet);	Изменен цикл googlet(25-googlet-googlet) => Хроники Амбера(25-Хроники-googlet)
261	176	t_cycle	2017-05-06 07:33:49.298042	UPDATE	perform literature.f_country8add(176,ddd,sdsds,dsds);	Изменен цикл ddd(176-sdsds-dsds) => Крыса из нержавеющей стали(176-Про крысу-dsds)
263	161	t_theme	2017-05-06 07:34:21.067759	UPDATE	perform literature.f_theme8add(161,wea,rqaw,reraw);	Изменена тема wea(161-rqaw-reraw) => Гражданская война(161-Война-reraw)
265	164	t_theme	2017-05-06 07:34:43.022941	UPDATE	perform literature.f_theme8add(164,ioio,ioio,oio);	Изменена тема ioio(164-ioio-oio) => Отцы и дети(164-Вечна-oio)
267	189	t_theme	2017-05-06 07:34:54.340656	UPDATE	perform literature.f_theme8add(189,qwerty,qwerty,qwerty);	Изменена тема qwerty(189-qwerty-qwerty) => Грязная проза(189-УУУ-qwerty)
269	69	t_theme	2017-05-06 07:35:13.198842	UPDATE	perform literature.f_theme8add(69,ddd,pipi,lilil);	Изменена тема ddd(69-pipi-lilil) => Одинечество(69-Одиночество-lilil)
271	197	t_genre	2017-05-06 07:35:40.522856	UPDATE	perform literature.f_genre8add(197,kl,kl);	Изменен жанр kl(197-kl) => Поэма(197-В стихах)
273	59	t_genre	2017-05-06 07:35:50.612324	UPDATE	perform literature.f_genre8add(59,ddd,titi);	Изменен жанр ddd(59-titi) => Роман(59-Прозаическиая фыорма)
276	55	t_phrase	2017-05-06 07:38:19.770023	UPDATE	perform literature.f_phrase8add(55,ddd,0,rtdyty);	Изменена фраза ddd(55-0-rtdyty) => Что бы там не было(55-0-rtdyty)
278	65	t_opus	2017-05-06 07:40:21.234979	UPDATE	perform literature.f_genre8add(6543,cap,ann,cov,1992-05-16,библ,key,orig,25,0,59,62);	Изменено произведение cap(65-43-cap-ann-cov-1992-05-16-библ-key-orig-25-0-59-62) => Хождения по мукам(65-43-Хождения по мукам-Про всю эту делягу-Линк на ковер-1992-05-16-Библиография-Про ключи-Оригинальный йазык-25-1-59-62)
280	62	t_type_opus	2017-05-06 07:40:51.077153	UPDATE	perform literature.f_type_opus8add(62,dddffff,fff);	Изменен тип литературы dddffff(62-fff) => Художественная литература(62-Для девочек)
282	66	t_group_person	2017-05-06 07:44:25.250473	UPDATE	perform literature.f_group_person8add(66,ddd,ddd,ddd,0);	Изменена группа писателей ddd(66-ddd-ddd-0) => Футуристы(66-Про ерунду-Линка-0)
284	43	t_person	2017-05-06 07:45:34.786388	UPDATE	perform literature.f_person8add(43,fadfa,fasdfas,asdfas,2017-05-01,2017-05-02,182,sadfdfa,asdfdf,asdfasdf,asdfasdf,asdfasd,asdfsadf,66);	Изменен писатель fadfa fasdfas(43-asdfas-2017-05-01-2017-05-02-182-sadfdfa-asdfdf-asdfasdf-asdfasd-asdfsadf-66) => Николай Толстой(43-Васильевич-2017-05-05-2017-05-11-182-Ключи-Аннотация-Линка-Портрет-Ббилиотграфия-66)
342	158	t_country	2017-05-06 22:14:13.261876	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
544	23	t_phrase	2017-05-08 00:55:56.532639	INSERT	perform literature.f_phrase8del(23);	Добавлена фраза sadf(23-0-asdfa)
287	43	t_person	2017-05-06 08:08:49.887764	DELETE	perform literature.f_person8add(43,Николай,Толстой,Васильевич,2017-05-05,2017-05-11,182,Ключи,Аннотация,Линка,Линка,Портрет,Ббилиотграфия,66);	Удален писатель Николай Толстой(43-Васильевич-2017-05-05-2017-05-11-182-Ключи-Аннотация-Линка-Портрет-Ббилиотграфия-66)
288	23	t_person	2017-05-06 19:55:40.717477	INSERT	perform literature.f_person8del(23);	Добавлен писатель 23 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0)
290	33	t_country	2017-05-06 20:06:10.792937	UPDATE	perform literature.f_country8add(33,Италия,Итальянский);	Изменена страна Италия(33). Язык:Итальянский (33-Пинталия-Итальянский)
292	33	t_country	2017-05-06 20:11:37.052318	UPDATE	perform literature.f_country8add(33,Пинталия,Итальянский);	Изменена страна Пинталия(33). Язык:Итальянский (33-Италия-Итальянский)
295	294	t_country	2017-05-06 20:11:48.61544	INSERT	perform literature.f_country8del(294);	Добавлена страна Питаай(294). Язык:Питаайский
297	33	t_country	2017-05-06 20:31:34.511609	UPDATE	perform literature.f_country8add(33,Италия,Итальянский);	Изменена страна Италия(33). Язык:Итальянский (33-GИталия-Итальянский)
299	33	t_country	2017-05-06 20:35:29.840321	UPDATE	perform literature.f_country8add(33,GИталия,Итальянский);	Изменена страна GИталия(33). Язык:Итальянский (33-Италия-Итальянский)
301	294	t_country	2017-05-06 20:39:11.809109	UPDATE	perform literature.f_country8add(294,Питаай,Питаайский);	Изменена страна Питаай(294). Язык:Питаайский (294-Пnитаай-Питаайский)
302	294	t_country	2017-05-06 20:39:15.725287	UPDATE	perform literature.f_country8add(294,Пnитаай,Питаайский);	Изменена страна Пnитаай(294). Язык:Питаайский (294-Пssssитаай-Питаайский)
306	294	t_country	2017-05-06 20:39:42.545808	UPDATE	perform literature.f_country8add(294,Пssssитаай,Питаайский);	Изменена страна Пssssитаай(294). Язык:Питаайский (294-Китай-Китай)
308	158	t_country	2017-05-06 21:38:42.50176	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россияj-Русский)
310	158	t_country	2017-05-06 21:39:00.20344	UPDATE	perform literature.f_country8add(158,Россияj,Русский);	Изменена страна Россияj(158). Язык:Русский (158-Росси-Русский)
312	158	t_country	2017-05-06 21:39:10.983584	UPDATE	perform literature.f_country8add(158,Росси,Русский);	Изменена страна Росси(158). Язык:Русский (158-Россияjh-Русский)
314	158	t_country	2017-05-06 21:49:40.926075	UPDATE	perform literature.f_country8add(158,Россияjh,Русский);	Изменена страна Россияjh(158). Язык:Русский (158-Россия-Русский)
316	158	t_country	2017-05-06 21:49:52.025908	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Росси-Русский)
318	158	t_country	2017-05-06 21:53:19.537852	UPDATE	perform literature.f_country8add(158,Росси,Русский);	Изменена страна Росси(158). Язык:Русский (158-Россия-Русский)
320	158	t_country	2017-05-06 21:59:57.099606	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россияa-Русский)
322	158	t_country	2017-05-06 22:00:03.060731	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
325	158	t_country	2017-05-06 22:07:29.179238	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россия-Русский)
326	158	t_country	2017-05-06 22:07:29.17959	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россия-Русский)
329	158	t_country	2017-05-06 22:07:43.140306	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россия-Русский)
330	158	t_country	2017-05-06 22:07:43.14228	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россия-Русский)
333	158	t_country	2017-05-06 22:14:08.36235	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россияa-Русский)
334	158	t_country	2017-05-06 22:14:08.369935	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
335	158	t_country	2017-05-06 22:14:12.308565	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
336	158	t_country	2017-05-06 22:14:12.320382	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
337	158	t_country	2017-05-06 22:14:12.955874	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
338	158	t_country	2017-05-06 22:14:12.96233	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
339	158	t_country	2017-05-06 22:14:13.113381	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
340	158	t_country	2017-05-06 22:14:13.125841	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
341	158	t_country	2017-05-06 22:14:13.25588	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
343	158	t_country	2017-05-06 22:14:13.370266	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
345	158	t_country	2017-05-06 22:14:13.398769	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
346	158	t_country	2017-05-06 22:14:13.496675	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
347	158	t_country	2017-05-06 22:14:13.503185	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
348	158	t_country	2017-05-06 22:14:13.60833	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
349	158	t_country	2017-05-06 22:14:13.616634	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
350	158	t_country	2017-05-06 22:14:13.724513	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
351	158	t_country	2017-05-06 22:14:13.730479	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
352	158	t_country	2017-05-06 22:14:13.840063	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
353	158	t_country	2017-05-06 22:14:13.845783	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
354	158	t_country	2017-05-06 22:14:13.981112	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
355	158	t_country	2017-05-06 22:14:13.987678	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
356	158	t_country	2017-05-06 22:14:18.30584	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
357	158	t_country	2017-05-06 22:14:18.312598	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
360	158	t_country	2017-05-06 22:14:18.627034	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
361	158	t_country	2017-05-06 22:14:18.630437	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
362	158	t_country	2017-05-06 22:14:18.832306	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
363	158	t_country	2017-05-06 22:14:18.836448	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
364	158	t_country	2017-05-06 22:14:18.986733	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
365	158	t_country	2017-05-06 22:14:18.991861	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
366	158	t_country	2017-05-06 22:14:19.087492	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
367	158	t_country	2017-05-06 22:14:19.092372	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россияa-Русский)
369	158	t_country	2017-05-06 22:21:24.021187	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россия-Русский)
371	158	t_country	2017-05-06 22:21:34.293641	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россияaqqq-Русский)
373	158	t_country	2017-05-06 22:24:34.458127	UPDATE	perform literature.f_country8add(158,Россияaqqq,Русский);	Изменена страна Россияaqqq(158). Язык:Русский (158-Россияa-Русский)
375	158	t_country	2017-05-06 22:28:03.407617	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россия-Русский)
377	158	t_country	2017-05-06 23:06:51.076451	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россияy-Русский)
379	158	t_country	2017-05-06 23:44:13.394311	UPDATE	perform literature.f_country8add(158,Россияy,Русский);	Изменена страна Россияy(158). Язык:Русский (158-Россия-Русский)
381	158	t_country	2017-05-06 23:46:55.120067	UPDATE	perform literature.f_country8add(158,Россия,Русский);	Изменена страна Россия(158). Язык:Русский (158-Россияa-Русский)
383	158	t_country	2017-05-07 00:02:54.912026	UPDATE	perform literature.f_country8add(158,Россияa,Русский);	Изменена страна Россияa(158). Язык:Русский (158-Россия-Русский)
386	23	t_person	2017-05-07 17:47:42.716484	UPDATE	perform literature.f_person8add(23,23,23,23,2017-05-06,2017-05-13,2,23,23,23,23,23,23,0);	Изменен писатель 23 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0) => My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0)
448	409	t_country	2017-05-07 21:06:50.380222	UPDATE	perform literature.f_country8add(409,Бубликo,Бублик);	Изменена страна Бубликo(409). Язык:Бублик (409-Бублик-Бублик)
387	23	t_person	2017-05-07 17:47:42.716709	UPDATE	perform literature.f_person8add(23,My person,23,23,2017-05-06,2017-05-13,2,23,23,23,23,23,23,0);	Изменен писатель My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0) => My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0)
388	23	t_person	2017-05-07 17:47:43.964223	UPDATE	perform literature.f_person8add(23,My person,23,23,2017-05-06,2017-05-13,2,23,23,23,23,23,23,0);	Изменен писатель My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0) => My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0)
389	23	t_person	2017-05-07 17:47:43.968331	UPDATE	perform literature.f_person8add(23,My person,23,23,2017-05-06,2017-05-13,2,23,23,23,23,23,23,0);	Изменен писатель My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0) => My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0)
391	182	t_country	2017-05-07 17:48:05.420875	UPDATE	perform literature.f_country8add(182,Франция,Французский);	Изменена страна Франция(182). Язык:Французский (182-Ирландия-Английский)
393	2	t_country	2017-05-07 17:53:41.47665	UPDATE	perform literature.f_country8add(2,Китай,Китайский);	Изменена страна Китай(2). Язык:Китайский (2-Китайка-Китайский)
395	2	t_country	2017-05-07 17:57:52.932685	UPDATE	perform literature.f_country8add(2,Китайка,Китайский);	Изменена страна Китайка(2). Язык:Китайский (2-Китай-Китайский)
397	294	t_country	2017-05-07 18:04:03.441445	UPDATE	perform literature.f_country8add(294,Китай,Китай);	Изменена страна Китай(294). Язык:Китай (294-Китайка-Китай)
399	294	t_country	2017-05-07 18:04:21.557996	UPDATE	perform literature.f_country8add(294,Китайка,Китай);	Изменена страна Китайка(294). Язык:Китай (294-Китай-Китай)
401	2	t_country	2017-05-07 18:04:30.581178	UPDATE	perform literature.f_country8add(2,Китай,Китайский);	Изменена страна Китай(2). Язык:Китайский (2-Китайка-Китайский)
403	2	t_country	2017-05-07 18:08:58.226686	UPDATE	perform literature.f_country8add(2,Китайка,Китайский);	Изменена страна Китайка(2). Язык:Китайский (2-Китай-Китайский)
405	2	t_country	2017-05-07 18:09:09.410954	UPDATE	perform literature.f_country8add(2,Китай,Китайский);	Изменена страна Китай(2). Язык:Китайский (2-Китайgt-Китайский)
407	2	t_country	2017-05-07 18:11:38.42561	UPDATE	perform literature.f_country8add(2,Китайgt,Китайский);	Изменена страна Китайgt(2). Язык:Китайский (2-Китай-Китайский)
410	409	t_country	2017-05-07 18:11:52.842066	INSERT	perform literature.f_country8del(409);	Добавлена страна Бублик(409). Язык:Бублик
412	409	t_country	2017-05-07 18:20:39.480777	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликa-Бублик)
414	409	t_country	2017-05-07 18:25:27.422339	UPDATE	perform literature.f_country8add(409,Бубликa,Бублик);	Изменена страна Бубликa(409). Язык:Бублик (409-Бублик-Бублик)
416	409	t_country	2017-05-07 18:25:37.415094	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликb-Бублик)
418	409	t_country	2017-05-07 18:28:53.402209	UPDATE	perform literature.f_country8add(409,Бубликb,Бублик);	Изменена страна Бубликb(409). Язык:Бублик (409-Бублик-Бублик)
420	409	t_country	2017-05-07 18:41:27.020071	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликf-Бублик)
422	409	t_country	2017-05-07 18:44:57.61051	UPDATE	perform literature.f_country8add(409,Бубликf,Бублик);	Изменена страна Бубликf(409). Язык:Бублик (409-Бублик-Бублик)
424	409	t_country	2017-05-07 18:46:24.330643	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликf-Бублик)
426	409	t_country	2017-05-07 18:47:25.896366	UPDATE	perform literature.f_country8add(409,Бубликf,Бублик);	Изменена страна Бубликf(409). Язык:Бублик (409-Бублик-Бублик)
428	2	t_country	2017-05-07 18:49:58.918306	UPDATE	perform literature.f_country8add(2,Китай,Китайский);	Изменена страна Китай(2). Язык:Китайский (2-Индия-Индийский)
430	409	t_country	2017-05-07 19:04:58.642693	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликf-Бублик)
432	409	t_country	2017-05-07 19:06:05.992799	UPDATE	perform literature.f_country8add(409,Бубликf,Бублик);	Изменена страна Бубликf(409). Язык:Бублик (409-Бублик-Бублик)
434	409	t_country	2017-05-07 19:07:06.625777	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликy-Бублик)
436	409	t_country	2017-05-07 19:12:43.707056	UPDATE	perform literature.f_country8add(409,Бубликy,Бублик);	Изменена страна Бубликy(409). Язык:Бублик (409-Бублик-Бублик)
438	409	t_country	2017-05-07 19:16:09.000965	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублиu-Бублик)
440	409	t_country	2017-05-07 19:16:18.378227	UPDATE	perform literature.f_country8add(409,Бублиu,Бублик);	Изменена страна Бублиu(409). Язык:Бублик (409-Бублиue-Бублик)
442	409	t_country	2017-05-07 19:39:13.798198	UPDATE	perform literature.f_country8add(409,Бублиue,Бублик);	Изменена страна Бублиue(409). Язык:Бублик (409-Бублик-Бублик)
444	409	t_country	2017-05-07 19:39:28.504152	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик-Бублик)
446	409	t_country	2017-05-07 19:44:15.513701	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликo-Бублик)
540	23	t_phrase	2017-05-08 00:47:46.632909	INSERT	perform literature.f_phrase8del(23);	Добавлена фраза ывфава(23-0-фываыа)
450	409	t_country	2017-05-07 21:09:33.196295	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бубликo-Бублик)
452	409	t_country	2017-05-07 21:09:44.624668	UPDATE	perform literature.f_country8add(409,Бубликo,Бублик);	Изменена страна Бубликo(409). Язык:Бублик (409-Бублик-Бублик)
454	409	t_country	2017-05-07 21:11:48.12156	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
456	409	t_country	2017-05-07 21:14:08.407811	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
458	409	t_country	2017-05-07 21:16:30.025055	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
460	409	t_country	2017-05-07 21:17:53.479603	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
462	409	t_country	2017-05-07 21:21:05.211672	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
464	409	t_country	2017-05-07 21:29:29.808737	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
466	409	t_country	2017-05-07 21:33:01.109251	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
468	409	t_country	2017-05-07 21:33:08.797971	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик01-Бублик)
470	409	t_country	2017-05-07 21:36:41.990208	UPDATE	perform literature.f_country8add(409,Бублик01,Бублик);	Изменена страна Бублик01(409). Язык:Бублик (409-Бублик-Бублик)
472	409	t_country	2017-05-07 21:39:55.423109	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
474	409	t_country	2017-05-07 21:57:23.687694	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
476	409	t_country	2017-05-07 22:01:24.510937	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
478	409	t_country	2017-05-07 22:03:05.503265	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
480	409	t_country	2017-05-07 22:04:34.281146	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
482	409	t_country	2017-05-07 22:06:50.507375	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
484	409	t_country	2017-05-07 22:11:50.116301	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
486	409	t_country	2017-05-07 22:12:30.335414	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
488	409	t_country	2017-05-07 22:16:22.650271	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
489	409	t_country	2017-05-07 22:16:26.961452	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
491	409	t_country	2017-05-07 22:20:03.719311	UPDATE	perform literature.f_country8add(409,Бублик,Бублик);	Изменена страна Бублик(409). Язык:Бублик (409-Бублик0-Бублик)
492	409	t_country	2017-05-07 22:20:06.748891	UPDATE	perform literature.f_country8add(409,Бублик0,Бублик);	Изменена страна Бублик0(409). Язык:Бублик (409-Бублик-Бублик)
511	409	t_country	2017-05-07 22:43:26.552569	DELETE	perform literature.f_country8add(409,Бублик,Бублик);	Удалена страна Бублик(409). Язык:Бублик
516	158	t_country	2017-05-07 22:44:01.06702	DELETE	perform literature.f_country8add(158,Россия,Русский);	Удалена страна Россия(158). Язык:Русский
518	182	t_country	2017-05-07 22:44:02.573046	DELETE	perform literature.f_country8add(182,Ирландия,Английский);	Удалена страна Ирландия(182). Язык:Английский
520	33	t_country	2017-05-07 22:44:03.740351	DELETE	perform literature.f_country8add(33,Италия,Итальянский);	Удалена страна Италия(33). Язык:Итальянский
522	294	t_country	2017-05-07 22:44:04.527077	DELETE	perform literature.f_country8add(294,Китай,Китай);	Удалена страна Китай(294). Язык:Китай
527	176	t_cycle	2017-05-07 22:44:10.543812	DELETE	perform literature.f_country8add(176,Крыса из нержавеющей стали,Про крысу,Линк);	Удален цикл Крыса из нержавеющей стали(176-Про крысу-Линк)
530	55	t_phrase	2017-05-07 22:44:14.768632	DELETE	perform literature.f_phrase8add(55,Что бы там не было,0,Что бы там не было);	Удалена фраза Что бы там не было(55-0-Что бы там не было)
531	55	t_phrase	2017-05-07 23:16:33.999224	INSERT	perform literature.f_phrase8del(55);	Добавлена фраза res(55-0-gerge)
534	533	t_phrase	2017-05-07 23:18:46.661425	INSERT	perform literature.f_phrase8del(533);	Добавлена фраза tfguyfguj(533-0-gfjhfg)
537	533	t_phrase	2017-05-07 23:24:37.961277	DELETE	perform literature.f_phrase8add(533,tfguyfguj,0,gfjhfg);	Удалена фраза tfguyfguj(533-0-gfjhfg)
539	55	t_phrase	2017-05-07 23:24:39.112662	DELETE	perform literature.f_phrase8add(55,res,0,gerge);	Удалена фраза res(55-0-gerge)
543	23	t_phrase	2017-05-08 00:54:40.788629	DELETE	perform literature.f_phrase8add(23,ывфава,0,фываыа);	Удалена фраза ывфава(23-0-фываыа)
547	66	t_group_person	2017-05-08 01:44:33.14301	UPDATE	perform literature.f_group_person8add(66,Футуристы,Про ерунду,Линка,0);	Изменена группа писателей Футуристы(66-Про ерунду-Линка-0) => Футуристы(66-Про ерунду-Линка-66)
548	66	t_group_person	2017-05-08 01:47:25.207373	UPDATE	perform literature.f_group_person8add(66,Футуристы,Про ерунду,Линка,66);	Изменена группа писателей Футуристы(66-Про ерунду-Линка-66) => Футуристы(66-Про ерунду-Линка-0)
550	23	t_person	2017-05-08 01:50:33.105061	UPDATE	perform literature.f_person8add(23,My person,23,23,2017-05-06,2017-05-13,2,23,23,23,23,23,23,0);	Изменен писатель My person 23(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0) => My person Гого(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0)
552	66	t_group_person	2017-05-08 01:50:39.123911	UPDATE	perform literature.f_group_person8add(66,Футуристы,Про ерунду,Линка,0);	Изменена группа писателей Футуристы(66-Про ерунду-Линка-0) => Футуристы1(66-Про ерунду-Линка-0)
554	65	t_opus	2017-05-08 01:50:42.416615	UPDATE	perform literature.f_genre8add(6543,Хождения по мукам,Про всю эту делягу,Линк на ковер,1992-05-16,Библиография,Про ключи,Оригинальный йазык,25,1,59,62);	Изменено произведение Хождения по мукам(65-43-Хождения по мукам-Про всю эту делягу-Линк на ковер-1992-05-16-Библиография-Про ключи-Оригинальный йазык-25-1-59-62) => Хождения по мукамц(65-43-Хождения по мукамц-Про всю эту делягу-Линк на ковер-1992-05-16-Библиография-Про ключи-Оригинальный йазык-25-1-59-62)
556	62	t_type_opus	2017-05-08 01:50:46.807879	UPDATE	perform literature.f_type_opus8add(62,Художественная литература,Для девочек);	Изменен тип литературы Художественная литература(62-Для девочек) => Художественная литературац(62-Для девочек)
558	23	t_phrase	2017-05-08 01:50:52.556264	UPDATE	perform literature.f_phrase8add(23,sadf,0,asdfa);	Изменена фраза sadf(23-0-asdfa) => ццц(23-0-asdfa)
560	197	t_genre	2017-05-08 01:50:56.776204	UPDATE	perform literature.f_genre8add(197,Поэма,В стихах);	Изменен жанр Поэма(197-В стихах) => Поэмац(197-В стихах)
562	25	t_cycle	2017-05-08 01:51:06.324815	UPDATE	perform literature.f_country8add(25,Хроники Амбера,Хроники,ДЛик);	Изменен цикл Хроники Амбера(25-Хроники-ДЛик) => Хроники Амбера(25-Хроникиц-ДЛик)
563	25	t_cycle	2017-05-08 01:51:08.676088	UPDATE	perform literature.f_country8add(25,Хроники Амбера,Хроникиц,ДЛик);	Изменен цикл Хроники Амбера(25-Хроникиц-ДЛик) => Хроники Амберац(25-Хроникиц-ДЛик)
565	161	t_theme	2017-05-08 01:51:11.973525	UPDATE	perform literature.f_theme8add(161,Гражданская война,Война,Линк);	Изменена тема Гражданская война(161-Война-Линк) => Гражданская войнац(161-Война-Линк)
567	2	t_country	2017-05-08 01:51:19.794338	UPDATE	perform literature.f_country8add(2,Индия,Индийский);	Изменена страна Индия(2). Язык:Индийский (2-Индияц-Индийский)
569	2	t_country	2017-05-08 02:30:55.679722	UPDATE	perform literature.f_country8add(2,Индияц,Индийский);	Изменена страна Индияц(2). Язык:Индийский (2-Россия-Русский)
571	161	t_theme	2017-05-08 02:32:49.231704	UPDATE	perform literature.f_theme8add(161,Гражданская войнац,Война,Линк);	Изменена тема Гражданская войнац(161-Война-Линк) => Гражданская война(161-1917 год в России-Линк)
573	164	t_theme	2017-05-08 02:32:52.030448	DELETE	perform literature.f_theme8add(164,Отцы и дети,Вечна,линк);	Удалена тема Отцы и дети(164-Вечна-линк)
575	189	t_theme	2017-05-08 02:32:53.000875	DELETE	perform literature.f_theme8add(189,Грязная проза,УУУ,Грязь);	Удалена тема Грязная проза(189-УУУ-Грязь)
578	23	t_person	2017-05-08 02:35:45.44642	UPDATE	perform literature.f_person8add(23,My person,Гого,23,2017-05-06,2017-05-13,2,23,23,23,23,23,23,0);	Изменен писатель My person Гого(23-23-2017-05-06-2017-05-13-2-23-23-23-23-23-0) => Алексей Толстой(23-Николаевич-1882-12-29-1945-02-23-2-Толстой-Граф Алексе́й Никола́евич Толсто́й (29 декабря 1882 (10 января 1883), Николаевск, Самарская губерния — 23 февраля 1945, Москва) — русский советский писатель и общественный деятель из рода Толстых. Автор социально-психологических, исторических и научно-фантастических романов, повестей и рассказов, публицистических произведений. Лауреат трёх Сталинских премий первой степени (1941; 1943; 1946, посмертно).-https://ru.wikipedia.org/wiki/%D0%A2%D0%BE%D0%BB%D1%81%D1%82%D0%BE%D0%B9,_%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9_%D0%9D%D0%B8%D0%BA%D0%BE%D0%BB%D0%B0%D0%B5%D0%B2%D0%B8%D1%87-https://upload.wikimedia.org/wikipedia/commons/2/20/ANTolstoy.jpg--0)
580	25	t_cycle	2017-05-08 02:36:31.392324	UPDATE	perform literature.f_country8add(25,Хроники Амберац,Хроникиц,ДЛик);	Изменен цикл Хроники Амберац(25-Хроникиц-ДЛик) => Хождения по мукам(25-«Хождение по мукам» — трилогия романов А. Н. Толстого, прослеживающая судьбы русской интеллигенции накануне, во время и после революционных событий 1917 года. Состоит из романов «Сёстры» (1921—1922), «Восемнадцатый год» (1927—1928) и «Хмурое утро» (1940—1941).-ДЛик)
582	65	t_opus	2017-05-08 02:58:36.843986	UPDATE	perform literature.f_genre8add(6543,Хождения по мукамц,Про всю эту делягу,Линк на ковер,1992-05-16,Библиография,Про ключи,Оригинальный йазык,25,1,59,62);	Изменено произведение Хождения по мукамц(65-43-Хождения по мукамц-Про всю эту делягу-Линк на ковер-1992-05-16-Библиография-Про ключи-Оригинальный йазык-25-1-59-62) => Хождение по мукам(65-23-Хождение по мукам-«Хождение по мукам» — трилогия романов А. Н. Толстого, прослеживающая судьбы русской интеллигенции накануне, во время и после революционных событий 1917 года. Состоит из романов «Сёстры» (1921—1922), «Восемнадцатый год» (1927—1928) и «Хмурое утро» (1940—1941).-https://upload.wikimedia.org/wikipedia/ru/9/97/%D0%A5%D0%BE%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5_%D0%BF%D0%BE_%D0%BC%D1%83%D0%BA%D0%B0%D0%BC.jpg-1921-01-01-Автор\tАлексей Толстой Формат издания \t130х200 мм (средний формат) Количество страниц\t832 Год выпуска\t2004 ISBN\t5-699-07178-4, 5-04-002362-6 Тираж\t3000 Издательство\tЭксмо Переплет\tТвердый переплет Язык издания\tРусский Тип издания\tОтдельное издание Вес в упаковке, г\t580-Бессонов, Катя, Телегин, отец врач, Махно, Деникин, Колчак, чехословаки, Октябрьская революция, Рощин, хмурое утро, сестры, восемнадцатый год,-Русский-25-1-59-62)
584	62	t_type_opus	2017-05-08 02:59:42.952341	UPDATE	perform literature.f_type_opus8add(62,Художественная литературац,Для девочек);	Изменен тип литературы Художественная литературац(62-Для девочек) => Художественная литература(62-Художественная литература — вид искусства, использующий в качестве единственного материала слова и конструкции естественного языка.)
586	23	t_phrase	2017-05-08 03:01:22.722207	UPDATE	perform literature.f_phrase8add(23,ццц,0,asdfa);	Изменена фраза ццц(23-0-asdfa) => В трех водах топлено, в трех кровях купано, в трех щелоках варено. Чище мы чистого.(23-23-asdfa)
588	197	t_genre	2017-05-08 03:01:33.771781	UPDATE	perform literature.f_genre8add(197,Поэмац,В стихах);	Изменен жанр Поэмац(197-В стихах) => Поэма(197-В стихах)
590	66	t_group_person	2017-05-08 03:01:47.469874	UPDATE	perform literature.f_group_person8add(66,Футуристы1,Про ерунду,Линка,0);	Изменена группа писателей Футуристы1(66-Про ерунду-Линка-0) => Футуристы(66-Футуристы-Футуристы-0)
592	69	t_theme	2017-05-08 03:02:16.347106	UPDATE	perform literature.f_theme8add(69,Одинечество,Одиночество,Линк);	Изменена тема Одинечество(69-Одиночество-Линк) => Одиночество(69-Одиночество-Линк)
\.


--
-- TOC entry 3281 (class 0 OID 27542)
-- Dependencies: 187
-- Data for Name: t_opus; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_opus (id, pid, caption, annotation, cover, published, bibliography, key_word, original_lang, cycle, cycle_order, genre, type) FROM stdin;
65	23	Хождение по мукам	«Хождение по мукам» — трилогия романов А. Н. Толстого, прослеживающая судьбы русской интеллигенции накануне, во время и после революционных событий 1917 года. Состоит из романов «Сёстры» (1921—1922), «Восемнадцатый год» (1927—1928) и «Хмурое утро» (1940—1941).	https://upload.wikimedia.org/wikipedia/ru/9/97/%D0%A5%D0%BE%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5_%D0%BF%D0%BE_%D0%BC%D1%83%D0%BA%D0%B0%D0%BC.jpg	1921-01-01	Автор\tАлексей Толстой Формат издания \t130х200 мм (средний формат) Количество страниц\t832 Год выпуска\t2004 ISBN\t5-699-07178-4, 5-04-002362-6 Тираж\t3000 Издательство\tЭксмо Переплет\tТвердый переплет Язык издания\tРусский Тип издания\tОтдельное издание Вес в упаковке, г\t580	Бессонов, Катя, Телегин, отец врач, Махно, Деникин, Колчак, чехословаки, Октябрьская революция, Рощин, хмурое утро, сестры, восемнадцатый год,	Русский	25	1	59	62
\.


--
-- TOC entry 3286 (class 0 OID 27622)
-- Dependencies: 192
-- Data for Name: t_opus_theme; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_opus_theme (id, opus, theme) FROM stdin;
72	65	69
\.


--
-- TOC entry 3277 (class 0 OID 27487)
-- Dependencies: 183
-- Data for Name: t_person; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_person (id, name, surname, last_name, birth_date, death_date, country, key_word, annotation, link, portrait, bibliography, group_person) FROM stdin;
23	Алексей	Толстой	Николаевич	1882-12-29	1945-02-23	2	Толстой	Граф Алексе́й Никола́евич Толсто́й (29 декабря 1882 (10 января 1883), Николаевск, Самарская губерния — 23 февраля 1945, Москва) — русский советский писатель и общественный деятель из рода Толстых. Автор социально-психологических, исторических и научно-фантастических романов, повестей и рассказов, публицистических произведений. Лауреат трёх Сталинских премий первой степени (1941; 1943; 1946, посмертно).	https://ru.wikipedia.org/wiki/%D0%A2%D0%BE%D0%BB%D1%81%D1%82%D0%BE%D0%B9,_%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9_%D0%9D%D0%B8%D0%BA%D0%BE%D0%BB%D0%B0%D0%B5%D0%B2%D0%B8%D1%87	https://upload.wikimedia.org/wikipedia/commons/2/20/ANTolstoy.jpg		\N
\.


--
-- TOC entry 3280 (class 0 OID 27527)
-- Dependencies: 186
-- Data for Name: t_phrase; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_phrase (id, pid, caption, original) FROM stdin;
23	23	В трех водах топлено, в трех кровях купано, в трех щелоках варено. Чище мы чистого.	В трех водах топлено, в трех кровях купано, в трех щелоках варено. Чище мы чистого.
\.


--
-- TOC entry 3285 (class 0 OID 27610)
-- Dependencies: 191
-- Data for Name: t_theme; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_theme (id, caption, description, link) FROM stdin;
161	Гражданская война	1917 год в России	https://ru.wikipedia.org/wiki/%D0%A0%D1%83%D1%81%D1%81%D0%BA%D0%B0%D1%8F_%D1%80%D0%B5%D0%B2%D0%BE%D0%BB%D1%8E%D1%86%D0%B8%D1%8F
69	Одиночество	Одиночество	Одиночество
\.


--
-- TOC entry 3284 (class 0 OID 27593)
-- Dependencies: 190
-- Data for Name: t_type_opus; Type: TABLE DATA; Schema: literature; Owner: shmihshmih
--

COPY t_type_opus (id, caption, description) FROM stdin;
62	Художественная литература	Художественная литература — вид искусства, использующий в качестве единственного материала слова и конструкции естественного языка.
\.


--
-- TOC entry 3086 (class 2606 OID 27499)
-- Name: t_country_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_country
    ADD CONSTRAINT t_country_pkey PRIMARY KEY (id);


--
-- TOC entry 3104 (class 2606 OID 27563)
-- Name: t_cycle_caption_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_cycle
    ADD CONSTRAINT t_cycle_caption_key UNIQUE (caption);


--
-- TOC entry 3106 (class 2606 OID 27565)
-- Name: t_cycle_description_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_cycle
    ADD CONSTRAINT t_cycle_description_key UNIQUE (description);


--
-- TOC entry 3108 (class 2606 OID 27567)
-- Name: t_cycle_link_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_cycle
    ADD CONSTRAINT t_cycle_link_key UNIQUE (link);


--
-- TOC entry 3110 (class 2606 OID 27561)
-- Name: t_cycle_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_cycle
    ADD CONSTRAINT t_cycle_pkey PRIMARY KEY (id);


--
-- TOC entry 3112 (class 2606 OID 27585)
-- Name: t_genre_caption_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_genre
    ADD CONSTRAINT t_genre_caption_key UNIQUE (caption);


--
-- TOC entry 3114 (class 2606 OID 27587)
-- Name: t_genre_description_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_genre
    ADD CONSTRAINT t_genre_description_key UNIQUE (description);


--
-- TOC entry 3116 (class 2606 OID 27583)
-- Name: t_genre_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_genre
    ADD CONSTRAINT t_genre_pkey PRIMARY KEY (id);


--
-- TOC entry 3088 (class 2606 OID 27514)
-- Name: t_group_person_caption_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_group_person
    ADD CONSTRAINT t_group_person_caption_key UNIQUE (caption);


--
-- TOC entry 3090 (class 2606 OID 27516)
-- Name: t_group_person_description_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_group_person
    ADD CONSTRAINT t_group_person_description_key UNIQUE (description);


--
-- TOC entry 3092 (class 2606 OID 27512)
-- Name: t_group_person_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_group_person
    ADD CONSTRAINT t_group_person_pkey PRIMARY KEY (id);


--
-- TOC entry 3132 (class 2606 OID 27746)
-- Name: t_log_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_log
    ADD CONSTRAINT t_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3098 (class 2606 OID 27553)
-- Name: t_opus_annotation_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus
    ADD CONSTRAINT t_opus_annotation_key UNIQUE (annotation);


--
-- TOC entry 3100 (class 2606 OID 27551)
-- Name: t_opus_caption_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus
    ADD CONSTRAINT t_opus_caption_key UNIQUE (caption);


--
-- TOC entry 3102 (class 2606 OID 27549)
-- Name: t_opus_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus
    ADD CONSTRAINT t_opus_pkey PRIMARY KEY (id);


--
-- TOC entry 3130 (class 2606 OID 27626)
-- Name: t_opus_theme_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus_theme
    ADD CONSTRAINT t_opus_theme_pkey PRIMARY KEY (id);


--
-- TOC entry 3084 (class 2606 OID 27494)
-- Name: t_person_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_person
    ADD CONSTRAINT t_person_pkey PRIMARY KEY (id);


--
-- TOC entry 3094 (class 2606 OID 27536)
-- Name: t_phrase_caption_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_phrase
    ADD CONSTRAINT t_phrase_caption_key UNIQUE (caption);


--
-- TOC entry 3096 (class 2606 OID 27534)
-- Name: t_phrase_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_phrase
    ADD CONSTRAINT t_phrase_pkey PRIMARY KEY (id);


--
-- TOC entry 3124 (class 2606 OID 27619)
-- Name: t_theme_caption_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_theme
    ADD CONSTRAINT t_theme_caption_key UNIQUE (caption);


--
-- TOC entry 3126 (class 2606 OID 27621)
-- Name: t_theme_description_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_theme
    ADD CONSTRAINT t_theme_description_key UNIQUE (description);


--
-- TOC entry 3128 (class 2606 OID 27617)
-- Name: t_theme_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_theme
    ADD CONSTRAINT t_theme_pkey PRIMARY KEY (id);


--
-- TOC entry 3118 (class 2606 OID 27602)
-- Name: t_type_opus_caption_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_type_opus
    ADD CONSTRAINT t_type_opus_caption_key UNIQUE (caption);


--
-- TOC entry 3120 (class 2606 OID 27604)
-- Name: t_type_opus_description_key; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_type_opus
    ADD CONSTRAINT t_type_opus_description_key UNIQUE (description);


--
-- TOC entry 3122 (class 2606 OID 27600)
-- Name: t_type_opus_pkey; Type: CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_type_opus
    ADD CONSTRAINT t_type_opus_pkey PRIMARY KEY (id);


--
-- TOC entry 3143 (class 2620 OID 27771)
-- Name: t_country_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_country_tr AFTER INSERT OR DELETE OR UPDATE ON t_country FOR EACH ROW EXECUTE PROCEDURE tr_country8add();


--
-- TOC entry 3147 (class 2620 OID 27774)
-- Name: t_cycle_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_cycle_tr AFTER INSERT OR DELETE OR UPDATE ON t_cycle FOR EACH ROW EXECUTE PROCEDURE tr_cycle8add();


--
-- TOC entry 3148 (class 2620 OID 27778)
-- Name: t_genre_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_genre_tr AFTER INSERT OR DELETE OR UPDATE ON t_genre FOR EACH ROW EXECUTE PROCEDURE tr_genre8add();


--
-- TOC entry 3144 (class 2620 OID 27780)
-- Name: t_group_person_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_group_person_tr AFTER INSERT OR DELETE OR UPDATE ON t_group_person FOR EACH ROW EXECUTE PROCEDURE tr_group_person8add();


--
-- TOC entry 3151 (class 2620 OID 27786)
-- Name: t_opus_theme_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_opus_theme_tr AFTER INSERT OR DELETE OR UPDATE ON t_opus_theme FOR EACH ROW EXECUTE PROCEDURE public.tr_opus_theme8add();


--
-- TOC entry 3146 (class 2620 OID 27782)
-- Name: t_opus_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_opus_tr AFTER INSERT OR DELETE OR UPDATE ON t_opus FOR EACH ROW EXECUTE PROCEDURE public.tr_opus8add();


--
-- TOC entry 3142 (class 2620 OID 27790)
-- Name: t_person_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_person_tr AFTER INSERT OR DELETE OR UPDATE ON t_person FOR EACH ROW EXECUTE PROCEDURE public.tr_person8add();


--
-- TOC entry 3145 (class 2620 OID 27793)
-- Name: t_phrase_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_phrase_tr AFTER INSERT OR DELETE OR UPDATE ON t_phrase FOR EACH ROW EXECUTE PROCEDURE public.tr_phrase8add();


--
-- TOC entry 3150 (class 2620 OID 27788)
-- Name: t_theme_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_theme_tr AFTER INSERT OR DELETE OR UPDATE ON t_theme FOR EACH ROW EXECUTE PROCEDURE public.tr_theme8add();


--
-- TOC entry 3149 (class 2620 OID 27784)
-- Name: t_type_opus_tr; Type: TRIGGER; Schema: literature; Owner: shmihshmih
--

CREATE TRIGGER t_type_opus_tr AFTER INSERT OR DELETE OR UPDATE ON t_type_opus FOR EACH ROW EXECUTE PROCEDURE public.tr_type_opus8add();


--
-- TOC entry 3135 (class 2606 OID 27522)
-- Name: t_group_person_fk; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_group_person
    ADD CONSTRAINT t_group_person_fk FOREIGN KEY (previous) REFERENCES t_group_person(id);


--
-- TOC entry 3137 (class 2606 OID 27568)
-- Name: t_opus_fk; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus
    ADD CONSTRAINT t_opus_fk FOREIGN KEY (cycle) REFERENCES t_cycle(id);


--
-- TOC entry 3138 (class 2606 OID 27588)
-- Name: t_opus_fk1; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus
    ADD CONSTRAINT t_opus_fk1 FOREIGN KEY (genre) REFERENCES t_genre(id);


--
-- TOC entry 3139 (class 2606 OID 27605)
-- Name: t_opus_fk2; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus
    ADD CONSTRAINT t_opus_fk2 FOREIGN KEY (type) REFERENCES t_type_opus(id);


--
-- TOC entry 3140 (class 2606 OID 27627)
-- Name: t_opus_theme_fk; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus_theme
    ADD CONSTRAINT t_opus_theme_fk FOREIGN KEY (opus) REFERENCES t_opus(id);


--
-- TOC entry 3141 (class 2606 OID 27632)
-- Name: t_opus_theme_fk1; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_opus_theme
    ADD CONSTRAINT t_opus_theme_fk1 FOREIGN KEY (theme) REFERENCES t_theme(id);


--
-- TOC entry 3133 (class 2606 OID 27500)
-- Name: t_person_fk; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_person
    ADD CONSTRAINT t_person_fk FOREIGN KEY (country) REFERENCES t_country(id);


--
-- TOC entry 3134 (class 2606 OID 27517)
-- Name: t_person_fk1; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_person
    ADD CONSTRAINT t_person_fk1 FOREIGN KEY (group_person) REFERENCES t_group_person(id);


--
-- TOC entry 3136 (class 2606 OID 27537)
-- Name: t_phrase_fk; Type: FK CONSTRAINT; Schema: literature; Owner: shmihshmih
--

ALTER TABLE ONLY t_phrase
    ADD CONSTRAINT t_phrase_fk FOREIGN KEY (pid) REFERENCES t_person(id);


--
-- TOC entry 3295 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;