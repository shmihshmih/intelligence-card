/* SQL Manager Lite for MySQL                              5.6.3.48526 */
/* ------------------------------------------------------------------- */
/* Host     : localhost                                                */
/* Port     : 3306                                                     */
/* Database : philosophy                                               */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES 'utf8' */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `philosophy`;

CREATE DATABASE `philosophy`
  CHARACTER SET 'utf8'
  COLLATE 'utf8_general_ci';

USE `philosophy`;

/* Удаление объектов БД */

DROP VIEW IF EXISTS `v_section`;
DROP VIEW IF EXISTS `v_school`;
DROP VIEW IF EXISTS `v_person`;
DROP VIEW IF EXISTS `v_opus`;
DROP VIEW IF EXISTS `v_idiom`;
DROP VIEW IF EXISTS `v_files`;
DROP VIEW IF EXISTS `v_dictionary`;
DROP VIEW IF EXISTS `v_country_group`;
DROP VIEW IF EXISTS `v_country`;
DROP FUNCTION IF EXISTS `f_section8mod`;
DROP FUNCTION IF EXISTS `f_section8upd`;
DROP FUNCTION IF EXISTS `f_section8del`;
DROP FUNCTION IF EXISTS `f_section8add`;
DROP FUNCTION IF EXISTS `f_school8mod`;
DROP FUNCTION IF EXISTS `f_school8upd`;
DROP FUNCTION IF EXISTS `f_school8del`;
DROP FUNCTION IF EXISTS `f_school8add`;
DROP FUNCTION IF EXISTS `f_person8mod`;
DROP FUNCTION IF EXISTS `f_person8upd`;
DROP FUNCTION IF EXISTS `f_person8del`;
DROP FUNCTION IF EXISTS `f_person8add`;
DROP FUNCTION IF EXISTS `f_opus8mod`;
DROP FUNCTION IF EXISTS `f_opus8upd`;
DROP FUNCTION IF EXISTS `f_opus8del`;
DROP FUNCTION IF EXISTS `f_opus8add`;
DROP FUNCTION IF EXISTS `f_idiom8mod`;
DROP FUNCTION IF EXISTS `f_idiom8upd`;
DROP FUNCTION IF EXISTS `f_idiom8del`;
DROP FUNCTION IF EXISTS `f_idiom8add`;
DROP FUNCTION IF EXISTS `f_files8mod`;
DROP FUNCTION IF EXISTS `f_files8upd`;
DROP FUNCTION IF EXISTS `f_files8del`;
DROP FUNCTION IF EXISTS `f_files8add`;
DROP FUNCTION IF EXISTS `f_dictionary8mod`;
DROP FUNCTION IF EXISTS `f_dictionary8upd`;
DROP FUNCTION IF EXISTS `f_dictionary8del`;
DROP FUNCTION IF EXISTS `f_dictionary8add`;
DROP FUNCTION IF EXISTS `f_country_group8mod`;
DROP FUNCTION IF EXISTS `f_country_group8upd`;
DROP FUNCTION IF EXISTS `f_country_group8del`;
DROP FUNCTION IF EXISTS `f_country_group8add`;
DROP FUNCTION IF EXISTS `f_country8mod`;
DROP FUNCTION IF EXISTS `f_country8upd`;
DROP FUNCTION IF EXISTS `f_country8del`;
DROP FUNCTION IF EXISTS `f_country8add`;
DROP FUNCTION IF EXISTS `f_core_get_id`;
DROP TABLE IF EXISTS `t_idiom`;
DROP TABLE IF EXISTS `t_files`;
DROP TABLE IF EXISTS `t_opus`;
DROP TABLE IF EXISTS `t_section`;
DROP TABLE IF EXISTS `t_person`;
DROP TABLE IF EXISTS `t_school`;
DROP TABLE IF EXISTS `t_dictionary`;
DROP TABLE IF EXISTS `t_country`;
DROP TABLE IF EXISTS `t_country_group`;
DROP TABLE IF EXISTS `core_user`;
DROP TABLE IF EXISTS `core_seq`;
DROP TABLE IF EXISTS `core_roles`;
DROP TABLE IF EXISTS `core_log`;

/* Структура для таблицы `core_log`:  */

CREATE TABLE `core_log` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентифитор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Идентификатор записи',
  `message` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Сообщение изменения',
  `table` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Изменяемая таблица',
  `timed` TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время изменения',
  `action_type` VARCHAR(20) COLLATE utf8_general_ci NOT NULL COMMENT 'Тип изменения (i,d,u)',
  `script` VARCHAR(12000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Скрипт для возврата'
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_roles`:  */

CREATE TABLE `core_roles` (
  `id` BIGINT(20) NOT NULL,
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `active` TINYINT(1) NOT NULL
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_seq`:  */

CREATE TABLE `core_seq` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `st` BIGINT(20) NOT NULL,
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
  AUTO_INCREMENT=249 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `core_user`:  */

CREATE TABLE `core_user` (
  `id` BIGINT(20) NOT NULL,
  `login` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `pass` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `active` TINYINT(1) NOT NULL
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_country_group`:  */

CREATE TABLE `t_country_group` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL COMMENT 'Название группы стран',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`)
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_country`:  */

CREATE TABLE `t_country` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Группа стран',
  `country` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Страна',
  `language` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Язык',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  CONSTRAINT `t_country_fk1` FOREIGN KEY (`pid`) REFERENCES `t_country_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_dictionary`:  */

CREATE TABLE `t_dictionary` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Автор термина',
  `parent` BIGINT(20) DEFAULT NULL COMMENT 'Предшествующий термин',
  `caption` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Термин',
  `description` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Описание термина',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Библиографическое описание',
  `foreign_lang` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'На др. языке',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  KEY `parent` USING BTREE (`parent`),
  CONSTRAINT `t_dictionary_fk1` FOREIGN KEY (`parent`) REFERENCES `t_dictionary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_dictionary_fk2` FOREIGN KEY (`pid`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_school`:  */

CREATE TABLE `t_school` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `school` VARCHAR(50) COLLATE utf8_general_ci NOT NULL COMMENT 'Школа',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Краткое описание',
  `parent` BIGINT(20) DEFAULT NULL COMMENT 'Предыдущее учение',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Библиографическое описание',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `parent` USING BTREE (`parent`),
  KEY `annotation` USING BTREE (`annotation`(255)),
  CONSTRAINT `t_school_fk1` FOREIGN KEY (`parent`) REFERENCES `t_school` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_person`:  */

CREATE TABLE `t_person` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `name` VARCHAR(30) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Имя',
  `surname` VARCHAR(30) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Фамилия',
  `last_name` VARCHAR(30) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Отчество',
  `birth_date` DATE NOT NULL COMMENT 'Дата рождения',
  `death_date` DATE NOT NULL COMMENT 'Дата смерти',
  `country` BIGINT(20) DEFAULT NULL COMMENT 'Страна-язык-национальность',
  `parent` BIGINT(20) DEFAULT NULL COMMENT 'Учитель, наставник',
  `school` BIGINT(20) DEFAULT NULL COMMENT 'Школа',
  `key_word` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Ключевые слова',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Краткое описание',
  `link` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Ссылка на биографию',
  `portrait` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT '' COMMENT 'Портрет',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Бибдиографическое описание',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `country` USING BTREE (`country`),
  KEY `parent` USING BTREE (`parent`),
  KEY `section` USING BTREE (`school`),
  CONSTRAINT `t_person_fk1` FOREIGN KEY (`country`) REFERENCES `t_country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_person_fk2` FOREIGN KEY (`school`) REFERENCES `t_school` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_person_fk3` FOREIGN KEY (`parent`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_section`:  */

CREATE TABLE `t_section` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Вышестоящий раздел',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL COMMENT 'Название',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Описание',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  KEY `annotation` USING BTREE (`annotation`(255)),
  CONSTRAINT `t_section_fk1` FOREIGN KEY (`pid`) REFERENCES `t_section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_opus`:  */

CREATE TABLE `t_opus` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) NOT NULL COMMENT 'Автор произведения',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL COMMENT 'Название',
  `annotation` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Аннотация',
  `cover` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Обложка',
  `published` DATE DEFAULT NULL COMMENT 'Опубликовано',
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Библиографическое описание',
  `key_word` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Ключевые слова',
  `section` BIGINT(20) DEFAULT NULL COMMENT 'Раздел науки',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id` USING BTREE (`id`),
  KEY `pid` USING BTREE (`pid`),
  KEY `section` USING BTREE (`section`),
  KEY `annotation` USING BTREE (`annotation`(255)),
  CONSTRAINT `t_opus_fk1` FOREIGN KEY (`pid`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `t_opus_fk2` FOREIGN KEY (`section`) REFERENCES `t_section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_files`:  */

CREATE TABLE `t_files` (
  `id` BIGINT(20) NOT NULL,
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Произведение',
  `caption` VARCHAR(300) COLLATE utf8_general_ci NOT NULL,
  `author` VARCHAR(300) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Автор',
  `link` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL,
  `add_date` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `visible` TINYINT(1) NOT NULL,
  `bibliography` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL,
  `type` VARCHAR(30) COLLATE utf8_general_ci NOT NULL COMMENT 'Тип файла(Гост,словарь и т.д)',
  `key_word` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL,
  `format` VARCHAR(30) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Формат файла',
  KEY `pid` USING BTREE (`pid`),
  CONSTRAINT `t_files_fk1` FOREIGN KEY (`pid`) REFERENCES `t_opus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Структура для таблицы `t_idiom`:  */

CREATE TABLE `t_idiom` (
  `id` BIGINT(20) NOT NULL COMMENT 'Идентификатор',
  `pid` BIGINT(20) DEFAULT NULL COMMENT 'Автор',
  `idiom` VARCHAR(3000) COLLATE utf8_general_ci NOT NULL COMMENT 'Высказывание',
  `original` VARCHAR(3000) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Оригинал',
  KEY `pid` USING BTREE (`pid`),
  CONSTRAINT `t_idiom_fk1` FOREIGN KEY (`pid`) REFERENCES `t_person` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB
  CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
;

/* Определение для функции `f_core_get_id`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_core_get_id`()
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT 'Последовательность идентификаторов'
  BEGIN
    declare
    new_id  bigint;
    insert `core_seq`(`st`) VALUES(1);
    select max(`id`) into `new_id` from `core_seq` where `st` = 1;
    delete from `core_seq` where `st` = 0;
    update `core_seq` set `st` = 0 where `st` = 1;
    RETURN `new_id`;
  END$$

DELIMITER ;

/* Определение для функции `f_country8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8add`(
  `pid` BIGINT(20),
  `country` VARCHAR(50),
  `language` VARCHAR(50)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_country`
      (
        `id`,
        `pid`,
        `country`,
        `language`)
    VALUES(
      @pn_id,
      pid,
      country,
      language);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_country8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_country`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_country8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8upd`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `country` VARCHAR(50),
  `language` VARCHAR(50)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_country` c
    SET
      c.`pid` = pid,
      c.`country` = country,
      c.`language` = language
    WHERE
      c.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_country8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country8mod`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `country` VARCHAR(50),
  `language` VARCHAR(50)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      SELECT `f_country8upd`(id, pid, country, language) INTO @pn_id;
    else
      SELECT `f_country8add`(pid, country, language) INTO @pn_id;
    end if;
    return @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_country_group8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8add`(
  `caption` VARCHAR(300)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_country_group`
      (
        `id`,
        `caption`)
      VALUE (
      @pn_id,
      caption);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_country_group8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_country_group`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_country_group8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8upd`(
  `id` BIGINT(20),
  `caption` VARCHAR(300)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_country_group` g
    SET
      g.`caption` = caption
    WHERE
      g.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_country_group8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_country_group8mod`(
  `id` BIGINT(20),
  `caption` VARCHAR(300)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      select `f_country_group8upd`(id, caption) into @pn_id;
    else
      select `f_country_group8add`(caption) into @pn_id;
    end if;
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_dictionary8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8add`(
  `pid` BIGINT(20),
  `parent` BIGINT(20),
  `caption` VARCHAR(3000),
  `description` VARCHAR(3000),
  `bibliography` VARCHAR(3000),
  `foreign_lang` VARCHAR(300)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `philosophy`.`t_dictionary`
      (
        `id`,
        `pid`,
        `parent`,
        `caption`,
        `description`,
        `bibliography`,
        `foreign_lang`)
    VALUES(
      @pn_id,
      pid,
      parent,
      caption,
      description,
      bibliography,
      foreign_lang);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_dictionary8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_dictionary`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_dictionary8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8upd`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `parent` BIGINT(20),
  `caption` VARCHAR(3000),
  `description` VARCHAR(3000),
  `bibliography` VARCHAR(3000),
  `foreign_lang` VARCHAR(300)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_dictionary` d
    SET
      d.`pid` = pid,
      d.`parent` = parent,
      d.`caption` = caption,
      d.`description` = description,
      d.`bibliography` = bibliography,
      d.`foreign_lang` = foreign_lang
    WHERE
      d.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_dictionary8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_dictionary8mod`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `parent` BIGINT(20),
  `caption` VARCHAR(3000),
  `description` VARCHAR(3000),
  `bibliography` VARCHAR(3000),
  `foreign_lang` VARCHAR(300)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      select `f_dictionary8upd`(id, pid, parent, caption, description, bibliography, foreign_lang) into @pn_id;
    else
      select `f_dictionary8add`(pid, parent, caption, description, bibliography, foreign_lang) into @pn_id;
    end if;
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_files8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8add`(
  `pid` BIGINT,
  `caption` VARCHAR(300),
  `author` VARCHAR(300),
  `link` VARCHAR(3000),
  `visible` BOOLEAN,
  `bibliography` VARCHAR(3000),
  `type` VARCHAR(30),
  `key_word` VARCHAR(3000),
  `format` VARCHAR(30)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_files`
      (
        `id`,
        `pid`,
        `caption`,
        `author`,
        `link`,
        `add_date`,
        `visible`,
        `bibliography`,
        `type`,
        `key_word`,
        `format`)
    VALUES (
      @pn_id,
      pid,
      caption,
      author,
      link,
      CURRENT_TIMESTAMP(),
      visible,
      bibliography,
      type,
      key_word,
      format);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_files8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_files`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_files8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8upd`(
  `id` BIGINT,
  `pid` BIGINT,
  `caption` VARCHAR(300),
  `author` VARCHAR(300),
  `link` VARCHAR(3000),
  `visible` BOOLEAN,
  `bibliography` VARCHAR(3000),
  `type` VARCHAR(30),
  `key_word` VARCHAR(3000),
  `firmat` VARCHAR(30)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_files` f
    SET
      f.`id` = id,
      f.`pid` = pid,
      f.`caption` = caption,
      f.`author` = author,
      f.`link` = link,
      f.`add_date` = CURRENT_TIMESTAMP(),
      f.`visible` = visible,
      f.`bibliography` = bibliography,
      f.`type` = type,
      f.`key_word` = key_word,
      f.`format` = format
    WHERE
      f.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_files8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_files8mod`(
  `id` BIGINT,
  `pid` BIGINT,
  `caption` VARCHAR(300),
  `author` VARCHAR(300),
  `link` VARCHAR(3000),
  `bibliography` VARCHAR(3000),
  `type` VARCHAR(30),
  `key_word` VARCHAR(3000),
  `format` VARCHAR(30)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      SELECT `f_files8upd`(id,
                           pid,
                           caption,
                           author,
                           link,
                           true,
                           bibliography,
                           type,
                           key_word,
                           format) INTO @pn_id;
    else
      SELECT `f_files8add`(pid,
                           caption,
                           author,
                           link,
                           true,
                           bibliography,
                           type,
                           key_word,
                           format) INTO @pn_id;
    end if;
    return @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_idiom8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8add`(
  `pid` BIGINT,
  `idiom` VARCHAR(3000),
  `original` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_idiom`
      (
        `id`,
        `pid`,
        `idiom`,
        `original`)
    VALUES (
      @pn_id,
      pid,
      idiom,
      original);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_idiom8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_idiom`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_idiom8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8upd`(
  `id` BIGINT,
  `pid` BIGINT,
  `idiom` VARCHAR(3000),
  `original` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_idiom` i
    SET
      i.`pid` = pid,
      i.`idiom` = idiom,
      i.`original` = original
    WHERE
      i.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_idiom8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_idiom8mod`(
  `id` BIGINT,
  `pid` BIGINT,
  `idiom` VARCHAR(3000),
  `original` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      SELECT `f_idiom8upd`(id, pid, idiom, original) INTO @pn_id;
    else
      SELECT `f_idiom8add`(pid, idiom, original) INTO @pn_id;
    end if;
    return @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_opus8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8add`(
  `pid` BIGINT(20),
  `caption` VARCHAR(300),
  `annotation` VARCHAR(3000),
  `cover` VARCHAR(300),
  `published` DATE,
  `bibliography` VARCHAR(3000),
  `key_word` VARCHAR(3000),
  `section` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_opus`
      (
        `id`,
        `pid`,
        `caption`,
        `annotation`,
        `cover`,
        `published`,
        `bibliography`,
        `key_word`,
        `section`)
      VALUE (
      @pn_id,
      pid,
      caption,
      annotation,
      cover,
      published,
      bibliography,
      key_word,
      section);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_opus8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_opus`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_opus8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8upd`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `caption` VARCHAR(300),
  `annotation` VARCHAR(3000),
  `cover` VARCHAR(300),
  `published` DATE,
  `bibliography` VARCHAR(3000),
  `key_word` VARCHAR(3000),
  `section` BIGINT(20)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_opus` o
    SET
      o.`pid` = pid,
      o.`caption` = caption,
      o.`annotation` = annotation,
      o.`cover` = cover,
      o.`published` = published,
      o.`bibliography` = bibliography,
      o.`key_word` = key_word,
      o.`section` = section
    WHERE
      o.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_opus8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_opus8mod`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `caption` VARCHAR(300),
  `annotation` VARCHAR(3000),
  `cover` VARCHAR(300),
  `published` DATE,
  `bibliography` VARCHAR(3000),
  `key_word` VARCHAR(3000),
  `section` BIGINT(20)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      select `f_opus8upd`(id, pid, caption, annotation, cover, published, bibliography, key_word, section) into @pn_id;
    else
      select `f_opus8add`(pid, caption, annotation, cover, published, bibliography, key_word, section) into @pn_id;
    end if;
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_person8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8add`(
  `name` VARCHAR(30),
  `surname` VARCHAR(30),
  `last_name` VARCHAR(30),
  `birth_date` DATE,
  `death_date` DATE,
  `country` BIGINT(20),
  `psrent` BIGINT(20),
  `school` BIGINT(20),
  `key_word` VARCHAR(3000),
  `annotation` VARCHAR(3000),
  `link` VARCHAR(3000),
  `portrait` VARCHAR(3000),
  `bibliography` VARCHAR(3000)
)
  RETURNS TINYINT(4)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_person`
      (
        `id`,
        `name`,
        `surname`,
        `last_name`,
        `birth_date`,
        `death_date`,
        `country`,
        `parent`,
        `school`,
        `key_word`,
        `annotation`,
        `link`,
        `portrait`,
        `bibliography`)
      VALUE (
      @pn_id,
      name,
      surname,
      last_name,
      birth_date,
      death_date,
      country,
      parent,
      school,
      key_word,
      annotation,
      link,
      portrait,
      bibliography);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_person8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_person`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_person8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8upd`(
  `id` BIGINT(20),
  `name` VARCHAR(30),
  `surname` VARCHAR(30),
  `last_name` VARCHAR(30),
  `birth_date` DATE,
  `death_date` DATE,
  `country` BIGINT(20),
  `parent` BIGINT(20),
  `school` BIGINT(20),
  `key_word` VARCHAR(3000),
  `annotation` VARCHAR(3000),
  `link` VARCHAR(3000),
  `portrait` VARCHAR(3000),
  `bibliography` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_person`  p
    SET
      p.`name` = name,
      p.`surname` = surname,
      p.`last_name` = last_name,
      p.`birth_date` = birth_date,
      p.`death_date` = death_date,
      p.`country` = country,
      p.`parent` = parent,
      p.`school` = school,
      p.`key_word` = key_word,
      p.`annotation` = annotation,
      p.`link` = link,
      p.`portrait` = portrait,
      p.`bibliography` = bibliography
    WHERE
      p.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_person8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_person8mod`(
  `id` BIGINT(20),
  `name` VARCHAR(30),
  `surname` VARCHAR(30),
  `last_name` VARCHAR(30),
  `birth_date` DATE,
  `death_date` DATE,
  `country` BIGINT(20),
  `parent` BIGINT(20),
  `school` BIGINT(20),
  `key_word` VARCHAR(3000),
  `annotation` VARCHAR(3000),
  `link` VARCHAR(3000),
  `portrait` VARCHAR(3000),
  `bibliography` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      select `f_person8upd`(id, name, surname, last_name, birth_date, death_date, country, parent, school, key_word, annotation, link, portrait, bibliography) into @pn_id;
    else
      select `f_person8add`(name, surname, last_name, birth_date, death_date, country, parent, school, key_word, annotation, link, portrait, bibliography) into @pn_id;
    end if;
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_school8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8add`(
  `school` VARCHAR(50),
  `annotation` VARCHAR(3000),
  `parent` BIGINT,
  `bibliography` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_school`
      (
        `id`,
        `school`,
        `annotation`,
        `parent`,
        `bibliography`)
      VALUE (
      @pn_id,
      school,
      annotation,
      parent,
      bibliography);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_school8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8del`(
  `pn_id` BIGINT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_school`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_school8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8upd`(
  `id` BIGINT(20),
  `school` VARCHAR(30),
  `annotation` VARCHAR(3000),
  `parent` BIGINT(20),
  `bibliography` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_school` s
    SET
      s.`school` = school,
      s.`annotation` = annotation,
      s.`parent` = parent,
      s.`bibliography` = bibliography
    WHERE
      s.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_school8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_school8mod`(
  `id` BIGINT(20),
  `school` VARCHAR(30),
  `annotation` VARCHAR(3000),
  `parent` BIGINT(20),
  `bibliography` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      select `f_school8upd`(id, school, annotation, parent, bibliography) into @pn_id;
    else
      select `f_school8add`(school, annotation, parent, bibliography) into @pn_id;
    end if;
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_section8add`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8add`(
  `pid` BIGINT,
  `caption` VARCHAR(300),
  `annotation` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DECLARE
    pn_id  bigint;
    SELECT `f_core_get_id`() INTO @pn_id;
    INSERT INTO
      `t_section`
      (
        `id`,
        `pid`,
        `caption`,
        `annotation`)
      VALUE (
      @pn_id,
      pid,
      caption,
      annotation);
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_section8del`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8del`(
  `pn_id` FLOAT
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    DELETE FROM
      `t_section`
    WHERE
      `id` = pn_id;
    RETURN pn_id;
  END$$

DELIMITER ;

/* Определение для функции `f_section8upd`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8upd`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `caption` VARCHAR(300),
  `annotation` VARCHAR(3000)
)
  RETURNS BIGINT(20)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    UPDATE
      `t_section` s
    SET
      s.`pid` = pid,
      s.`caption` = caption,
      s.`annotation` = annotation
    WHERE
      s.`id` = id;
    RETURN id;
  END$$

DELIMITER ;

/* Определение для функции `f_section8mod`:  */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' FUNCTION `f_section8mod`(
  `id` BIGINT(20),
  `pid` BIGINT(20),
  `caption` VARCHAR(300),
  `annotation` VARCHAR(3000)
)
  RETURNS TINYINT(4)
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    declare
    pn_id  bigint;
    IF (id is not null) then
      select `f_section8upd`(id, pid, caption, annotation) into @pn_id;
    else
      select `f_section8add`(pid, caption, annotation) into @pn_id;
    end if;
    RETURN @pn_id;
  END$$

DELIMITER ;

/* Определение для представления `v_country`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_country`
AS
  select
    `t`.`id` AS `id`,
    `t`.`pid` AS `pid`,
    `t`.`country` AS `country`,
    `t`.`language` AS `language`,
    `t2`.`country` AS `pid_country`,
    `t2`.`language` AS `pid_language`
  from
    (`t_country` `t`
      left join `t_country` `t2` on ((`t`.`pid` = `t2`.`id`)));

/* Определение для представления `v_country_group`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_country_group`
AS
  select
    `cg`.`id` AS `id`,
    `cg`.`caption` AS `caption`
  from
    `t_country_group` `cg`;

/* Определение для представления `v_dictionary`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_dictionary`
AS
  select
    `d`.`id` AS `id`,
    `d`.`pid` AS `pid`,
    `d`.`parent` AS `parent`,
    `d`.`caption` AS `caption`,
    `d`.`description` AS `description`,
    `d`.`bibliography` AS `bibliography`,
    `d`.`foreign_lang` AS `foreign_lang`,
    `d2`.`caption` AS `parent_caption`,
    `d2`.`description` AS `parent_description`,
    `p`.`name` AS `name`,
    `p`.`surname` AS `surname`
  from
    ((`t_dictionary` `d`
      left join `t_dictionary` `d2` on ((`d`.`parent` = `d2`.`id`)))
      left join `t_person` `p` on ((`d`.`pid` = `p`.`id`)));

/* Определение для представления `v_files`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_files`
AS
  select
    `f`.`id` AS `id`,
    `f`.`pid` AS `pid`,
    `f`.`caption` AS `caption`,
    `f`.`author` AS `author`,
    `f`.`link` AS `link`,
    `f`.`add_date` AS `add_date`,
    `f`.`visible` AS `visible`,
    `f`.`bibliography` AS `bibliography`,
    `f`.`type` AS `type`,
    `f`.`key_word` AS `key_word`,
    `f`.`format` AS `format`
  from
    (`t_files` `f`
      left join `t_opus` `o` on ((`f`.`pid` = `o`.`id`)));

/* Определение для представления `v_idiom`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_idiom`
AS
  select
    `i`.`id` AS `id`,
    `i`.`pid` AS `pid`,
    `i`.`idiom` AS `idiom`,
    `i`.`original` AS `original`,
    `p`.`name` AS `name`
  from
    (`t_idiom` `i`
      left join `t_person` `p` on ((`i`.`pid` = `p`.`id`)));

/* Определение для представления `v_opus`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_opus`
AS
  select
    `t`.`id` AS `id`,
    `t`.`pid` AS `pid`,
    `t`.`caption` AS `caption`,
    `t`.`annotation` AS `annotation`,
    `t`.`cover` AS `cover`,
    `t`.`published` AS `published`,
    `t`.`bibliography` AS `bibliography`,
    `t`.`key_word` AS `key_word`,
    `t`.`section` AS `section`,
    `t2`.`name` AS `name`,
    `t2`.`surname` AS `surname`,
    `t3`.`caption` AS `section_caption`
  from
    ((`t_opus` `t`
      left join `t_person` `t2` on ((`t`.`pid` = `t2`.`id`)))
      left join `t_section` `t3` on ((`t`.`section` = `t3`.`id`)));

/* Определение для представления `v_person`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_person`
AS
  select
    `p`.`id` AS `id`,
    `p`.`name` AS `name`,
    `p`.`surname` AS `surname`,
    `p`.`last_name` AS `last_name`,
    `p`.`birth_date` AS `birth_date`,
    `p`.`death_date` AS `death_date`,
    `p`.`country` AS `country_id`,
    `p`.`parent` AS `papa_person_id`,
    `p`.`school` AS `school_id`,
    `p`.`annotation` AS `person_annotation`,
    `p`.`link` AS `link`,
    `p`.`portrait` AS `portrait`,
    `p`.`bibliography` AS `person_bibliography`,
    `p`.`key_word` AS `person_key_word`,
    `c`.`country` AS `country_caption`,
    `s`.`school` AS `school_caption`
  from
    ((`t_person` `p`
      left join `t_country` `c` on ((`p`.`country` = `c`.`id`)))
      left join `t_school` `s` on ((`p`.`school` = `s`.`id`)));

/* Определение для представления `v_school`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_school`
AS
  select
    `t`.`id` AS `id`,
    `t`.`school` AS `school`,
    `t`.`annotation` AS `annotation`,
    `t`.`parent` AS `parent`,
    `t`.`bibliography` AS `bibliography`,
    `t2`.`school` AS `parent_school`
  from
    (`t_school` `t`
      left join `t_school` `t2` on ((`t`.`parent` = `t2`.`id`)));

/* Определение для представления `v_section`:  */

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `v_section`
AS
  select
    `s`.`id` AS `id`,
    `s`.`pid` AS `pid`,
    `s`.`caption` AS `caption`,
    `s`.`annotation` AS `annotation`,
    `s2`.`caption` AS `pid_caption`,
    `s2`.`annotation` AS `pid_annotation`
  from
    (`t_section` `s`
      left join `t_section` `s2` on ((`s`.`pid` = `s2`.`id`)));

/* Data for the `core_log` table  (LIMIT 0,500) */

INSERT INTO `core_log` (`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`) VALUES
  (68,67,'В таблицу \"Файлы\" добавлена запись Новейший философский словарь (67-без связи-Грицанов-https://drive.google.com/file/d/0B7gd14FVoD9rOVZsMEw5eHZvS00/view?usp=sharing-1-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - 1280 с. - (Мир энциклопедий).-Словарь-Философский словарь, 2003-doc)','files','2017-01-16 19:51:51','i','insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (67,,Новейший философский словарь,Грицанов,https://drive.google.com/file/d/0B7gd14FVoD9rOVZsMEw5eHZvS00/view?usp=sharing,1,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - 1280 с. - (Мир энциклопедий).,Словарь,Философский словарь, 2003,doc)'),
  (124,123,'В таблицу \"Файлы\" добавлена запись Краткая история философии. Нескучная книга. (123-без связи-Гусев Д.А.-https://drive.google.com/file/d/0B7gd14FVoD9raVZsTTN2SjN5Xzg/view?usp=sharing-1-Гусев Д.А. Краткая история философии. Издательство НЦ ЭНАС, 2003 г., 224 с.-Книга-история философии, популярная-fb2)','files','2017-01-17 11:34:55','i','insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (123,,Краткая история философии. Нескучная книга.,Гусев Д.А.,https://drive.google.com/file/d/0B7gd14FVoD9raVZsTTN2SjN5Xzg/view?usp=sharing,1,Гусев Д.А. Краткая история философии. Издательство НЦ ЭНАС, 2003 г., 224 с.,Книга,история философии, популярная,fb2)'),
  (126,125,'В таблицу \"Высказывания\" добавлена запись Navigare necesse est, vivere non est necesse (125-нет автора-Плыть необходимо, а жить — нет!)','idiom','2017-01-24 19:58:24','i','insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (125,,Navigare necesse est, vivere non est necesse,Плыть необходимо, а жить — нет!)'),
  (127,125,'В таблице \"Высказывания\" изменена запись Navigare necesse est, vivere non est necesse (125-нет автора-Плыть необходимо, а жить — нет!) на Navigare necesse est, vivere non est necesse (125-нет автора-Плыть необходимо, а жить — нет!(Помпей))','idiom','2017-01-24 20:00:46','u','insert into t_idiom(id, \r\n    pid, \r\n    idiom, \r\n    original) values (125,,Navigare necesse est, vivere non est necesse,Плыть необходимо, а жить — нет!)'),
  (129,128,'В таблицу \"Высказывания\" добавлена запись Carpe diem (128-нет автора-Лови день(Гораций)в «Оде к Левконое»)','idiom','2017-01-24 20:01:33','i','insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (128,,Carpe diem,Лови день(Гораций)в «Оде к Левконое»)'),
  (130,125,'В таблице \"Высказывания\" изменена запись Navigare necesse est, vivere non est necesse (125-нет автора-Плыть необходимо, а жить — нет!(Помпей)) на Navigare necesse est, vivere non est necesse (125-нет автора-Плыть необходимо, а жить — нет!(Помпей) у плутарха в Жизнеописаниях)','idiom','2017-01-24 20:02:37','u','insert into t_idiom(id, \r\n    pid, \r\n    idiom, \r\n    original) values (125,,Navigare necesse est, vivere non est necesse,Плыть необходимо, а жить — нет!(Помпей))'),
  (132,131,'В таблицу \"Файлы\" добавлена запись Социальная философия: Учебное пособие (131-без связи-Петр Васильевич Алексеев-https://drive.google.com/file/d/0B7gd14FVoD9rcGFWUjdfVTN4LVE/view?usp=sharing-1--Учебное пособие-Социальная философия, Алексеев-mobi)','files','2017-01-24 20:14:06','i','insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (131,,Социальная философия: Учебное пособие,Петр Васильевич Алексеев,https://drive.google.com/file/d/0B7gd14FVoD9rcGFWUjdfVTN4LVE/view?usp=sharing,1,,Учебное пособие,Социальная философия, Алексеев,mobi)'),
  (134,133,'В таблицу \"Разделы науки\" добавлена запись Философия (133-нет надраздела-Философия-особая форма познания мира, вырабатывающая систему знаний о наиболее общих характеристиках, предельно-обобщающих понятиях и фундаментальных принципах реальности (бытия) и познания, бытия человека, об отношении человека и мира.)','section','2017-01-24 20:28:02','i','insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (133,,Философия,особая форма познания мира, вырабатывающая систему знаний о наиболее общих характеристиках, предельно-обобщающих понятиях и фундаментальных принципах реальности (бытия) и познания, бытия человека, об отношении человека и мира.)'),
  (136,135,'В таблицу \"Разделы науки\" добавлена запись История философии (135-133-История философии-раздел философии, изучающий исторические типы философии.)','section','2017-01-24 20:28:49','i','insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (135,133,История философии,раздел философии, изучающий исторические типы философии.)'),
  (138,137,'В таблицу \"Разделы науки\" добавлена запись Социальная философия (137-133-Социальная философия-раздел философии, призванный ответить на вопрос о том, что есть общество и какое место занимает в нём человек.)','section','2017-01-24 20:30:04','i','insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (137,133,Социальная философия,раздел философии, призванный ответить на вопрос о том, что есть общество и какое место занимает в нём человек.)'),
  (140,139,'В таблицу \"Высказывания\" добавлена запись Quis custodiet ipsos custodes? (139-нет автора-Кто устережёт самих сторожей?(Ювенал Сатиры))','idiom','2017-01-26 15:57:08','i','insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (139,,Quis custodiet ipsos custodes?,Кто устережёт самих сторожей?(Ювенал Сатиры))'),
  (142,141,'В таблицу \"Группы стран\" добавлена запись Древняя Греция (141)','country_group','2017-01-30 20:12:44','i','insert into t_country_group(id,\r\n     caption) values (141,Древняя Греция)'),
  (144,143,'В таблицу \"Страны\" добавлена запись Самос (143-141-Древнегреческий)','country','2017-01-30 20:15:53','i','insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (143,141,Самос,Древнегреческий)'),
  (146,145,'В таблицу \"Школы\" добавлена запись Пифагореизм (145-Так называемые пифагорейцы, взявшись за математические науки, первые подвинули их вперёд; вскормленные на этих науках, они признали математические начала за начала всего существующего. Из таких начал, естественно, первыми являются числа. В числах усматривали они множество аналогий или подобий с вещами… так что одно свойство чисел являлось им как справедливость, другое — как душа или разум, ещё другое — как благоприятный случай и т. д. Далее они находили в числах свойства и отношения музыкальной гармонии, и так как все прочие вещи по своей природе являлись им подобием чисел, числа же — первыми из всей природы, то они и признали, что элементы числа суть элементы всего сущего, и что все небо есть гармония и число (Аристотель, Met., I, 5).-нет предшественника-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).)','school','2017-01-30 20:27:25','i','insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (145,Пифагореизм,Так называемые пифагорейцы, взявшись за математические науки, первые подвинули их вперёд; вскормленные на этих науках, они признали математические начала за начала всего существующего. Из таких начал, естественно, первыми являются числа. В числах усматривали они множество аналогий или подобий с вещами… так что одно свойство чисел являлось им как справедливость, другое — как душа или разум, ещё другое — как благоприятный случай и т. д. Далее они находили в числах свойства и отношения музыкальной гармонии, и так как все прочие вещи по своей природе являлись им подобием чисел, числа же — первыми из всей природы, то они и признали, что элементы числа суть элементы всего сущего, и что все небо есть гармония и число (Аристотель, Met., I, 5).,,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).)'),
  (148,147,'В таблицу \"Персоны\" добавлена запись Пифагор (147---0001-01-01-0001-01-01-143-нет предшественника-145-Пифагор, число, пифагореизм, -Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.-https://ru.wikipedia.org/wiki/Пифагор-https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).-)','person','2017-01-30 20:33:42','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (147,Пифагор,,,0001-01-01,0001-01-01,143,,145,Пифагор, число, пифагореизм, ,Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.,https://ru.wikipedia.org/wiki/Пифагор,https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).)'),
  (150,149,'В таблицу \"Высказывания\" добавлена запись Я не мудрец, но только философ. (149-147-)','idiom','2017-01-30 20:34:26','i','insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (149,147,Я не мудрец, но только философ.,)'),
  (152,151,'В таблицу \"Словарь\" добавлена запись Мифология (151-нет автора-нет предыдущего понятия-тип функционирования культурных программ, предполагающий их некритическое восприятие индивидуальным и массовым сознанием, сакрализацию их содержания и неукоснительность исполнения.-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 634- (Мир энциклопедий).-греч. myphos - сказание и logos - рассказ)','dictionary','2017-01-30 20:41:13','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (151,,,тип функционирования культурных программ, предполагающий их некритическое восприятие индивидуальным и массовым сознанием, сакрализацию их содержания и неукоснительность исполнения.,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 634- (Мир энциклопедий).,греч. myphos - сказание и logos - рассказ)'),
  (154,153,'В таблицу \"Словарь\" добавлена запись Гелиос (153-нет автора-нет предыдущего понятия-в древнегреческой мифологии[3] солнечное божество, сын титана Гипериона (откуда пошло его прозвище «Гиперионид») и Тейи[4] (либо сын Гипериона и Ирифессы[5]), брат Селены и Эос[6].--)','dictionary','2017-01-30 20:42:31','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (153,,,в древнегреческой мифологии[3] солнечное божество, сын титана Гипериона (откуда пошло его прозвище «Гиперионид») и Тейи[4] (либо сын Гипериона и Ирифессы[5]), брат Селены и Эос[6].,,)'),
  (156,155,'В таблицу \"Словарь\" добавлена запись Деме́тра  (155-нет автора-нет предыдущего понятия- в древнегреческой мифологии богиня плодородия, покровительница земледелия[1].--)','dictionary','2017-01-30 20:43:58','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (155,,, в древнегреческой мифологии богиня плодородия, покровительница земледелия[1].,,)'),
  (158,157,'В таблицу \"Словарь\" добавлена запись Персефона (157-нет автора-нет предыдущего понятия-в древнегреческой мифологии богиня плодородия и царства мёртвых. Дочь Деметры и Зевса, супруга Аида. У римлян — Прозерпина.--)','dictionary','2017-01-30 20:44:17','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (157,,,в древнегреческой мифологии богиня плодородия и царства мёртвых. Дочь Деметры и Зевса, супруга Аида. У римлян — Прозерпина.,,)'),
  (160,159,'В таблицу \"Словарь\" добавлена запись Аи́д  (159-нет автора-нет предыдущего понятия-в древнегреческой мифологии бог подземного царства мёртвых и название самого царства мёртвых. Старший сын Кроноса и Реи[4], брат Зевса, Посейдона, Геры, Деметры и Гестии. Супруг Персефоны, вместе с ним почитаемой и призываемой.--(др.-греч. Ἀΐδης или ᾍδης, или Га́дес; у римлян Плуто́н, др.-греч. Πλούτων, лат. Pluto — «богатый»; также Дит лат. Dis или Орк лат. Orcus[⇨]))','dictionary','2017-01-30 20:44:58','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (159,,,в древнегреческой мифологии бог подземного царства мёртвых и название самого царства мёртвых. Старший сын Кроноса и Реи[4], брат Зевса, Посейдона, Геры, Деметры и Гестии. Супруг Персефоны, вместе с ним почитаемой и призываемой.,,(др.-греч. Ἀΐδης или ᾍδης, или Га́дес; у римлян Плуто́н, др.-греч. Πλούτων, лат. Pluto — «богатый»; также Дит лат. Dis или Орк лат. Orcus[⇨]))'),
  (162,161,'В таблицу \"Словарь\" добавлена запись Амон-Ра (161-нет автора-нет предыдущего понятия-древнеегипетский бог солнца, верховное божество в религии древних египтян. Его имя означает «Солнце» (коптское PH). Центром культа был Гелиополь, где Ра был отождествлён с более древним местным солнечным божеством, Атумом, и где ему были посвящены, как его воплощения, птица Феникс, бык Мневис и обелиск Бен-Бен. В других религиозных центрах Ра в русле религиозного синкретизма также сопоставлялся с местными божествами света: Амоном (в Фивах), под именем Амона-Ра, Хнумом (в Элефантине) — в форме Хнума-Ра, Гором — в форме Ра-Горахти (Ра-Хорахте). Последнее сопоставление было особенно распространено. Ра возглавлял гелиопольскую эннеаду божеств.--)','dictionary','2017-01-30 20:46:40','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (161,,,древнеегипетский бог солнца, верховное божество в религии древних египтян. Его имя означает «Солнце» (коптское PH). Центром культа был Гелиополь, где Ра был отождествлён с более древним местным солнечным божеством, Атумом, и где ему были посвящены, как его воплощения, птица Феникс, бык Мневис и обелиск Бен-Бен. В других религиозных центрах Ра в русле религиозного синкретизма также сопоставлялся с местными божествами света: Амоном (в Фивах), под именем Амона-Ра, Хнумом (в Элефантине) — в форме Хнума-Ра, Гором — в форме Ра-Горахти (Ра-Хорахте). Последнее сопоставление было особенно распространено. Ра возглавлял гелиопольскую эннеаду божеств.,,)'),
  (164,163,'В таблицу \"Словарь\" добавлена запись Антропоморфизм (163-нет автора-нет предыдущего понятия-перенесение человеческого образа и его свойств на неодушевлённые предметы, на одушевлённых существ, на явления и силы природы, на сверхъестественных существ, на абстрактные понятия и др--фр. anthropomorphisme)','dictionary','2017-01-30 20:50:11','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (163,,,перенесение человеческого образа и его свойств на неодушевлённые предметы, на одушевлённых существ, на явления и силы природы, на сверхъестественных существ, на абстрактные понятия и др,,фр. anthropomorphisme)'),
  (166,165,'В таблицу \"Словарь\" добавлена запись Монотеизм (165-нет автора-нет предыдущего понятия- религиозное представление о существовании только одного Бога или о единственности Бога.--)','dictionary','2017-01-30 20:53:19','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (165,,, религиозное представление о существовании только одного Бога или о единственности Бога.,,)'),
  (168,167,'В таблицу \"Словарь\" добавлена запись Буддизм (167-нет автора-нет предыдущего понятия-религиозно-философское учение (дхарма) о духовном пробуждении (бодхи), возникшее около VI века до н. э. в Древней Индии.-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий).-санскр. बुद्ध धर्म, buddha dharma)','dictionary','2017-01-30 21:09:47','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (167,,,религиозно-философское учение (дхарма) о духовном пробуждении (бодхи), возникшее около VI века до н. э. в Древней Индии.,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий).,санскр. बुद्ध धर्म, buddha dharma)'),
  (170,169,'В таблицу \"Страны\" добавлена запись Непал (169-нет группы-непали)','country','2017-01-30 21:12:36','i','insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (169,,Непал,непали)'),
  (172,171,'В таблицу \"Персоны\" добавлена запись Сиддхартха  (171-Гаутама--0001-01-31-0001-01-01-нет страны-нет предшественника-нет школы-Буддизм, будда, Шакьямуни, дхарма, бодхи-дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.-https://ru.wikipedia.org/wiki/Будда_Шакьямуни-https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru--)','person','2017-01-30 21:14:49','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (171,Сиддхартха ,Гаутама,,0001-01-31,0001-01-01,,,,Буддизм, будда, Шакьямуни, дхарма, бодхи,дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.,https://ru.wikipedia.org/wiki/Будда_Шакьямуни,https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru,)'),
  (173,171,'В таблице \"Персоны\" изменена запись Сиддхартха  (171-Гаутама--0001-01-31-0001-01-01-нет страны-нет предшественника-нет школы-Буддизм, будда, Шакьямуни, дхарма, бодхи-дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.-https://ru.wikipedia.org/wiki/Будда_Шакьямуни-https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru- на Сиддхартха  (171-Гаутама--0001-01-31-0001-01-01-169-нет предшественника-нет школы-Буддизм, будда, Шакьямуни, дхарма, бодхи-дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.-https://ru.wikipedia.org/wiki/Будда_Шакьямуни-https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru-)','person','2017-01-30 21:14:56','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (171,Сиддхартха ,Гаутама,,0001-01-31,0001-01-01,,,,Буддизм, будда, Шакьямуни, дхарма, бодхи,дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.,https://ru.wikipedia.org/wiki/Будда_Шакьямуни,https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru,)'),
  (175,174,'В таблицу \"Страны\" добавлена запись Китай (174-нет группы-Китайский)','country','2017-01-30 21:18:17','i','insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (174,,Китай,Китайский)'),
  (177,176,'В таблицу \"Персоны\" добавлена запись Конфуций (176---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Конфуцианство, Кун Цю-этико-философское учение, разработанное Конфуцием (551—479 до н. э.) и развитое его последователями, вошедшее в религиозный комплекс Китая, Кореи, Японии и некоторых других стран-https://ru.wikipedia.org/wiki/Конфуций-https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg--)','person','2017-01-30 21:20:55','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (176,Конфуций,,,0001-01-01,0001-01-01,174,,,Конфуцианство, Кун Цю,этико-философское учение, разработанное Конфуцием (551—479 до н. э.) и развитое его последователями, вошедшее в религиозный комплекс Китая, Кореи, Японии и некоторых других стран,https://ru.wikipedia.org/wiki/Конфуций,https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg,)'),
  (179,178,'В таблицу \"Словарь\" добавлена запись Конфуцианство (178-нет автора-нет предыдущего понятия-этико-философское учение, разработанное Конфуцием (551—479 до н. э.) и развитое его последователями, вошедшее в религиозный комплекс Китая, Кореи, Японии и некоторых других стран--кит. трад. 儒學)','dictionary','2017-01-30 21:22:27','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (178,,,этико-философское учение, разработанное Конфуцием (551—479 до н. э.) и развитое его последователями, вошедшее в религиозный комплекс Китая, Кореи, Японии и некоторых других стран,,кит. трад. 儒學)'),
  (180,176,'В таблице \"Персоны\" изменена запись Конфуций (176---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Конфуцианство, Кун Цю-этико-философское учение, разработанное Конфуцием (551—479 до н. э.) и развитое его последователями, вошедшее в религиозный комплекс Китая, Кореи, Японии и некоторых других стран-https://ru.wikipedia.org/wiki/Конфуций-https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg- на Конфуций (176---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Конфуцианство, Кун Цю-основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.-https://ru.wikipedia.org/wiki/Конфуций-https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg-)','person','2017-01-30 21:22:51','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (176,Конфуций,,,0001-01-01,0001-01-01,174,,,Конфуцианство, Кун Цю,этико-философское учение, разработанное Конфуцием (551—479 до н. э.) и развитое его последователями, вошедшее в религиозный комплекс Китая, Кореи, Японии и некоторых других стран,https://ru.wikipedia.org/wiki/Конфуций,https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg,)'),
  (182,181,'В таблицу \"Группы стран\" добавлена запись Древний Восток (181)','country_group','2017-01-30 21:25:27','i','insert into t_country_group(id,\r\n     caption) values (181,Древний Восток)'),
  (184,183,'В таблицу \"Произведения\" добавлена запись Лунь Юй (183-176-Лунь Юй-один из наиболее знаменитых текстов Восточной Азии. Главная книга конфуцианства, составленная учениками Конфуция из кратких заметок, фиксирующих высказывания, поступки учителя, а также диалоги с его участием.--0000-00-00-Лунь юй\r\nАвтор:\tКонфуций\r\nПеревод:\tПереломов Л.-«Беседы и суждения», «Аналекты Конфуция»-нет раздела)','opus','2017-01-30 21:28:33','i','insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (183,176,Лунь Юй,один из наиболее знаменитых текстов Восточной Азии. Главная книга конфуцианства, составленная учениками Конфуция из кратких заметок, фиксирующих высказывания, поступки учителя, а также диалоги с его участием.,,0000-00-00,Лунь юй\r\nАвтор:\tКонфуций\r\nПеревод:\tПереломов Л.,«Беседы и суждения», «Аналекты Конфуция»,)'),
  (186,185,'В таблицу \"Словарь\" добавлена запись Пантеизм (185-нет автора-нет предыдущего понятия-Несмотря на существующие различные течения внутри пантеизма, центральные идеи в большинстве форм пантеизма постоянны: Вселенная как всеобъемлющее единство и святость природы.-- от древнегреческих слов παν (пан) — «всё, всякий» и θεός (теос) — «Бог, божество»)','dictionary','2017-01-30 21:31:28','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (185,,,Несмотря на существующие различные течения внутри пантеизма, центральные идеи в большинстве форм пантеизма постоянны: Вселенная как всеобъемлющее единство и святость природы.,, от древнегреческих слов παν (пан) — «всё, всякий» и θεός (теос) — «Бог, божество»)'),
  (188,187,'В таблицу \"Персоны\" добавлена запись Лао-цзы (187---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Дао, Трое Чистых, - древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»-https://ru.wikipedia.org/wiki/Лао-цзы-https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg--)','person','2017-01-30 21:36:54','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (187,Лао-цзы,,,0001-01-01,0001-01-01,174,,,Дао, Трое Чистых, , древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»,https://ru.wikipedia.org/wiki/Лао-цзы,https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg,)'),
  (189,176,'В таблице \"Персоны\" изменена запись Конфуций (176---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Конфуцианство, Кун Цю-основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.-https://ru.wikipedia.org/wiki/Конфуций-https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg- на Конфуций (176---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Конфуцианство, Кун Цю-основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.-https://ru.wikipedia.org/wiki/Конфуций-https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 502- (Мир энциклопедий).)','person','2017-01-30 21:37:27','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (176,Конфуций,,,0001-01-01,0001-01-01,174,,,Конфуцианство, Кун Цю,основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.,https://ru.wikipedia.org/wiki/Конфуций,https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg,)'),
  (190,171,'В таблице \"Персоны\" изменена запись Сиддхартха  (171-Гаутама--0001-01-31-0001-01-01-169-нет предшественника-нет школы-Буддизм, будда, Шакьямуни, дхарма, бодхи-дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.-https://ru.wikipedia.org/wiki/Будда_Шакьямуни-https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru- на Сиддхартха  (171-Гаутама--0001-01-31-0001-01-01-169-нет предшественника-нет школы-Буддизм, будда, Шакьямуни, дхарма, бодхи-дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.-https://ru.wikipedia.org/wiki/Будда_Шакьямуни-https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий).)','person','2017-01-30 21:37:58','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (171,Сиддхартха ,Гаутама,,0001-01-31,0001-01-01,169,,,Буддизм, будда, Шакьямуни, дхарма, бодхи,дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.,https://ru.wikipedia.org/wiki/Будда_Шакьямуни,https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru,)'),
  (191,187,'В таблице \"Персоны\" изменена запись Лао-цзы (187---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Дао, Трое Чистых, - древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»-https://ru.wikipedia.org/wiki/Лао-цзы-https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg- на Лао-цзы (187---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Дао, Трое Чистых, - древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»-https://ru.wikipedia.org/wiki/Лао-цзы-https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 542- (Мир энциклопедий).)','person','2017-01-30 21:38:36','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (187,Лао-цзы,,,0001-01-01,0001-01-01,174,,,Дао, Трое Чистых, , древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»,https://ru.wikipedia.org/wiki/Лао-цзы,https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg,)'),
  (193,192,'В таблицу \"Произведения\" добавлена запись Дао дэ цзин (192-187-Дао дэ цзин-Основная идея этого произведения — понятие дао — трактуется как естественный порядок вещей, не допускающий постороннего вмешательства, «небесная воля» или «чистое небытие». Споры о содержании книги и её авторе продолжаются до сих пор.--0001-01-01--Дао, Даосизм-133)','opus','2017-01-30 21:40:08','i','insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (192,187,Дао дэ цзин,Основная идея этого произведения — понятие дао — трактуется как естественный порядок вещей, не допускающий постороннего вмешательства, «небесная воля» или «чистое небытие». Споры о содержании книги и её авторе продолжаются до сих пор.,,0001-01-01,,Дао, Даосизм,133)'),
  (195,194,'В таблицу \"Словарь\" добавлена запись Дао (194-187-нет предыдущего понятия-Конфуций и ранние конфуцианцы придали ему этическое значение, истолковав как «путь человека», то есть нравственное поведение и основанный на морали социальный порядок. Наиболее известная и значимая даосская интерпретация Дао содержится в трактате Дао Дэ Цзин.-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 288- (Мир энциклопедий).-кит. 道, буквально — путь)','dictionary','2017-01-30 21:41:42','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (194,187,,Конфуций и ранние конфуцианцы придали ему этическое значение, истолковав как «путь человека», то есть нравственное поведение и основанный на морали социальный порядок. Наиболее известная и значимая даосская интерпретация Дао содержится в трактате Дао Дэ Цзин.,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 288- (Мир энциклопедий).,кит. 道, буквально — путь)'),
  (197,196,'В таблицу \"Словарь\" добавлена запись Даосизм (196-187-194- учение о дао или «пути вещей», китайское традиционное учение, включающее элементы религии и философии--кит. упр. 道教, пиньинь: dàojiào)','dictionary','2017-01-30 21:43:03','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (196,187,194, учение о дао или «пути вещей», китайское традиционное учение, включающее элементы религии и философии,,кит. упр. 道教, пиньинь: dàojiào)'),
  (199,198,'В таблицу \"Школы\" добавлена запись Милетская школа (198-Исходя из принципа сохранения: «ничто не возникает из ничего», милетцы полагали единое вечное, бесконечное, «божественное» первоначало видимого многообразия вещей, источник жизни и существования космоса. Таким образом, в основе многообразия явлений они усмотрели некое единое правещество; для Фалеса это — вода, для Анаксимандра — апейрон (неопределённое и беспредельное первовещество), для Анаксимена — воздух. («Воду» Фалеса и «воздух» Анаксимена следует, конечно, понимать условно-аллегорически, как символ комплекса абстрактных свойств такого первовещества.)-нет предшественника-)','school','2017-01-30 21:46:03','i','insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (198,Милетская школа,Исходя из принципа сохранения: «ничто не возникает из ничего», милетцы полагали единое вечное, бесконечное, «божественное» первоначало видимого многообразия вещей, источник жизни и существования космоса. Таким образом, в основе многообразия явлений они усмотрели некое единое правещество; для Фалеса это — вода, для Анаксимандра — апейрон (неопределённое и беспредельное первовещество), для Анаксимена — воздух. («Воду» Фалеса и «воздух» Анаксимена следует, конечно, понимать условно-аллегорически, как символ комплекса абстрактных свойств такого первовещества.),,)'),
  (201,200,'В таблицу \"Страны\" добавлена запись Милет (200-нет группы-Древнегреческий)','country','2017-01-30 21:47:58','i','insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (200,,Милет,Древнегреческий)'),
  (203,202,'В таблицу \"Персоны\" добавлена запись Фалес  (202-Милетский--0001-01-01-0001-01-01-200-нет предшественника-198-Семь мудрецов, вода-древнегреческий философ и математик из Милета (Малая Азия). Представитель ионической натурфилософии и основатель милетской (ионийской) школы, с которой начинается история европейской науки. Традиционно считается основоположником греческой философии (и науки) — он неизменно открывал список «семи мудрецов», заложивших основы греческой культуры и государственности.-https://ru.wikipedia.org/wiki/Фалес_Милетский-https://upload.wikimedia.org/wikipedia/commons/c/c6/Illustrerad_Verldshistoria_band_I_Ill_107.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 1075- (Мир энциклопедий).-)','person','2017-01-30 21:50:42','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (202,Фалес ,Милетский,,0001-01-01,0001-01-01,200,,198,Семь мудрецов, вода,древнегреческий философ и математик из Милета (Малая Азия). Представитель ионической натурфилософии и основатель милетской (ионийской) школы, с которой начинается история европейской науки. Традиционно считается основоположником греческой философии (и науки) — он неизменно открывал список «семи мудрецов», заложивших основы греческой культуры и государственности.,https://ru.wikipedia.org/wiki/Фалес_Милетский,https://upload.wikimedia.org/wikipedia/commons/c/c6/Illustrerad_Verldshistoria_band_I_Ill_107.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 1075- (Мир энциклопедий).)'),
  (205,204,'В таблицу \"Персоны\" добавлена запись Анаксима́ндр  (204-Миле́тский--0001-01-01-0001-01-01-200-нет предшественника-198-Огонь-Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.-https://ru.wikipedia.org/wiki/Анаксимандр-https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg--)','person','2017-01-30 21:54:26','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (204,Анаксима́ндр ,Миле́тский,,0001-01-01,0001-01-01,200,,198,Огонь,Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.,https://ru.wikipedia.org/wiki/Анаксимандр,https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg,)'),
  (207,206,'В таблицу \"Персоны\" добавлена запись Анаксимен (206---0001-01-01-0001-01-01-200-нет предшественника-198-Воздух-Анаксимен — последний представитель милетской школы. Анаксимен укрепил и завершил тенденцию стихийного материализма — поиск естественных причин явлений и вещей. Как ранее Фалес и Анаксимандр, первоосновой мира он полагает определенный вид материи. Такой материей он считает неограниченный, бесконечный, имеющий неопределённую форму воздух, из которого возникает всё остальное. «Анаксимен… провозглашает воздух началом сущего, ибо из него всё возникает и к нему всё возвращается».-https://ru.wikipedia.org/wiki/Анаксимен-https://upload.wikimedia.org/wikipedia/commons/2/2d/Anaximenes.jpg--)','person','2017-01-30 21:57:26','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (206,Анаксимен,,,0001-01-01,0001-01-01,200,,198,Воздух,Анаксимен — последний представитель милетской школы. Анаксимен укрепил и завершил тенденцию стихийного материализма — поиск естественных причин явлений и вещей. Как ранее Фалес и Анаксимандр, первоосновой мира он полагает определенный вид материи. Такой материей он считает неограниченный, бесконечный, имеющий неопределённую форму воздух, из которого возникает всё остальное. «Анаксимен… провозглашает воздух началом сущего, ибо из него всё возникает и к нему всё возвращается».,https://ru.wikipedia.org/wiki/Анаксимен,https://upload.wikimedia.org/wikipedia/commons/2/2d/Anaximenes.jpg,)'),
  (208,204,'В таблице \"Персоны\" изменена запись Анаксима́ндр  (204-Миле́тский--0001-01-01-0001-01-01-200-нет предшественника-198-Огонь-Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.-https://ru.wikipedia.org/wiki/Анаксимандр-https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg- на Анаксима́ндр  (204-Миле́тский--0001-01-01-0001-01-01-200-нет предшественника-198-Апейрон-Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.-https://ru.wikipedia.org/wiki/Анаксимандр-https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg-)','person','2017-01-30 21:57:37','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (204,Анаксима́ндр ,Миле́тский,,0001-01-01,0001-01-01,200,,198,Огонь,Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.,https://ru.wikipedia.org/wiki/Анаксимандр,https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg,)'),
  (210,209,'В таблицу \"Словарь\" добавлена запись Апейрон (209-204-нет предыдущего понятия-понятие древнегреческой философии, введённое Анаксимандром, означающее неопределённое, беспредельное и бесконечное первовещество. Апейрон у Анаксимандра является основой мира и существует в вечном движении. Апейрон — это бескачественная материя; всё возникло путём выделения из апейрона противоположностей (например, горячее и холодное).-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 52- (Мир энциклопедий).-греч. ἄπειρον, «бесконечное, беспредельное»)','dictionary','2017-01-30 21:58:45','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (209,204,,понятие древнегреческой философии, введённое Анаксимандром, означающее неопределённое, беспредельное и бесконечное первовещество. Апейрон у Анаксимандра является основой мира и существует в вечном движении. Апейрон — это бескачественная материя; всё возникло путём выделения из апейрона противоположностей (например, горячее и холодное).,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 52- (Мир энциклопедий).,греч. ἄπειρον, «бесконечное, беспредельное»)'),
  (212,211,'В таблицу \"Словарь\" добавлена запись Архэ (211-нет автора-нет предыдущего понятия-в досократовской древнегреческой философии — первооснова, первовещество, первоэлемент, из которого состоит мир. Для характеристики учений первых философов этот термин использовал Аристотель.--(греч. ἀρχή — начало, принцип) )','dictionary','2017-01-30 22:00:44','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (211,,,в досократовской древнегреческой философии — первооснова, первовещество, первоэлемент, из которого состоит мир. Для характеристики учений первых философов этот термин использовал Аристотель.,,(греч. ἀρχή — начало, принцип) )'),
  (214,213,'В таблицу \"Школы\" добавлена запись Элейская школа (213-В отличие от большинства досократиков, элейцы не занимались вопросами естествознания, но разрабатывали теоретическое учение о бытии (предложив впервые сам этот термин), заложив фундамент классической греческой онтологии.-нет предшественника-)','school','2017-01-30 22:02:51','i','insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (213,Элейская школа,В отличие от большинства досократиков, элейцы не занимались вопросами естествознания, но разрабатывали теоретическое учение о бытии (предложив впервые сам этот термин), заложив фундамент классической греческой онтологии.,,)'),
  (216,215,'В таблицу \"Страны\" добавлена запись Великая Греция (215-141-Древнегреческий)','country','2017-01-30 22:16:14','i','insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (215,141,Великая Греция,Древнегреческий)'),
  (218,217,'В таблицу \"Персоны\" добавлена запись Ксенофан (217---0001-01-01-0001-01-01-215-нет предшественника-213-Элея, рапсод, элеаты-По Ксенофану, мифология — это продукт исключительно человеческого воображения. Образы богов созданы людьми как их подобие, поэтому боги не только не превосходят людей нравственно, но и не могут быть предметом поклонения:-https://ru.wikipedia.org/wiki/Ксенофан-https://upload.wikimedia.org/wikipedia/commons/5/5f/Xenophanes_in_Thomas_Stanley_History_of_Philosophy.jpg--)','person','2017-01-30 22:18:22','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (217,Ксенофан,,,0001-01-01,0001-01-01,215,,213,Элея, рапсод, элеаты,По Ксенофану, мифология — это продукт исключительно человеческого воображения. Образы богов созданы людьми как их подобие, поэтому боги не только не превосходят людей нравственно, но и не могут быть предметом поклонения:,https://ru.wikipedia.org/wiki/Ксенофан,https://upload.wikimedia.org/wikipedia/commons/5/5f/Xenophanes_in_Thomas_Stanley_History_of_Philosophy.jpg,)'),
  (220,219,'В таблицу \"Персоны\" добавлена запись Пармени́д  (219---0001-01-01-0001-01-01-215-нет предшественника-213-метафизика, бытие, онтология, гносеология-Диоген Лаэртский так передаёт его философию: «Критерием истины называл он разум; в чувствах же, — говорил он, — точности нет». По замечанию БСЭ (2-е изд.), — отвергая ощущения и опыт как источник знания, Парменид противостоял ионийскому естествознанию, возражал против требования Гераклита «прислушиваться к природе»[8]. «Парменид — мыслитель действительно необыкновенной глубины», — говорит Сократ в диалоге Платона «Теэтет». Он был современником Гераклита[8], с которым спорил. Проф. Дж. Бернет назвал Парменида «отцом материализма[17]».-https://ru.wikipedia.org/wiki/Парменид-https://upload.wikimedia.org/wikipedia/commons/e/ed/Parmenides.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 732- (Мир энциклопедий).-)','person','2017-01-30 22:24:29','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (219,Пармени́д ,,,0001-01-01,0001-01-01,215,,213,метафизика, бытие, онтология, гносеология,Диоген Лаэртский так передаёт его философию: «Критерием истины называл он разум; в чувствах же, — говорил он, — точности нет». По замечанию БСЭ (2-е изд.), — отвергая ощущения и опыт как источник знания, Парменид противостоял ионийскому естествознанию, возражал против требования Гераклита «прислушиваться к природе»[8]. «Парменид — мыслитель действительно необыкновенной глубины», — говорит Сократ в диалоге Платона «Теэтет». Он был современником Гераклита[8], с которым спорил. Проф. Дж. Бернет назвал Парменида «отцом материализма[17]».,https://ru.wikipedia.org/wiki/Парменид,https://upload.wikimedia.org/wikipedia/commons/e/ed/Parmenides.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 732- (Мир энциклопедий).)'),
  (222,221,'В таблицу \"Высказывания\" добавлена запись «мыслить и быть — одно и то же» (221-219-)','idiom','2017-01-30 22:25:29','i','insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (221,219,«мыслить и быть — одно и то же»,)'),
  (224,223,'В таблицу \"Высказывания\" добавлена запись Бытие есть, а небытия — нет (223-219-)','idiom','2017-01-30 22:25:54','i','insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (223,219,Бытие есть, а небытия — нет,)'),
  (226,225,'В таблицу \"Разделы науки\" добавлена запись Эпистемология (225-133-Эпистемология-или гносеоло́гия,  теория познания)','section','2017-01-30 22:27:21','i','insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (225,133,Эпистемология,или гносеоло́гия,  теория познания)'),
  (228,227,'В таблицу \"Разделы науки\" добавлена запись онтология (227-133-онтология-учение о сущем; учение о бытии как таковом;)','section','2017-01-30 22:27:56','i','insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (227,133,онтология,учение о сущем; учение о бытии как таковом;)'),
  (230,229,'В таблицу \"Персоны\" добавлена запись Зенон (229-Элейский--0001-01-01-0001-01-01-нет страны-нет предшественника-213-Апории, дихотомия-Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].-https://ru.wikipedia.org/wiki/Зенон_Элейский-https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий).-)','person','2017-01-30 22:33:03','i','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (229,Зенон,Элейский,,0001-01-01,0001-01-01,,,213,Апории, дихотомия,Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].,https://ru.wikipedia.org/wiki/Зенон_Элейский,https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий).)'),
  (232,231,'В таблицу \"Словарь\" добавлена запись Дихотомия  (231-229-нет предыдущего понятия-Чтобы преодолеть путь, нужно сначала преодолеть половину пути, а чтобы преодолеть половину пути, нужно сначала преодолеть половину половины, и так до бесконечности.--)','dictionary','2017-01-30 22:42:15','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (231,229,,Чтобы преодолеть путь, нужно сначала преодолеть половину пути, а чтобы преодолеть половину пути, нужно сначала преодолеть половину половины, и так до бесконечности.,,)'),
  (234,233,'В таблицу \"Словарь\" добавлена запись Стрела Зенона (233-229-нет предыдущего понятия-Летящая стрела неподвижна, так как в каждый момент времени она занимает равное себе положение, то есть покоится; поскольку она покоится в каждый момент времени, то она покоится во все моменты времени, то есть не существует момента времени, в котором стрела совершает движение.--)','dictionary','2017-01-30 22:42:42','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (233,229,,Летящая стрела неподвижна, так как в каждый момент времени она занимает равное себе положение, то есть покоится; поскольку она покоится в каждый момент времени, то она покоится во все моменты времени, то есть не существует момента времени, в котором стрела совершает движение.,,)'),
  (236,235,'В таблицу \"Словарь\" добавлена запись Акме (235-нет автора-нет предыдущего понятия-соматическое, физиологическое, психологическое и социальное состояние личности, которое характеризуется зрелостью её развития, достижением наиболее высоких показателей в деятельности, творчестве.[1] Считается, что данный период в жизни человека приходится на возраст от 30 до 50 лет.\r\n\r\nВ психоанализе данный термин обозначает «пик» удовлетворения в сексуальном акте. В медицине: высшая точка развития заболевания.--)','dictionary','2017-01-30 22:45:09','i','insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (235,,,соматическое, физиологическое, психологическое и социальное состояние личности, которое характеризуется зрелостью её развития, достижением наиболее высоких показателей в деятельности, творчестве.[1] Считается, что данный период в жизни человека приходится на возраст от 30 до 50 лет.\r\n\r\nВ психоанализе данный термин обозначает «пик» удовлетворения в сексуальном акте. В медицине: высшая точка развития заболевания.,,)'),
  (237,147,'В таблице \"Персоны\" изменена запись Пифагор (147---0001-01-01-0001-01-01-143-нет предшественника-145-Пифагор, число, пифагореизм, -Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.-https://ru.wikipedia.org/wiki/Пифагор-https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий). на Пифагор (147-Самосский--0001-01-01-0001-01-01-143-нет предшественника-145-Пифагор, число, пифагореизм, -Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.-https://ru.wikipedia.org/wiki/Пифагор-https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).)','person','2017-02-01 10:56:24','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (147,Пифагор,,,0001-01-01,0001-01-01,143,,145,Пифагор, число, пифагореизм, ,Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.,https://ru.wikipedia.org/wiki/Пифагор,https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).)'),
  (238,229,'В таблице \"Персоны\" изменена запись Зенон (229-Элейский--0001-01-01-0001-01-01-нет страны-нет предшественника-213-Апории, дихотомия-Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].-https://ru.wikipedia.org/wiki/Зенон_Элейский-https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий). на Зенон (229-Элейский--0001-01-01-0001-01-01-215-нет предшественника-213-Апории, дихотомия-Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].-https://ru.wikipedia.org/wiki/Зенон_Элейский-https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий).)','person','2017-02-06 20:44:16','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (229,Зенон,Элейский,,0001-01-01,0001-01-01,,,213,Апории, дихотомия,Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].,https://ru.wikipedia.org/wiki/Зенон_Элейский,https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий).)'),
  (239,147,'В таблице \"Персоны\" изменена запись Пифагор (147-Самосский--0001-01-01-0001-01-01-143-нет предшественника-145-Пифагор, число, пифагореизм, -Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.-https://ru.wikipedia.org/wiki/Пифагор-https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий). на Пифагор (147-Самосский--0101-01-01-0001-01-01-143-нет предшественника-145-Пифагор, число, пифагореизм, -Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.-https://ru.wikipedia.org/wiki/Пифагор-https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).)','person','2017-02-06 23:31:01','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (147,Пифагор,Самосский,,0001-01-01,0001-01-01,143,,145,Пифагор, число, пифагореизм, ,Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.,https://ru.wikipedia.org/wiki/Пифагор,https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).)'),
  (240,171,'В таблице \"Персоны\" изменена запись Сиддхартха  (171-Гаутама--0001-01-31-0001-01-01-169-нет предшественника-нет школы-Буддизм, будда, Шакьямуни, дхарма, бодхи-дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.-https://ru.wikipedia.org/wiki/Будда_Шакьямуни-https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий). на Сиддхартха  (171-Гаутама--0101-01-31-0001-01-01-169-нет предшественника-нет школы-Буддизм, будда, Шакьямуни, дхарма, бодхи-дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.-https://ru.wikipedia.org/wiki/Будда_Шакьямуни-https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий).)','person','2017-02-06 23:31:14','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (171,Сиддхартха ,Гаутама,,0001-01-31,0001-01-01,169,,,Буддизм, будда, Шакьямуни, дхарма, бодхи,дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.,https://ru.wikipedia.org/wiki/Будда_Шакьямуни,https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий).)'),
  (241,176,'В таблице \"Персоны\" изменена запись Конфуций (176---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Конфуцианство, Кун Цю-основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.-https://ru.wikipedia.org/wiki/Конфуций-https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 502- (Мир энциклопедий). на Конфуций (176---0101-01-01-0001-01-01-174-нет предшественника-нет школы-Конфуцианство, Кун Цю-основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.-https://ru.wikipedia.org/wiki/Конфуций-https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 502- (Мир энциклопедий).)','person','2017-02-06 23:31:29','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (176,Конфуций,,,0001-01-01,0001-01-01,174,,,Конфуцианство, Кун Цю,основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.,https://ru.wikipedia.org/wiki/Конфуций,https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 502- (Мир энциклопедий).)'),
  (242,187,'В таблице \"Персоны\" изменена запись Лао-цзы (187---0001-01-01-0001-01-01-174-нет предшественника-нет школы-Дао, Трое Чистых, - древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»-https://ru.wikipedia.org/wiki/Лао-цзы-https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 542- (Мир энциклопедий). на Лао-цзы (187---0101-01-01-0001-01-01-174-нет предшественника-нет школы-Дао, Трое Чистых, - древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»-https://ru.wikipedia.org/wiki/Лао-цзы-https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 542- (Мир энциклопедий).)','person','2017-02-06 23:31:39','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (187,Лао-цзы,,,0001-01-01,0001-01-01,174,,,Дао, Трое Чистых, , древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»,https://ru.wikipedia.org/wiki/Лао-цзы,https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 542- (Мир энциклопедий).)'),
  (243,202,'В таблице \"Персоны\" изменена запись Фалес  (202-Милетский--0001-01-01-0001-01-01-200-нет предшественника-198-Семь мудрецов, вода-древнегреческий философ и математик из Милета (Малая Азия). Представитель ионической натурфилософии и основатель милетской (ионийской) школы, с которой начинается история европейской науки. Традиционно считается основоположником греческой философии (и науки) — он неизменно открывал список «семи мудрецов», заложивших основы греческой культуры и государственности.-https://ru.wikipedia.org/wiki/Фалес_Милетский-https://upload.wikimedia.org/wikipedia/commons/c/c6/Illustrerad_Verldshistoria_band_I_Ill_107.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 1075- (Мир энциклопедий). на Фалес  (202-Милетский--0101-01-01-0001-01-01-200-нет предшественника-198-Семь мудрецов, вода-древнегреческий философ и математик из Милета (Малая Азия). Представитель ионической натурфилософии и основатель милетской (ионийской) школы, с которой начинается история европейской науки. Традиционно считается основоположником греческой философии (и науки) — он неизменно открывал список «семи мудрецов», заложивших основы греческой культуры и государственности.-https://ru.wikipedia.org/wiki/Фалес_Милетский-https://upload.wikimedia.org/wikipedia/commons/c/c6/Illustrerad_Verldshistoria_band_I_Ill_107.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 1075- (Мир энциклопедий).)','person','2017-02-06 23:31:45','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (202,Фалес ,Милетский,,0001-01-01,0001-01-01,200,,198,Семь мудрецов, вода,древнегреческий философ и математик из Милета (Малая Азия). Представитель ионической натурфилософии и основатель милетской (ионийской) школы, с которой начинается история европейской науки. Традиционно считается основоположником греческой философии (и науки) — он неизменно открывал список «семи мудрецов», заложивших основы греческой культуры и государственности.,https://ru.wikipedia.org/wiki/Фалес_Милетский,https://upload.wikimedia.org/wikipedia/commons/c/c6/Illustrerad_Verldshistoria_band_I_Ill_107.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 1075- (Мир энциклопедий).)'),
  (244,204,'В таблице \"Персоны\" изменена запись Анаксима́ндр  (204-Миле́тский--0001-01-01-0001-01-01-200-нет предшественника-198-Апейрон-Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.-https://ru.wikipedia.org/wiki/Анаксимандр-https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg- на Анаксима́ндр  (204-Миле́тский--0101-01-01-0001-01-01-200-нет предшественника-198-Апейрон-Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.-https://ru.wikipedia.org/wiki/Анаксимандр-https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg-)','person','2017-02-06 23:31:49','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (204,Анаксима́ндр ,Миле́тский,,0001-01-01,0001-01-01,200,,198,Апейрон,Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.,https://ru.wikipedia.org/wiki/Анаксимандр,https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg,)'),
  (245,206,'В таблице \"Персоны\" изменена запись Анаксимен (206---0001-01-01-0001-01-01-200-нет предшественника-198-Воздух-Анаксимен — последний представитель милетской школы. Анаксимен укрепил и завершил тенденцию стихийного материализма — поиск естественных причин явлений и вещей. Как ранее Фалес и Анаксимандр, первоосновой мира он полагает определенный вид материи. Такой материей он считает неограниченный, бесконечный, имеющий неопределённую форму воздух, из которого возникает всё остальное. «Анаксимен… провозглашает воздух началом сущего, ибо из него всё возникает и к нему всё возвращается».-https://ru.wikipedia.org/wiki/Анаксимен-https://upload.wikimedia.org/wikipedia/commons/2/2d/Anaximenes.jpg- на Анаксимен (206---0101-01-01-0001-01-01-200-нет предшественника-198-Воздух-Анаксимен — последний представитель милетской школы. Анаксимен укрепил и завершил тенденцию стихийного материализма — поиск естественных причин явлений и вещей. Как ранее Фалес и Анаксимандр, первоосновой мира он полагает определенный вид материи. Такой материей он считает неограниченный, бесконечный, имеющий неопределённую форму воздух, из которого возникает всё остальное. «Анаксимен… провозглашает воздух началом сущего, ибо из него всё возникает и к нему всё возвращается».-https://ru.wikipedia.org/wiki/Анаксимен-https://upload.wikimedia.org/wikipedia/commons/2/2d/Anaximenes.jpg-)','person','2017-02-06 23:32:02','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (206,Анаксимен,,,0001-01-01,0001-01-01,200,,198,Воздух,Анаксимен — последний представитель милетской школы. Анаксимен укрепил и завершил тенденцию стихийного материализма — поиск естественных причин явлений и вещей. Как ранее Фалес и Анаксимандр, первоосновой мира он полагает определенный вид материи. Такой материей он считает неограниченный, бесконечный, имеющий неопределённую форму воздух, из которого возникает всё остальное. «Анаксимен… провозглашает воздух началом сущего, ибо из него всё возникает и к нему всё возвращается».,https://ru.wikipedia.org/wiki/Анаксимен,https://upload.wikimedia.org/wikipedia/commons/2/2d/Anaximenes.jpg,)'),
  (246,217,'В таблице \"Персоны\" изменена запись Ксенофан (217---0001-01-01-0001-01-01-215-нет предшественника-213-Элея, рапсод, элеаты-По Ксенофану, мифология — это продукт исключительно человеческого воображения. Образы богов созданы людьми как их подобие, поэтому боги не только не превосходят людей нравственно, но и не могут быть предметом поклонения:-https://ru.wikipedia.org/wiki/Ксенофан-https://upload.wikimedia.org/wikipedia/commons/5/5f/Xenophanes_in_Thomas_Stanley_History_of_Philosophy.jpg- на Ксенофан (217---0101-01-01-0001-01-01-215-нет предшественника-213-Элея, рапсод, элеаты-По Ксенофану, мифология — это продукт исключительно человеческого воображения. Образы богов созданы людьми как их подобие, поэтому боги не только не превосходят людей нравственно, но и не могут быть предметом поклонения:-https://ru.wikipedia.org/wiki/Ксенофан-https://upload.wikimedia.org/wikipedia/commons/5/5f/Xenophanes_in_Thomas_Stanley_History_of_Philosophy.jpg-)','person','2017-02-06 23:32:07','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (217,Ксенофан,,,0001-01-01,0001-01-01,215,,213,Элея, рапсод, элеаты,По Ксенофану, мифология — это продукт исключительно человеческого воображения. Образы богов созданы людьми как их подобие, поэтому боги не только не превосходят людей нравственно, но и не могут быть предметом поклонения:,https://ru.wikipedia.org/wiki/Ксенофан,https://upload.wikimedia.org/wikipedia/commons/5/5f/Xenophanes_in_Thomas_Stanley_History_of_Philosophy.jpg,)'),
  (247,219,'В таблице \"Персоны\" изменена запись Пармени́д  (219---0001-01-01-0001-01-01-215-нет предшественника-213-метафизика, бытие, онтология, гносеология-Диоген Лаэртский так передаёт его философию: «Критерием истины называл он разум; в чувствах же, — говорил он, — точности нет». По замечанию БСЭ (2-е изд.), — отвергая ощущения и опыт как источник знания, Парменид противостоял ионийскому естествознанию, возражал против требования Гераклита «прислушиваться к природе»[8]. «Парменид — мыслитель действительно необыкновенной глубины», — говорит Сократ в диалоге Платона «Теэтет». Он был современником Гераклита[8], с которым спорил. Проф. Дж. Бернет назвал Парменида «отцом материализма[17]».-https://ru.wikipedia.org/wiki/Парменид-https://upload.wikimedia.org/wikipedia/commons/e/ed/Parmenides.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 732- (Мир энциклопедий). на Пармени́д  (219---0101-01-01-0001-01-01-215-нет предшественника-213-метафизика, бытие, онтология, гносеология-Диоген Лаэртский так передаёт его философию: «Критерием истины называл он разум; в чувствах же, — говорил он, — точности нет». По замечанию БСЭ (2-е изд.), — отвергая ощущения и опыт как источник знания, Парменид противостоял ионийскому естествознанию, возражал против требования Гераклита «прислушиваться к природе»[8]. «Парменид — мыслитель действительно необыкновенной глубины», — говорит Сократ в диалоге Платона «Теэтет». Он был современником Гераклита[8], с которым спорил. Проф. Дж. Бернет назвал Парменида «отцом материализма[17]».-https://ru.wikipedia.org/wiki/Парменид-https://upload.wikimedia.org/wikipedia/commons/e/ed/Parmenides.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 732- (Мир энциклопедий).)','person','2017-02-06 23:32:11','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (219,Пармени́д ,,,0001-01-01,0001-01-01,215,,213,метафизика, бытие, онтология, гносеология,Диоген Лаэртский так передаёт его философию: «Критерием истины называл он разум; в чувствах же, — говорил он, — точности нет». По замечанию БСЭ (2-е изд.), — отвергая ощущения и опыт как источник знания, Парменид противостоял ионийскому естествознанию, возражал против требования Гераклита «прислушиваться к природе»[8]. «Парменид — мыслитель действительно необыкновенной глубины», — говорит Сократ в диалоге Платона «Теэтет». Он был современником Гераклита[8], с которым спорил. Проф. Дж. Бернет назвал Парменида «отцом материализма[17]».,https://ru.wikipedia.org/wiki/Парменид,https://upload.wikimedia.org/wikipedia/commons/e/ed/Parmenides.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 732- (Мир энциклопедий).)'),
  (248,229,'В таблице \"Персоны\" изменена запись Зенон (229-Элейский--0001-01-01-0001-01-01-215-нет предшественника-213-Апории, дихотомия-Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].-https://ru.wikipedia.org/wiki/Зенон_Элейский-https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий). на Зенон (229-Элейский--0101-01-01-0001-01-01-215-нет предшественника-213-Апории, дихотомия-Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].-https://ru.wikipedia.org/wiki/Зенон_Элейский-https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg-Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий).)','person','2017-02-06 23:32:18','u','insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (229,Зенон,Элейский,,0001-01-01,0001-01-01,215,,213,Апории, дихотомия,Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].,https://ru.wikipedia.org/wiki/Зенон_Элейский,https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg,Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий).)');
COMMIT;

/* Data for the `core_seq` table  (LIMIT 0,500) */

INSERT INTO `core_seq` (`id`, `st`) VALUES
  (248,0);
COMMIT;

/* Data for the `t_country_group` table  (LIMIT 0,500) */

INSERT INTO `t_country_group` (`id`, `caption`) VALUES
  (141,'Древняя Греция'),
  (181,'Древний Восток');
COMMIT;

/* Data for the `t_country` table  (LIMIT 0,500) */

INSERT INTO `t_country` (`id`, `pid`, `country`, `language`) VALUES
  (143,141,'Самос','Древнегреческий'),
  (169,NULL,'Непал','непали'),
  (174,NULL,'Китай','Китайский'),
  (200,NULL,'Милет','Древнегреческий'),
  (215,141,'Великая Греция','Древнегреческий');
COMMIT;

/* Data for the `t_dictionary` table  (LIMIT 0,500) */

INSERT INTO `t_dictionary` (`id`, `pid`, `parent`, `caption`, `description`, `bibliography`, `foreign_lang`) VALUES
  (151,NULL,NULL,'Мифология','тип функционирования культурных программ, предполагающий их некритическое восприятие индивидуальным и массовым сознанием, сакрализацию их содержания и неукоснительность исполнения.','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 634- (Мир энциклопедий).','греч. myphos - сказание и logos - рассказ'),
  (153,NULL,NULL,'Гелиос','в древнегреческой мифологии[3] солнечное божество, сын титана Гипериона (откуда пошло его прозвище «Гиперионид») и Тейи[4] (либо сын Гипериона и Ирифессы[5]), брат Селены и Эос[6].','',''),
  (155,NULL,NULL,'Деме́тра ',' в древнегреческой мифологии богиня плодородия, покровительница земледелия[1].','',''),
  (157,NULL,NULL,'Персефона','в древнегреческой мифологии богиня плодородия и царства мёртвых. Дочь Деметры и Зевса, супруга Аида. У римлян — Прозерпина.','',''),
  (159,NULL,NULL,'Аи́д ','в древнегреческой мифологии бог подземного царства мёртвых и название самого царства мёртвых. Старший сын Кроноса и Реи[4], брат Зевса, Посейдона, Геры, Деметры и Гестии. Супруг Персефоны, вместе с ним почитаемой и призываемой.','','(др.-греч. Ἀΐδης или ᾍδης, или Га́дес; у римлян Плуто́н, др.-греч. Πλούτων, лат. Pluto — «богатый»; также Дит лат. Dis или Орк лат. Orcus[⇨])'),
  (161,NULL,NULL,'Амон-Ра','древнеегипетский бог солнца, верховное божество в религии древних египтян. Его имя означает «Солнце» (коптское PH). Центром культа был Гелиополь, где Ра был отождествлён с более древним местным солнечным божеством, Атумом, и где ему были посвящены, как его воплощения, птица Феникс, бык Мневис и обелиск Бен-Бен. В других религиозных центрах Ра в русле религиозного синкретизма также сопоставлялся с местными божествами света: Амоном (в Фивах), под именем Амона-Ра, Хнумом (в Элефантине) — в форме Хнума-Ра, Гором — в форме Ра-Горахти (Ра-Хорахте). Последнее сопоставление было особенно распространено. Ра возглавлял гелиопольскую эннеаду божеств.','',''),
  (163,NULL,NULL,'Антропоморфизм','перенесение человеческого образа и его свойств на неодушевлённые предметы, на одушевлённых существ, на явления и силы природы, на сверхъестественных существ, на абстрактные понятия и др','','фр. anthropomorphisme'),
  (165,NULL,NULL,'Монотеизм',' религиозное представление о существовании только одного Бога или о единственности Бога.','',''),
  (167,NULL,NULL,'Буддизм','религиозно-философское учение (дхарма) о духовном пробуждении (бодхи), возникшее около VI века до н. э. в Древней Индии.','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий).','санскр. बुद्ध धर्म, buddha dharma'),
  (178,NULL,NULL,'Конфуцианство','этико-философское учение, разработанное Конфуцием (551—479 до н. э.) и развитое его последователями, вошедшее в религиозный комплекс Китая, Кореи, Японии и некоторых других стран','','кит. трад. 儒學'),
  (185,NULL,NULL,'Пантеизм','Несмотря на существующие различные течения внутри пантеизма, центральные идеи в большинстве форм пантеизма постоянны: Вселенная как всеобъемлющее единство и святость природы.','',' от древнегреческих слов παν (пан) — «всё, всякий» и θεός (теос) — «Бог, божество»'),
  (194,187,NULL,'Дао','Конфуций и ранние конфуцианцы придали ему этическое значение, истолковав как «путь человека», то есть нравственное поведение и основанный на морали социальный порядок. Наиболее известная и значимая даосская интерпретация Дао содержится в трактате Дао Дэ Цзин.','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 288- (Мир энциклопедий).','кит. 道, буквально — путь'),
  (196,187,194,'Даосизм',' учение о дао или «пути вещей», китайское традиционное учение, включающее элементы религии и философии','','кит. упр. 道教, пиньинь: dàojiào'),
  (209,204,NULL,'Апейрон','понятие древнегреческой философии, введённое Анаксимандром, означающее неопределённое, беспредельное и бесконечное первовещество. Апейрон у Анаксимандра является основой мира и существует в вечном движении. Апейрон — это бескачественная материя; всё возникло путём выделения из апейрона противоположностей (например, горячее и холодное).','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 52- (Мир энциклопедий).','греч. ἄπειρον, «бесконечное, беспредельное»'),
  (211,NULL,NULL,'Архэ','в досократовской древнегреческой философии — первооснова, первовещество, первоэлемент, из которого состоит мир. Для характеристики учений первых философов этот термин использовал Аристотель.','','(греч. ἀρχή — начало, принцип) '),
  (231,229,NULL,'Дихотомия ','Чтобы преодолеть путь, нужно сначала преодолеть половину пути, а чтобы преодолеть половину пути, нужно сначала преодолеть половину половины, и так до бесконечности.','',''),
  (233,229,NULL,'Стрела Зенона','Летящая стрела неподвижна, так как в каждый момент времени она занимает равное себе положение, то есть покоится; поскольку она покоится в каждый момент времени, то она покоится во все моменты времени, то есть не существует момента времени, в котором стрела совершает движение.','',''),
  (235,NULL,NULL,'Акме','соматическое, физиологическое, психологическое и социальное состояние личности, которое характеризуется зрелостью её развития, достижением наиболее высоких показателей в деятельности, творчестве.[1] Считается, что данный период в жизни человека приходится на возраст от 30 до 50 лет.\r\n\r\nВ психоанализе данный термин обозначает «пик» удовлетворения в сексуальном акте. В медицине: высшая точка развития заболевания.','','');
COMMIT;

/* Data for the `t_school` table  (LIMIT 0,500) */

INSERT INTO `t_school` (`id`, `school`, `annotation`, `parent`, `bibliography`) VALUES
  (145,'Пифагореизм','Так называемые пифагорейцы, взявшись за математические науки, первые подвинули их вперёд; вскормленные на этих науках, они признали математические начала за начала всего существующего. Из таких начал, естественно, первыми являются числа. В числах усматривали они множество аналогий или подобий с вещами… так что одно свойство чисел являлось им как справедливость, другое — как душа или разум, ещё другое — как благоприятный случай и т. д. Далее они находили в числах свойства и отношения музыкальной гармонии, и так как все прочие вещи по своей природе являлись им подобием чисел, числа же — первыми из всей природы, то они и признали, что элементы числа суть элементы всего сущего, и что все небо есть гармония и число (Аристотель, Met., I, 5).',NULL,'Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).'),
  (198,'Милетская школа','Исходя из принципа сохранения: «ничто не возникает из ничего», милетцы полагали единое вечное, бесконечное, «божественное» первоначало видимого многообразия вещей, источник жизни и существования космоса. Таким образом, в основе многообразия явлений они усмотрели некое единое правещество; для Фалеса это — вода, для Анаксимандра — апейрон (неопределённое и беспредельное первовещество), для Анаксимена — воздух. («Воду» Фалеса и «воздух» Анаксимена следует, конечно, понимать условно-аллегорически, как символ комплекса абстрактных свойств такого первовещества.)',NULL,''),
  (213,'Элейская школа','В отличие от большинства досократиков, элейцы не занимались вопросами естествознания, но разрабатывали теоретическое учение о бытии (предложив впервые сам этот термин), заложив фундамент классической греческой онтологии.',NULL,'');
COMMIT;

/* Data for the `t_person` table  (LIMIT 0,500) */

INSERT INTO `t_person` (`id`, `name`, `surname`, `last_name`, `birth_date`, `death_date`, `country`, `parent`, `school`, `key_word`, `annotation`, `link`, `portrait`, `bibliography`) VALUES
  (147,'Пифагор','Самосский','','0101-01-01','0001-01-01',143,NULL,145,'Пифагор, число, пифагореизм, ','Пифагор Самосский (др.-греч. Πυθαγόρας ὁ Σάμιος, лат. Pythagoras, «пифийский вещатель»[2]; 570—490 гг. до н. э.) — древнегреческий философ, математик и мистик, создатель религиозно-философской школы пифагорейцев.','https://ru.wikipedia.org/wiki/Пифагор','https://commons.wikimedia.org/wiki/File:Kapitolinischer_Pythagoras.jpg','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 753 - (Мир энциклопедий).'),
  (171,'Сиддхартха ','Гаутама','','0101-01-31','0001-01-01',169,NULL,NULL,'Буддизм, будда, Шакьямуни, дхарма, бодхи','дословно «Пробуждённый мудрец (молчальник) из рода Шакья (Сакья)[2]») — духовный учитель, легендарный основатель буддизма, одной из трёх мировых религий.','https://ru.wikipedia.org/wiki/Будда_Шакьямуни','https://upload.wikimedia.org/wikipedia/commons/f/ff/Buddha_in_Sarnath_Museum_%28Dhammajak_Mutra%29.jpg?uselang=ru','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 135- (Мир энциклопедий).'),
  (176,'Конфуций','','','0101-01-01','0001-01-01',174,NULL,NULL,'Конфуцианство, Кун Цю','основал первый университет и привёл в систему летописи, составленные в разных княжествах. Учение Конфуция о правилах поведения князей, чиновников, воинов и крестьян распространялось в Китае так же широко, как учение Будды в Индии.','https://ru.wikipedia.org/wiki/Конфуций','https://upload.wikimedia.org/wikipedia/commons/4/4f/Konfuzius-1770.jpg','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 502- (Мир энциклопедий).'),
  (187,'Лао-цзы','','','0101-01-01','0001-01-01',174,NULL,NULL,'Дао, Трое Чистых, ',' древнекитайский философ VI–V веков до н. э., которому приписывается авторство классического даосского философского трактата «Дао Дэ Цзин»','https://ru.wikipedia.org/wiki/Лао-цзы','https://upload.wikimedia.org/wikipedia/commons/3/3a/Lao_Tzu_-_Project_Gutenberg_eText_15250.jpg','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 542- (Мир энциклопедий).'),
  (202,'Фалес ','Милетский','','0101-01-01','0001-01-01',200,NULL,198,'Семь мудрецов, вода','древнегреческий философ и математик из Милета (Малая Азия). Представитель ионической натурфилософии и основатель милетской (ионийской) школы, с которой начинается история европейской науки. Традиционно считается основоположником греческой философии (и науки) — он неизменно открывал список «семи мудрецов», заложивших основы греческой культуры и государственности.','https://ru.wikipedia.org/wiki/Фалес_Милетский','https://upload.wikimedia.org/wikipedia/commons/c/c6/Illustrerad_Verldshistoria_band_I_Ill_107.jpg','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 1075- (Мир энциклопедий).'),
  (204,'Анаксима́ндр ','Миле́тский','','0101-01-01','0001-01-01',200,NULL,198,'Апейрон','Автор первого греческого научного сочинения, написанного прозой («О природе», 547 до н. э.). Ввёл термин «закон», применив понятие общественной практики к природе и науке. Анаксимандру приписывают одну из первых формулировок закона сохранения материи («из тех же вещей, из которых рождаются все сущие вещи, в эти же самые вещи они разрушаются согласно предназначению»).Впервые в Греции установил гномон — простейшие солнечные часы и усовершенствовал вавилонские солнечные часы, имевшие форму сферической чаши — так называемый скафис.','https://ru.wikipedia.org/wiki/Анаксимандр','https://upload.wikimedia.org/wikipedia/commons/3/38/Anaximander.jpg',''),
  (206,'Анаксимен','','','0101-01-01','0001-01-01',200,NULL,198,'Воздух','Анаксимен — последний представитель милетской школы. Анаксимен укрепил и завершил тенденцию стихийного материализма — поиск естественных причин явлений и вещей. Как ранее Фалес и Анаксимандр, первоосновой мира он полагает определенный вид материи. Такой материей он считает неограниченный, бесконечный, имеющий неопределённую форму воздух, из которого возникает всё остальное. «Анаксимен… провозглашает воздух началом сущего, ибо из него всё возникает и к нему всё возвращается».','https://ru.wikipedia.org/wiki/Анаксимен','https://upload.wikimedia.org/wikipedia/commons/2/2d/Anaximenes.jpg',''),
  (217,'Ксенофан','','','0101-01-01','0001-01-01',215,NULL,213,'Элея, рапсод, элеаты','По Ксенофану, мифология — это продукт исключительно человеческого воображения. Образы богов созданы людьми как их подобие, поэтому боги не только не превосходят людей нравственно, но и не могут быть предметом поклонения:','https://ru.wikipedia.org/wiki/Ксенофан','https://upload.wikimedia.org/wikipedia/commons/5/5f/Xenophanes_in_Thomas_Stanley_History_of_Philosophy.jpg',''),
  (219,'Пармени́д ','','','0101-01-01','0001-01-01',215,NULL,213,'метафизика, бытие, онтология, гносеология','Диоген Лаэртский так передаёт его философию: «Критерием истины называл он разум; в чувствах же, — говорил он, — точности нет». По замечанию БСЭ (2-е изд.), — отвергая ощущения и опыт как источник знания, Парменид противостоял ионийскому естествознанию, возражал против требования Гераклита «прислушиваться к природе»[8]. «Парменид — мыслитель действительно необыкновенной глубины», — говорит Сократ в диалоге Платона «Теэтет». Он был современником Гераклита[8], с которым спорил. Проф. Дж. Бернет назвал Парменида «отцом материализма[17]».','https://ru.wikipedia.org/wiki/Парменид','https://upload.wikimedia.org/wikipedia/commons/e/ed/Parmenides.jpg','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 732- (Мир энциклопедий).'),
  (229,'Зенон','Элейский','','0101-01-01','0001-01-01',215,NULL,213,'Апории, дихотомия','Зенон участвовал в заговоре против элейского тирана того времени, имя которого Диогену точно было неизвестно. Был арестован. На допросе, при требовании выдать сообщников, вёл себя стойко и даже, согласно Антисфену, откусил собственный язык и выплюнул его в лицо тирану. Присутствовавшие граждане были настолько потрясены произошедшим, что побили тирана камнями. По сведениям же Гермиппа, Зенон был тираном казнён: его бросили в ступу и истолкли в ней[6].\r\n\r\nДиоген сообщает, что Зенон был любовником своего учителя[7], однако Афиней решительно опровергает подобное утверждение: «Но что всего отвратительнее и всего лживее — так это безо всякой нужды сказать, что согражданин Парменида Зенон был его любовником»[8].','https://ru.wikipedia.org/wiki/Зенон_Элейский','https://upload.wikimedia.org/wikipedia/commons/d/d9/Diogenis_Laertii_De_Vitis_%281627%29_-_Zenon_of_Elea_or_Zenon_of_Citium.jpg','Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - С. 382- (Мир энциклопедий).');
COMMIT;

/* Data for the `t_section` table  (LIMIT 0,500) */

INSERT INTO `t_section` (`id`, `pid`, `caption`, `annotation`) VALUES
  (133,NULL,'Философия','особая форма познания мира, вырабатывающая систему знаний о наиболее общих характеристиках, предельно-обобщающих понятиях и фундаментальных принципах реальности (бытия) и познания, бытия человека, об отношении человека и мира.'),
  (135,133,'История философии','раздел философии, изучающий исторические типы философии.'),
  (137,133,'Социальная философия','раздел философии, призванный ответить на вопрос о том, что есть общество и какое место занимает в нём человек.'),
  (225,133,'Эпистемология','или гносеоло́гия,  теория познания'),
  (227,133,'онтология','учение о сущем; учение о бытии как таковом;');
COMMIT;

/* Data for the `t_opus` table  (LIMIT 0,500) */

INSERT INTO `t_opus` (`id`, `pid`, `caption`, `annotation`, `cover`, `published`, `bibliography`, `key_word`, `section`) VALUES
  (183,176,'Лунь Юй','один из наиболее знаменитых текстов Восточной Азии. Главная книга конфуцианства, составленная учениками Конфуция из кратких заметок, фиксирующих высказывания, поступки учителя, а также диалоги с его участием.','','0000-00-00','Лунь юй\r\nАвтор:\tКонфуций\r\nПеревод:\tПереломов Л.','«Беседы и суждения», «Аналекты Конфуция»',NULL),
  (192,187,'Дао дэ цзин','Основная идея этого произведения — понятие дао — трактуется как естественный порядок вещей, не допускающий постороннего вмешательства, «небесная воля» или «чистое небытие». Споры о содержании книги и её авторе продолжаются до сих пор.','','0001-01-01','','Дао, Даосизм',133);
COMMIT;

/* Data for the `t_files` table  (LIMIT 0,500) */

INSERT INTO `t_files` (`id`, `pid`, `caption`, `author`, `link`, `add_date`, `visible`, `bibliography`, `type`, `key_word`, `format`) VALUES
  (67,NULL,'Новейший философский словарь','Грицанов','https://drive.google.com/file/d/0B7gd14FVoD9rOVZsMEw5eHZvS00/view?usp=sharing','2017-01-16 19:51:51',1,'Новейший философский словарь: 3-е изд., исправл. - Мн.: Книжный Дом. 2003. - 1280 с. - (Мир энциклопедий).','Словарь','Философский словарь, 2003','doc'),
  (123,NULL,'Краткая история философии. Нескучная книга.','Гусев Д.А.','https://drive.google.com/file/d/0B7gd14FVoD9raVZsTTN2SjN5Xzg/view?usp=sharing','2017-01-17 11:34:55',1,'Гусев Д.А. Краткая история философии. Издательство НЦ ЭНАС, 2003 г., 224 с.','Книга','история философии, популярная','fb2'),
  (131,NULL,'Социальная философия: Учебное пособие','Петр Васильевич Алексеев','https://drive.google.com/file/d/0B7gd14FVoD9rcGFWUjdfVTN4LVE/view?usp=sharing','2017-01-24 20:14:06',1,'','Учебное пособие','Социальная философия, Алексеев','mobi');
COMMIT;

/* Data for the `t_idiom` table  (LIMIT 0,500) */

INSERT INTO `t_idiom` (`id`, `pid`, `idiom`, `original`) VALUES
  (125,NULL,'Navigare necesse est, vivere non est necesse','Плыть необходимо, а жить — нет!(Помпей) у плутарха в Жизнеописаниях'),
  (128,NULL,'Carpe diem','Лови день(Гораций)в «Оде к Левконое»'),
  (139,NULL,'Quis custodiet ipsos custodes?','Кто устережёт самих сторожей?(Ювенал Сатиры)'),
  (149,147,'Я не мудрец, но только философ.',''),
  (221,219,'«мыслить и быть — одно и то же»',''),
  (223,219,'Бытие есть, а небытия — нет','');
COMMIT;

/* Definition for the `t_country_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_after_ins_tr1` AFTER INSERT ON `t_country`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Страны" добавлена запись ',
                  coalesce(NEW.country, 'нет название'),' (',
                  NEW.id,'-',
                  coalesce(NEW.pid, 'нет группы'), '-',
                  coalesce(NEW.language, 'нет языка'), ')'),
           'country',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (',
                  NEW.id,',',
                  coalesce(NEW.pid, ''),',',
                  coalesce(NEW.country, ''),',',
                  coalesce(NEW.language, ''),')'));
  END$$

DELIMITER ;

/* Definition for the `t_country_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_after_upd_tr1` AFTER UPDATE ON `t_country`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Страны" изменена запись ',
                  OLD.country,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid, 'нет группы'), '-',
                  OLD.language, ') на ',
                  NEW.country, ' (',
                  NEW.id,'-',
                  coalesce(NEW.pid, 'нет группы'), '-',
                  NEW.language, ')'),
           'country',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_country(id, \r\n    pid, \r\n    country, \r\n    language) values (',
                  OLD.id,',',
                  coalesce(OLD.pid, ''),',',
                  OLD.country,',',
                  OLD.language,')'));
  END$$

DELIMITER ;

/* Definition for the `t_country_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_after_del_tr1` AFTER DELETE ON `t_country`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Страны" удалена запись ',
                  OLD.country,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid, 'нет группы'), '-',
                  OLD.language, ')'),
           'country',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_country(id, \r\n    pid,\r\n    country, \r\n    language) values (',
                  OLD.id,',',
                  coalesce(OLD.pid, ''),',',
                  OLD.country,',',
                  OLD.language,')'));
  END$$

DELIMITER ;

/* Definition for the `t_country_group_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_group_after_ins_tr1` AFTER INSERT ON `t_country_group`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Группы стран" добавлена запись ',
                  NEW.caption,' (',
                  NEW.id, ')'),
           'country_group',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_country_group(id,\r\n     caption) values (',
                  NEW.id,',',
                  NEW.caption,')'));
  END$$

DELIMITER ;

/* Definition for the `t_country_group_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_group_after_upd_tr1` AFTER UPDATE ON `t_country_group`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Группы стран" изменена запись ',
                  OLD.caption,' (',
                  OLD.id, ') на ',
                  NEW.caption, ' (',
                  NEW.id, ')'),
           'country_group',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_country_group(id, \r\n    caption) values (',
                  OLD.id,',',
                  OLD.caption,
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_country_group_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_country_group_after_del_tr1` AFTER DELETE ON `t_country_group`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Группы стран" удалена запись ',
                  OLD.caption,' (',
                  OLD.id, ')'),
           'country_group',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_country_group(id, \r\n    caption) values (',
                  OLD.id,',',
                  OLD.caption,')'));
  END$$

DELIMITER ;

/* Definition for the `t_dictionary_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_dictionary_after_ins_tr1` AFTER INSERT ON `t_dictionary`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Словарь" добавлена запись ',
                  NEW.caption, ' (',
                  NEW.id,'-',
                  coalesce(NEW.pid,'нет автора'), '-',
                  COALESCE(NEW.parent, 'нет предыдущего понятия'), '-',
                  coalesce(NEW.description, 'нет описания'),'-',
                  coalesce(NEW.bibliography, 'нет библиографии'),'-',
                  coalesce(NEW.foreign_lang, 'нет перевода'),
                  ')'),
           'dictionary',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (',
                  NEW.id,',',
                  coalesce(NEW.pid,''),',',
                  COALESCE(NEW.parent, ''),',',
                  coalesce(NEW.description, ''),',',
                  coalesce(NEW.bibliography, ''),',',
                  coalesce(NEW.foreign_lang, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_dictionary_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_dictionary_after_upd_tr1` AFTER UPDATE ON `t_dictionary`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Словарь" изменена запись ',
                  OLD.caption,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid,'нет автора'), '-',
                  COALESCE(OLD.parent, 'нет предыдущего понятия'),'-',
                  coalesce(OLD.description, 'нет описания'),'-',
                  coalesce(OLD.bibliography, 'нет библиографии'),'-',
                  coalesce(OLD.foreign_lang, 'нет перевода'),
                  ') на ',
                  NEW.caption,' (',
                  NEW.id,'-',
                  coalesce(NEW.pid,'нет автора'), '-',
                  COALESCE(NEW.parent, 'нет предыдущего понятия'),'-',
                  coalesce(NEW.description, 'нет описания'),'-',
                  coalesce(NEW.bibliography, 'нет библиографии'),'-',
                  coalesce(NEW.foreign_lang, 'нет перевода'), ')'
           ),
           'dictionary',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (',
                  OLD.id,',',
                  coalesce(OLD.pid,''), ',',
                  COALESCE(OLD.parent, ''),',',
                  OLD.description,',',
                  OLD.bibliography,',',
                  OLD.foreign_lang,
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_dictionary_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_dictionary_after_del_tr1` AFTER DELETE ON `t_dictionary`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Словарь" удалена запись ',
                  OLD.caption,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid,'нет автора'),'-',
                  COALESCE(OLD.parent, 'нет предыдущего понятия'),'-',
                  OLD.description,'-',
                  OLD.bibliography,'-',
                  OLD.foreign_lang,
                  ')'),
           'dictionary',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_dictionary(id, \r\n    pid, \r\n    parent, \r\n    caption, \r\n    description, \r\n    bibliography, \r\n    foreign_lang) values (',
                  OLD.id,',',
                  coalesce(OLD.pid,''), ',',
                  COALESCE(OLD.parent, ''),',',
                  OLD.description,',',
                  OLD.bibliography,',',
                  OLD.foreign_lang,
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_files_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_files_after_ins_tr1` AFTER INSERT ON `t_files`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Файлы" добавлена запись ',
                  coalesce(NEW.caption, 'без названия'),' (',
                  coalesce(NEW.id, ''),'-',
                  coalesce(NEW.pid, 'без связи'),'-',
                  coalesce(NEW.author, 'без автора'),'-',
                  coalesce(NEW.link, 'без ссылки'),'-',
                  coalesce(NEW.visible, 'без статуса'),'-',
                  coalesce(NEW.bibliography, 'без библиографии'),'-',
                  coalesce(NEW.type, 'без типа'),'-',
                  coalesce(NEW.key_word, 'без ключевых слов'),'-',
                  coalesce(NEW.format, 'без расширения'),')'),
           'files',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (',
                  coalesce(NEW.id, ''),',',
                  coalesce(NEW.pid, ''),',',
                  coalesce(NEW.caption, ''),',',
                  coalesce(NEW.author, ''),',',
                  coalesce(NEW.link, ''),',',
                  coalesce(NEW.visible, ''),',',
                  coalesce(NEW.bibliography, ''),',',
                  coalesce(NEW.type, ''),',',
                  coalesce(NEW.key_word, ''),',',
                  coalesce(NEW.format, ''),')'));
  END$$

DELIMITER ;

/* Definition for the `t_files_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_files_after_upd_tr1` AFTER UPDATE ON `t_files`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Файлы" изменена запись ',
                  coalesce(OLD.caption, 'без названия'),' (',
                  coalesce(OLD.id, ''),'-',
                  coalesce(OLD.pid, 'без связи'),'-',
                  coalesce(OLD.author, 'без автора'),'-',
                  coalesce(OLD.link, 'без ссылки'),'-',
                  coalesce(OLD.visible, 'без статуса'),'-',
                  coalesce(OLD.bibliography, 'без библиографии'),'-',
                  coalesce(OLD.type, 'без типа'),'-',
                  coalesce(OLD.key_word, 'без ключевых слов'),'-',
                  coalesce(OLD.format, 'без расширения'),') на ',
                  coalesce(NEW.caption, 'без названия'),' (',
                  coalesce(NEW.id, ''),'-',
                  coalesce(NEW.pid, 'без связи'),'-',
                  coalesce(NEW.author, 'без автора'),'-',
                  coalesce(NEW.link, 'без ссылки'),'-',
                  coalesce(NEW.visible, 'без статуса'),'-',
                  coalesce(NEW.bibliography, 'без библиографии'),'-',
                  coalesce(NEW.type, 'без типа'),'-',
                  coalesce(NEW.key_word, 'без ключевых слов'),'-',
                  coalesce(NEW.format, 'без расширения'),')'),
           'files',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (',
                  coalesce(OLD.id, ''),',',
                  coalesce(OLD.pid, ''),',',
                  coalesce(OLD.caption, ''),',',
                  coalesce(OLD.author, ''),',',
                  coalesce(OLD.link, ''),',',
                  coalesce(OLD.visible, ''),',',
                  coalesce(OLD.bibliography, ''),',',
                  coalesce(OLD.type, ''),',',
                  coalesce(OLD.key_word, ''),',',
                  coalesce(OLD.format, ''),')'));
  END$$

DELIMITER ;

/* Definition for the `t_files_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_files_after_del_tr1` AFTER DELETE ON `t_files`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Файлы" удалена запись ',
                  coalesce(OLD.caption, 'без названия'),' (',
                  coalesce(OLD.id, ''),'-',
                  coalesce(OLD.pid, 'без связи'),'-',
                  coalesce(OLD.author, 'без автора'),'-',
                  coalesce(OLD.link, 'без ссылки'),'-',
                  coalesce(OLD.visible, 'без статуса'),'-',
                  coalesce(OLD.bibliography, 'без библиографии'),'-',
                  coalesce(OLD.type, 'без типа'),'-',
                  coalesce(OLD.key_word, 'без ключевых слов'),'-',
                  coalesce(OLD.format, 'без расширения'),')'),
           'files',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_files(\r\n    id,\r\n    pid,\r\n    caption,\r\n    author,\r\n    link,\r\n    add_date,\r\n    visible,\r\n    bibliography,\r\n    type,\r\n    key_word,\r\n    format\r\n    ) values (',
                  coalesce(OLD.id, ''),',',
                  coalesce(OLD.pid, ''),',',
                  coalesce(OLD.caption, ''),',',
                  coalesce(OLD.author, ''),',',
                  coalesce(OLD.link, ''),',',
                  coalesce(OLD.visible, ''),',',
                  coalesce(OLD.bibliography, ''),',',
                  coalesce(OLD.type, ''),',',
                  coalesce(OLD.key_word, ''),',',
                  coalesce(OLD.format, ''),')'));
  END$$

DELIMITER ;

/* Definition for the `t_idiom_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_idiom_after_ins_tr1` AFTER INSERT ON `t_idiom`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Высказывания" добавлена запись ',
                  NEW.idiom,' (',
                  NEW.id,'-',
                  coalesce(NEW.pid, 'нет автора'), '-',
                  coalesce(NEW.original, 'нет оригинала'), ')'),
           'idiom',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (',
                  NEW.id,',',
                  coalesce(NEW.pid, ''),',',
                  coalesce(NEW.idiom, ''),',',
                  coalesce(NEW.original, ''),')'));
  END$$

DELIMITER ;

/* Definition for the `t_idiom_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_idiom_after_upd_tr1` AFTER UPDATE ON `t_idiom`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Высказывания" изменена запись ',
                  OLD.idiom,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid, 'нет автора'), '-',
                  coalesce(OLD.original, 'нет оригинала'), ') на ',
                  NEW.idiom, ' (',
                  NEW.id,'-',
                  coalesce(NEW.pid, 'нет автора'), '-',
                  coalesce(NEW.original ,'нет оригинала'), ')'),
           'idiom',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_idiom(id, \r\n    pid, \r\n    idiom, \r\n    original) values (',
                  OLD.id,',',
                  coalesce(OLD.pid, ''),',',
                  coalesce(OLD.idiom, ''),',',
                  coalesce(OLD.original, ''),')'));
  END$$

DELIMITER ;

/* Definition for the `t_idiom_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_idiom_after_del_tr1` AFTER DELETE ON `t_idiom`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Высказывания" удалена запись ',
                  OLD.idiom,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid, 'нет автора'), '-',
                  coalesce(OLD.original, 'нет оригинала'), ')'),
           'idiom',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_idiom(id, \r\n    pid,\r\n    idiom, \r\n    original) values (',
                  OLD.id,',',
                  coalesce(OLD.pid, ''),',',
                  coalesce(OLD.idiom, ''),',',
                  coalesce(OLD.original, ''),')'));
  END$$

DELIMITER ;

/* Definition for the `t_opus_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_opus_after_ins_tr1` AFTER INSERT ON `t_opus`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Произведения" добавлена запись ',
                  NEW.caption,' (',
                  NEW.id, '-',
                  coalesce(NEW.pid, 'нет автора'), '-',
                  coalesce(NEW.caption, 'нет названия'), '-',
                  coalesce(NEW.annotation, 'нет аннотации'), '-',
                  coalesce(NEW.cover, 'нет обложки'), '-',
                  coalesce(NEW.published, 'нет даты'), '-',
                  coalesce(NEW.bibliography, 'нет библиографии'), '-',
                  coalesce(NEW.key_word, 'нет ключей'), '-',
                  coalesce(NEW.section, 'нет раздела'),
                  ')'),
           'opus',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (',
                  NEW.id, ',',
                  coalesce(NEW.pid, ''), ',',
                  coalesce(NEW.caption, ''), ',',
                  coalesce(NEW.annotation, ''), ',',
                  coalesce(NEW.cover, ''), ',',
                  coalesce(NEW.published, ''), ',',
                  coalesce(NEW.bibliography, ''), ',',
                  coalesce(NEW.key_word, ''), ',',
                  coalesce(NEW.section, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_opus_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_opus_after_upd_tr1` AFTER UPDATE ON `t_opus`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Произведения" изменена запись ',
                  OLD.caption,' (',
                  OLD.id, '-',
                  coalesce(OLD.pid, 'нет автора'), '-',
                  coalesce(OLD.caption, 'нет названия'), '-',
                  coalesce(OLD.annotation, 'нет аннотации'), '-',
                  coalesce(OLD.cover, 'нет обложки'), '-',
                  coalesce(OLD.published, 'нет даты'), '-',
                  coalesce(OLD.bibliography, 'нет библиографии'), '-',
                  coalesce(OLD.key_word, 'нет ключей'), '-',
                  coalesce(OLD.section, 'нет раздела'),
                  ') на ',
                  NEW.caption, '(',
                  coalesce(NEW.id, ''), '-',
                  coalesce(NEW.pid, 'нет автора'), '-',
                  coalesce(NEW.annotation, 'нет аннотации'), '-',
                  coalesce(NEW.cover, 'нет обложки'), '-',
                  coalesce(NEW.published, 'нет даты'), '-',
                  coalesce(NEW.bibliography, 'нет библиографии'), '-',
                  coalesce(NEW.key_word, 'нет ключей'), '-',
                  coalesce(NEW.section, 'нет раздела'), ')'
           ),
           'opus',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (',
                  OLD.id, ',',
                  coalesce(OLD.pid, ''), ',',
                  coalesce(OLD.caption, ''), ',',
                  coalesce(OLD.annotation, ''), ',',
                  coalesce(OLD.cover, ''), ',',
                  coalesce(OLD.published, ''), ',',
                  coalesce(OLD.bibliography, ''), ',',
                  coalesce(OLD.key_word, ''), ',',
                  coalesce(OLD.section, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_opus_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_opus_after_del_tr1` AFTER DELETE ON `t_opus`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Произведения" удалена запись ', OLD.caption,' (',
                  OLD.id, '-',
                  coalesce(OLD.pid, 'нет автора'), '-',
                  coalesce(OLD.caption, 'нет названия'), '-',
                  coalesce(OLD.annotation, 'нет аннотации'), '-',
                  coalesce(OLD.cover, 'нет обложки'), '-',
                  coalesce(OLD.published, 'нет даты'), '-',
                  coalesce(OLD.bibliography, 'нет библиографии'), '-',
                  coalesce(OLD.key_word, 'нет ключей'), '-',
                  coalesce(OLD.section, 'нет раздела'),
                  ')'),
           'opus',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_opus(id, \r\n    pid, \r\n    caption, \r\n    annotation, \r\n    cover, \r\n    published, \r\n    bibliography, \r\n    key_word, \r\n    section) values (',
                  OLD.id, ',',
                  coalesce(OLD.pid, ''), ',',
                  coalesce(OLD.caption, ''), ',',
                  coalesce(OLD.annotation, ''), ',',
                  coalesce(OLD.cover, ''), ',',
                  coalesce(OLD.published, ''), ',',
                  coalesce(OLD.bibliography, ''), ',',
                  coalesce(OLD.key_word, ''), ',',
                  coalesce(OLD.section, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_person_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_person_after_ins_tr1` AFTER INSERT ON `t_person`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Персоны" добавлена запись ',
                  NEW.name,' (',
                  NEW.id, '-',
                  coalesce(NEW.surname, 'нет фамилии'), '-',
                  coalesce(NEW.last_name, 'нет отчества'), '-',
                  coalesce(NEW.birth_date, 'нет рождения'), '-',
                  coalesce(NEW.death_date, 'нет смерти'), '-',
                  coalesce(NEW.country, 'нет страны'), '-',
                  coalesce(NEW.parent, 'нет предшественника'), '-',
                  coalesce(NEW.school, 'нет школы'), '-',
                  coalesce(NEW.key_word, 'нет ключей'), '-',
                  coalesce(NEW.annotation, 'нет аннотации'), '-',
                  coalesce(NEW.link, 'нет подробностей'), '-',
                  coalesce(NEW.portrait, 'нет портрета'), '-',
                  coalesce(NEW.bibliography, 'нет библиографии'), '-'
                  ')'),
           'person',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (',
                  NEW.id, ',',
                  coalesce(NEW.name, ''), ',',
                  coalesce(NEW.surname, ''), ',',
                  coalesce(NEW.last_name, ''), ',',
                  coalesce(NEW.birth_date, ''), ',',
                  coalesce(NEW.death_date, ''), ',',
                  coalesce(NEW.country, ''), ',',
                  coalesce(NEW.parent, ''), ',',
                  coalesce(NEW.school, ''), ',',
                  coalesce(NEW.key_word, ''), ',',
                  coalesce(NEW.annotation, ''), ',',
                  coalesce(NEW.link, ''), ',',
                  coalesce(NEW.portrait, ''), ',',
                  coalesce(NEW.bibliography, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_person_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_person_after_upd_tr1` AFTER UPDATE ON `t_person`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Персоны" изменена запись ',
                  OLD.name,' (',
                  OLD.id, '-',
                  coalesce(OLD.surname, 'нет фамилии'), '-',
                  coalesce(OLD.last_name, 'нет отчества'), '-',
                  coalesce(OLD.birth_date, 'нет рождения'), '-',
                  coalesce(OLD.death_date, 'нет смерти'), '-',
                  coalesce(OLD.country, 'нет страны'), '-',
                  coalesce(OLD.parent, 'нет предшественника'), '-',
                  coalesce(OLD.school, 'нет школы'), '-',
                  coalesce(OLD.key_word, 'нет ключей'), '-',
                  coalesce(OLD.annotation, 'нет аннотации'), '-',
                  coalesce(OLD.link, 'нет подробностей'), '-',
                  coalesce(OLD.portrait, 'нет портрета'), '-',
                  coalesce(OLD.bibliography, 'нет библиографии'), ' на ',
                  NEW.name,' (',
                  NEW.id, '-',
                  coalesce(NEW.surname, 'нет фамилии'), '-',
                  coalesce(NEW.last_name, 'нет отчества'), '-',
                  coalesce(NEW.birth_date, 'нет рождения'), '-',
                  coalesce(NEW.death_date, 'нет смерти'), '-',
                  coalesce(NEW.country, 'нет страны'), '-',
                  coalesce(NEW.parent, 'нет предшественника'), '-',
                  coalesce(NEW.school, 'нет школы'), '-',
                  coalesce(NEW.key_word, 'нет ключей'), '-',
                  coalesce(NEW.annotation, 'нет аннотации'), '-',
                  coalesce(NEW.link, 'нет подробностей'), '-',
                  coalesce(NEW.portrait, 'нет портрета'), '-',
                  coalesce(NEW.bibliography, 'нет библиографии'),
                  ')'),
           'person',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (',
                  OLD.id, ',',
                  coalesce(OLD.name, ''), ',',
                  coalesce(OLD.surname, ''), ',',
                  coalesce(OLD.last_name, ''), ',',
                  coalesce(OLD.birth_date, ''), ',',
                  coalesce(OLD.death_date, ''), ',',
                  coalesce(OLD.country, ''), ',',
                  coalesce(OLD.parent, ''), ',',
                  coalesce(OLD.school, ''), ',',
                  coalesce(OLD.key_word, ''), ',',
                  coalesce(OLD.annotation, ''), ',',
                  coalesce(OLD.link, ''), ',',
                  coalesce(OLD.portrait, ''), ',',
                  coalesce(OLD.bibliography, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_person_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_person_after_del_tr1` AFTER DELETE ON `t_person`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Персоны" удалена запись ',
                  OLD.name,' (',
                  OLD.id, '-',
                  coalesce(OLD.surname, 'нет фамилии'), '-',
                  coalesce(OLD.last_name, 'нет отчества'), '-',
                  coalesce(OLD.birth_date, 'нет рождения'), '-',
                  coalesce(OLD.death_date, 'нет смерти'), '-',
                  coalesce(OLD.country, 'нет страны'), '-',
                  coalesce(OLD.parent, 'нет предшественника'), '-',
                  coalesce(OLD.school, 'нет школы'), '-',
                  coalesce(OLD.key_word, 'нет ключей'), '-',
                  coalesce(OLD.annotation, 'нет аннотации'), '-',
                  coalesce(OLD.link, 'нет подробностей'), '-',
                  coalesce(OLD.portrait, 'нет портрета'), '-',
                  coalesce(OLD.bibliography, 'нет библиографии'), '-'
                  ')'),
           'person',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_person(\r\n    id, \r\n    name,\r\n    surname,\r\n    last_name,\r\n    birth_date,\r\n    death_date,\r\n    country,\r\n    parent,\r\n    school,\r\n    key_word,\r\n    annotation,\r\n    link,\r\n    portrait,\r\n    bibliography\r\n    ) values (',
                  OLD.id, ',',
                  coalesce(OLD.name, ''), ',',
                  coalesce(OLD.surname, ''), ',',
                  coalesce(OLD.last_name, ''), ',',
                  coalesce(OLD.birth_date, ''), ',',
                  coalesce(OLD.death_date, ''), ',',
                  coalesce(OLD.country, ''), ',',
                  coalesce(OLD.parent, ''), ',',
                  coalesce(OLD.school, ''), ',',
                  coalesce(OLD.key_word, ''), ',',
                  coalesce(OLD.annotation, ''), ',',
                  coalesce(OLD.link, ''), ',',
                  coalesce(OLD.portrait, ''), ',',
                  coalesce(OLD.bibliography, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_school_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_school_after_ins_tr1` AFTER INSERT ON `t_school`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Школы" добавлена запись ',
                  coalesce(NEW.school, 'нет названия'),' (',
                  coalesce(NEW.id, ''), '-',
                  coalesce(NEW.annotation, 'нет аннотации'), '-',
                  coalesce(NEW.parent, 'нет предшественника'), '-',
                  coalesce(NEW.bibliography, 'нет библиографии'),
                  ')'),
           'school',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (',
                  coalesce(NEW.id, ''), ',',
                  coalesce(NEW.school, ''),',',
                  coalesce(NEW.annotation, ''), ',',
                  coalesce(NEW.parent, ''), ',',
                  coalesce(NEW.bibliography, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_school_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_school_after_upd_tr1` AFTER UPDATE ON `t_school`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Школы" изменена запись ',
                  coalesce(OLD.school, 'нет названия'),' (',
                  coalesce(OLD.id, ''), '-',
                  coalesce(OLD.annotation, 'нет аннотации'), '-',
                  coalesce(OLD.parent, 'нет предщественника'), '-',
                  coalesce(OLD.bibliography, 'нет библиографии'),
                  ') на ',
                  coalesce(NEW.school, 'нет названия'),' (',
                  coalesce(NEW.id, ''), '-',
                  coalesce(NEW.annotation, 'нет аннотации'), '-',
                  coalesce(NEW.parent, 'нет предшественника'), '-',
                  coalesce(NEW.bibliography, 'нет библиографии'), ')'
           ),
           'school',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (',
                  coalesce(OLD.id, ''), ',',
                  coalesce(OLD.school, ''),',',
                  coalesce(OLD.annotation, ''), ',',
                  coalesce(OLD.parent, ''), ',',
                  coalesce(OLD.bibliography, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_school_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_school_after_del_tr1` AFTER DELETE ON `t_school`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Школы" удалена запись ',
                  coalesce(OLD.school, 'нет названия'),' (',
                  coalesce(OLD.id, ''), '-',
                  coalesce(OLD.annotation, 'нет аннотации'), '-',
                  coalesce(OLD.parent, 'нет предщественника'), '-',
                  coalesce(OLD.bibliography, 'нет библиографии'),
                  ')'),
           'school',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_school(\r\n    id, \r\n    school,\r\n    annotation,\r\n    parent,\r\n    bibliography\r\n    ) values (',
                  coalesce(OLD.id, ''), ',',
                  coalesce(OLD.school, ''),',',
                  coalesce(OLD.annotation, ''), ',',
                  coalesce(OLD.parent, ''), ',',
                  coalesce(OLD.bibliography, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_section_after_ins_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_section_after_ins_tr1` AFTER INSERT ON `t_section`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           NEW.id,
           CONCAT('В таблицу "Разделы науки" добавлена запись ',
                  NEW.caption,' (',
                  NEW.id,'-',
                  coalesce(NEW.pid, 'нет надраздела'),'-',
                  coalesce(NEW.caption, 'нет названия'),'-',
                  coalesce(NEW.annotation, 'нет аннотации'),
                  ')'),
           'section',
           CURRENT_TIMESTAMP(),
           'i',
           CONCAT('insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (',
                  NEW.id,',',
                  coalesce(NEW.pid, ''),',',
                  coalesce(NEW.caption, ''),',',
                  coalesce(NEW.annotation, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_section_after_upd_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_section_after_upd_tr1` AFTER UPDATE ON `t_section`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('В таблице "Разделы науки" изменена запись ',
                  OLD.caption,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid, 'нет надраздела'),'-',
                  coalesce(OLD.caption, 'нет названия'),'-',
                  coalesce(OLD.annotation, 'нет аннотации'),
                  ') на ',
                  NEW.caption,' (',
                  NEW.id,'-',
                  coalesce(NEW.pid, 'нет надраздела'),'-',
                  coalesce(NEW.caption, 'нет названия'),'-',
                  coalesce(NEW.annotation, 'нет аннотации'), ')'
           ),
           'section',
           CURRENT_TIMESTAMP(),
           'u',
           CONCAT('insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (',
                  OLD.id,',',
                  coalesce(OLD.pid, ''),',',
                  coalesce(OLD.caption, ''),',',
                  coalesce(OLD.annotation, ''),
                  ')'));
  END$$

DELIMITER ;

/* Definition for the `t_section_after_del_tr1` trigger : */

DELIMITER $$

CREATE DEFINER = 'root'@'localhost' TRIGGER `t_section_after_del_tr1` AFTER DELETE ON `t_section`
FOR EACH ROW
  BEGIN
    INSERT into `core_log`(`id`, `pid`, `message`, `table`, `timed`, `action_type`, `script`)
    VALUES(`f_core_get_id`(),
           OLD.id,
           CONCAT('Из таблицы "Разделы науки" удалена запись ',
                  OLD.caption,' (',
                  OLD.id,'-',
                  coalesce(OLD.pid, 'нет надраздела'),'-',
                  coalesce(OLD.caption, 'нет названия'),'-',
                  coalesce(OLD.annotation, 'нет аннотации'),
                  ')'),
           'section',
           CURRENT_TIMESTAMP(),
           'd',
           CONCAT('insert into t_section(\r\n    id, \r\n    pid,\r\n    caption,\r\n    annotation\r\n    ) values (',
                  OLD.id,',',
                  coalesce(OLD.pid, ''),',',
                  coalesce(OLD.caption, ''),',',
                  coalesce(OLD.annotation, ''),
                  ')'));
  END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;