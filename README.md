**Versions:**
1. Angular 4.0.0^
2. npm 3.3.6^
3. node 5.0.0^
4. webpack 2.2.1^
5. with preboot box
6. MariaDB 10.0^ (Philosophy Module)
7. PostgreSQL 9.5^ (Literature Module)
8. Vis.js 4.17.0^
9. I use it with in **1366x768** with F11 mode. In other resolutions it is **not handsomely** now.

**How to use it:**

_Frontend:_
1. Clone rep
2. cd my-app
3. webpack -p && npm start
4. http://localhost:8080

_Backend:_
1. Install base you need.
2. Set config in setting.php (/my-app/src/core/setting.php).
3. Set dump you need (example on /my-app/base_examples/).

Also you can check the screens in /base_examples/screens/.

Thanks to [Preboot-Starter](https://github.com/preboot) for [angular-webpack rep.](https://github.com/preboot/angular-webpack)
Thanks to [Severes](https://github.com/seveves) for [ng2-vis rep.](https://github.com/seveves/ng2-vis)